package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.CPcontainers.ManageVehiclePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class LandRoverCPE2EJourney extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ManageVehiclePageContainer manageVehiclePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(LandRoverCPE2EJourney.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	
	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			manageVehiclePageContainer = PageFactory.initElements(driver, ManageVehiclePageContainer.class);
			
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void landRoverCp(String vehicleName, String nickName, String reminderDate, String note,
			 String language,String country, String question,String vinNumber, String engineNumber, String ownerDriver) {
		try {

			landRoverCpE2E(vehicleName, nickName, reminderDate, note, language, country,  question, vinNumber,  engineNumber,  ownerDriver);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void landRoverCpE2E(String vehicleName, String nickName, String reminderDate, String note,
			 String language,String country, String question,String vinNumber, String engineNumber, String ownerDriver) {
		try {
			
			CPCJ5_AddVehicle addVehicle=new CPCJ5_AddVehicle();
			addVehicle.cpcj5(vehicleName, vinNumber, engineNumber, ownerDriver);
			extentLogger.log(Status.INFO, "Add A Vehicle Test Completed!!");
			

			// Vehicle Management
			// Click on options and select vehicle
			// Edit vehicle info
			extentLogger.log(Status.INFO, "Editing Vehicle Information");
			String homeRegistrationNo = homePageContainer.registrationNo.getText();
			ActionHandler.click(homePageContainer.editVehicleInfo);
			ActionHandler.waitForElement(manageVehiclePageContainer.manageMyVehicleHeader, 10);

			// Verification of Registration number
			assertEquals(homeRegistrationNo,
					ActionHandler.getAttributeValue(manageVehiclePageContainer.regID, "value"));
			extenVerificationObj.extentVerification("Verify Registration Number:", homeRegistrationNo,
					ActionHandler.getAttributeValue(manageVehiclePageContainer.regID, "value"), "");

			// Set New nick name
			extentLogger.log(Status.INFO, "Entering Nick Name:" + nickName);
			ActionHandler.clearAndSetText(manageVehiclePageContainer.nickName, nickName);
			String nickNameEdit = ActionHandler.getAttributeValue(manageVehiclePageContainer.nickName, "value");

			
			commonFuncObject.reminderInfoValidation(Constants.FINANCE);
			commonFuncObject.reminderInfoValidation(Constants.INSURANCE);
			commonFuncObject.reminderInfoValidation(Constants.MOT);
			commonFuncObject.reminderInfoValidation(Constants.ROADSIDE);
			commonFuncObject.reminderInfoValidation(Constants.SERVICEDUE);
			commonFuncObject.reminderInfoValidation(Constants.SERVICEPLAN);
			commonFuncObject.reminderInfoValidation(Constants.MANUFACTURER);
			
			// Enter Reminder Road Tax
			commonFuncObject.enterDate(reminderDate, Constants.ROADTAX);

			// Enter Text
			extentLogger.log(Status.INFO, "Entering notes: "+ note);
			ActionHandler.clearAndSetText(manageVehiclePageContainer.notes, note);
			ActionHandler.wait(4);

			ActionHandler.click(manageVehiclePageContainer.saveButton);
			ActionHandler.wait(3);
			extenVerificationObj.extentVerification("Changes Saved Pop Up Verification:", Constants.CHANGESSAVED,
					manageVehiclePageContainer.changesSaved.getText(), "");
			ActionHandler.wait(4);
			ActionHandler.scrollAndClick(manageVehiclePageContainer.backButton);
			ActionHandler.wait(3);
			assertEquals(homePageContainer.nickName.getText(), nickNameEdit);
			extenVerificationObj.extentVerification("Nick Name Validation:", homePageContainer.nickName.getText(),
					nickNameEdit, "");
			
			// ReminderDate Validations
			commonFuncObject.remainingDaysMonthsYearsValidations(reminderDate);
					
			CPCJ9_UserGuidesManual userGuideManual=new CPCJ9_UserGuidesManual();
			userGuideManual.cpcj9(vehicleName, language, country);
			extentLogger.log(Status.INFO, "User And Guide Test Completed!!");
			
			
			CPCJ6_HelpAndSupport helpAndSupport=new CPCJ6_HelpAndSupport();
			helpAndSupport.cpcj6(question);
			extentLogger.log(Status.INFO, "Help And Support Test Completed!!");	
			
			

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
