package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.JaguarGearContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class CJ7 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static JaguarGearContainer jaguarGearContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	public static String journeyName = "";
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ7.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			journeyName = context.getCurrentXmlTest().getName();
			setupTest(journeyName);

			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.JAGUAREPACE);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			jaguarGearContainer = PageFactory.initElements(driver, JaguarGearContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		} catch (Exception e) {

			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ7(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String options, String infotainment,
			String jaguarGear, String extAccessories, String paymentOpt, String advRetail, String defaultAr,
			String term, String defaultTerm, String annualMileage, String defaultMileage, String deposit,
			String defaultDeposit) {

		try {
			/*
			 * commonFuncObject.pdfValidation(Constants.JAGUAREPACE, models,
			 * engine, specif, nextStep, exterior, colours, visualPacks,
			 * interior, headlining, options, infotainment, jaguarGear,
			 * extAccessories);
			 */

			testNcosCJ7(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, options,
					infotainment, jaguarGear, extAccessories, paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testNcosCJ7(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String options, String infotainment,
			String jaguarGear, String extAccessories, String paymentOpt, String advRetail, String defaultAr,
			String term, String defaultTerm, String annualMileage, String defaultMileage, String deposit,
			String defaultDeposit) throws Exception {
		try {
			String finalValue = "", perMonth = "", expectedCashback = "";

			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.JAGUAREPACE);

			ActionHandler.wait(5);
			driver.findElement(By.xpath(financeContainer.financeProduct(paymentOpt))).click();
			// Navigates to Finance Quote
			List<WebElement> defaultValueList = financeContainer.defaultValueList;
			String termValue = defaultValueList.get(1).getText().replaceAll("[^\\d.]", "");
			String annualMileageValue = null;

			extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);

			// Validating default options of PCP
			commonFuncObject.verifyFinanceQuoteDetails(paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit, driver);

			// Part Exchange Validations
			commonFuncObject.partExchange_R1(Constants.MILEAGE);
			commonValidationObj.partExchangeValidations();

			ActionHandler.click(financeContainer.useTowardDepositButton);
			ActionHandler.wait(3);

			// Validating part exchange value is set under total deposit
			// On UI decimal numbers are not allowed
			String totalDeposit = financeContainer.totalDepositDefault.getAttribute("value");
			Double actualDeposit = Double.valueOf(
					ActionHandler.getElementText(financeContainer.exchangeEquityTotalValue).replaceAll("[^\\d.]", ""));
			Double expectedDeposit = Double.valueOf(totalDeposit.replaceAll("[^\\d.]", ""));
			LOGGER.info(
					"Verify Part Exchange Value is set under Total Deposit: " + actualDeposit + " " + expectedDeposit);
			assertEquals(actualDeposit, expectedDeposit);

			ActionHandler.click(financeContainer.getYourQuote);
			ActionHandler.wait(2);
			String finalDeposite = ActionHandler.getElementText(financeContainer.finalDeposite).replaceAll("[^\\d.]",
					""); // final deposite on Hire Purchase
			finalDeposite = finalDeposite.contains(".00") ? finalDeposite.replace(".00", "") : finalDeposite;
			ActionHandler.waitForElement(financeContainer.continueToBuild, 20);
			// Quote Validations
			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Part Exchange Equity"))).getText()
					.replaceAll("[^\\d.]", ""), totalDeposit.replaceAll("[^\\d.]", "") + ".00");
			extenVerificationObj.extentVerification("Verify Part Exchange Equity: ",
					driver.findElement(By.xpath(financeContainer.getQuote("Part Exchange Equity"))).getText()
							.replaceAll("[^\\d.]", ""),
					totalDeposit.replaceAll("[^\\d.]", "") + ".00", "");

			commonValidationObj.financeQuoteValidations(termValue, annualMileageValue,
					homePageContainer.onTheRoadPrice.getText(), totalDeposit, paymentOpt);

			assertEquals(
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(paymentOpt))).getText(),
					Constants.HP);
			extenVerificationObj.extentVerification("Verify HP Text",
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(paymentOpt))).getText(),
					Constants.HP, "");

			ActionHandler.click(financeContainer.continueToBuild);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(2);
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior : Colors");

				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior : Visual Pack");

			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(4);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))).click();
				ActionHandler.wait(3);
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Interior : Headlining");

			}

			if (("Yes").equals(jaguarGear)) {
				ActionHandler.click(jaguarGearContainer.jaguarGearTab);
				ActionHandler.wait(2);
				ActionHandler.click(jaguarGearContainer.TouringAcc);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(extAccessories))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
						"Jaguar Gear: Touring Accessories");

			}

			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(2);
			LOGGER.info(homePageContainer.onTheRoadPrice.getText());
			LOGGER.info(summaryContainer.grandTotal.getText());
			String split[] = summaryContainer.grandTotal.getText().split(" ");
			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar OTR Validation: ", split[0],
					homePageContainer.onTheRoadPrice.getText(), "");
			finalValue = homePageContainer.onTheRoadPrice.getText();

			assertEquals(split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar Per Month Validation: ",
					split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText(), "");
			perMonth = homePageContainer.perMonthInfoBarValue.getText();

			ActionHandler.click(summaryContainer.orderNow);
			ActionHandler.wait(2);
			String oldTab = commonFuncObject.completePersonalDetails(Constants.JAGUAREPACE);

			commonFuncObject.avokaTransact(models, engine, colours, headlining, paymentOpt, finalDeposite,
					expectedCashback, perMonth, finalValue, "Yes", Constants.VEHICLENAMER1, Constants.FUELTYPER1,
					Constants.REGISTRATIONNUMBER1);

			/*
			 * enquiryMaxObj.createEnquiryMaxExcel(journeyName, models, engine,
			 * colours, headlining, paymentOpt, totalDeposit, expectedCashback,
			 * perMonth, finalValue, Config.getPropertyValue("email"),
			 * Config.getPropertyValue("firstName"),
			 * Config.getPropertyValue("lastName"),
			 * Config.getPropertyValue("mobileNo"),
			 * Config.getPropertyValue("address"));
			 */

			// Switching to Parent window
			driver.switchTo().window(oldTab);
		} catch (Exception e) {
			extenVerificationObj.extentError(e.toString());
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		// commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
