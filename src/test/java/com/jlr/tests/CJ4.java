package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.AccessoriesContainer;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.OptionsContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class CJ4 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static OptionsContainer optionsContainer = null;
	private static AccessoriesContainer accessoriesContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ4.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.LANDROVER);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			optionsContainer = PageFactory.initElements(driver, OptionsContainer.class);
			accessoriesContainer = PageFactory.initElements(driver, AccessoriesContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ4(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String options, String convenience,
			String accessories, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {

		testNcosCJ4(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, options,
				convenience, accessories, touringAcc, paymentOpt, advRetail, defaultAr, term, defaultTerm,
				annualMileage, defaultMileage, deposit, defaultDeposit);

	}

	public void testNcosCJ4(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String options, String convenience,
			String accessories, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {
		try {

			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.LANDROVER);

			ActionHandler.wait(5);
			// Navigates to Finance Quote
			List<WebElement> defaultValueList = financeContainer.defaultValueList;
			String termValue = defaultValueList.get(1).getText().replaceAll("[^\\d.]", "");
			String annualMileageValue = defaultValueList.get(2).getText().replaceAll("[^\\d.]", "");

			extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);

			// Validating default options of PCP
			commonFuncObject.verifyFinanceQuoteDetails(paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit, driver);

			// Part Exchange Validations
			commonFuncObject.partExchange("", "", Constants.MILEAGE);
			commonValidationObj.partExchangeValidations();
			ActionHandler.click(financeContainer.useTowardDepositButton);
			ActionHandler.wait(3);
			// On UI decimal numbers are not allowed
			String totalDeposit = financeContainer.totalDepositDefault.getAttribute("value");

			// Validating part exchange value is set under total deposit
			Double actualDeposit = Double.valueOf(
					ActionHandler.getElementText(financeContainer.exchangeEquityTotalValue).replaceAll("[^\\d.]", ""));
			Double expectedDeposit = Double.valueOf(totalDeposit.replaceAll("[^\\d.]", ""));
			LOGGER.info(
					"Verify Part Exchange Value is set under Total Deposit: " + actualDeposit + " " + expectedDeposit);
			assertEquals(actualDeposit, expectedDeposit);

			ActionHandler.click(financeContainer.getYourQuote);

			ActionHandler.wait(4);
			// Quote Validations
			commonValidationObj.financeQuoteValidations_R2(termValue, 
					homePageContainer.onTheRoadPrice.getText(), totalDeposit, paymentOpt);

			extenVerificationObj.extentVerification("Verify PCP Text",
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(Constants.PCP))).getText(),
					Constants.PCP, "Yes");

			ActionHandler.click(financeContainer.continueToBuild);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(2);
				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior");

			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(2);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Interior");

			}

			if (("Yes").equals(options)) {
				ActionHandler.click(optionsContainer.optionsTab);
				ActionHandler.wait(2);
				ActionHandler.click(optionsContainer.convenienceInfotainment);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(optionsContainer.selectGivenCategory(convenience))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Options");

			}

			if (("Yes").equals(accessories)) {
				ActionHandler.click(accessoriesContainer.accessoriesTab);
				ActionHandler.wait(2);
				// ActionHandler.click(accessoriesContainer.touringAccessories);
				driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))).click();
				ActionHandler.wait(1);
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Accessories");
			}

			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(2);
/*			LOGGER.info(homePageContainer.onTheRoadPrice.getText());
			LOGGER.info(summaryContainer.grandTotal.getText());
			String split[] = summaryContainer.grandTotal.getText().split(" ");
			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());

			*/
			String split[] = summaryContainer.grandTotal.getText().split(" ");

			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar OTR Validation: ", split[0],
					homePageContainer.onTheRoadPrice.getText(), "");
			
			assertEquals(split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar Per Month Validation: ",
					split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText(), "");

			ActionHandler.click(summaryContainer.sendToRetailer);

		/*	String oldTab = commonFuncObject.completePersonalDetails(Constants.LANDROVER);

			// Switching to Parent window
			driver.switchTo().window(oldTab);*/

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
