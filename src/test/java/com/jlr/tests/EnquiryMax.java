package com.jlr.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.EnquiryMaxContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class EnquiryMax extends TestBase {

	/*
	 * private static HomePageContainer homePageContainer = null; private static
	 * ExteriorContainer exteriorContainer = null; private static
	 * InteriorContainer interiorContainer = null; private static
	 * OptionsContainer optionsContainer = null; private static SummaryContainer
	 * summaryContainer = null; private static FinanceContainer financeContainer
	 * = null; private static JaguarGearContainer jaguarGearContainer = null;
	 */
	private static EnquiryMaxContainer enquiryMaxContainer = null;

	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnquiryMax.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;
	ExtenVerification extenVerificationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.ENQUIRYMAX);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			// commonFuncObject.userLogin();

			enquiryMaxContainer = PageFactory.initElements(driver, EnquiryMaxContainer.class);
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getEnquiryMaxData", dataProviderClass = DataReader.class)
	public void enquiryMaxVerif(String models, String engine, String colours, String headlining, String paymentOpt,
			String totalDeposit, String deposit, String perMonth, String finalValue, String email, String firstName,
			String lastName, String mobile, String addr) throws Exception {
		enquiryMaxVerification(models, engine, colours, headlining, paymentOpt, totalDeposit, deposit, perMonth,
				finalValue, email, firstName, lastName, mobile, addr);
	}

	public void enquiryMaxVerification(String models, String engine, String colours, String headlining,
			String paymentOpt, String totalDeposit, String deposit, String perMonth, String finalValue, String email,
			String firstName, String lastName, String mobile, String addr) throws Exception {
		try {

			extentLogger.log(Status.INFO, "Verifiyng saved data on enquiry Max Portal");

			driver.get(Config.getPropertyValue("enquiryMax.url"));

			ActionHandler.setText(enquiryMaxContainer.userNameSVCRM, Config.getPropertyValue("valid.enquiryMax.Id"));

			ActionHandler.setText(enquiryMaxContainer.passwordNameSVCRM,
					Config.getPropertyValue("valid.enquiryMax.Password"));

			ActionHandler.click(enquiryMaxContainer.submitButton);

			ActionHandler.click(enquiryMaxContainer.centralLeadLink);

			ActionHandler.wait(4);
			String userName = firstName + " " + lastName; // Create
			// UserName
			extentLogger.info("Enquiry Max Portal Home Page ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());

			if (driver.findElements(By.xpath(enquiryMaxContainer.getInfo(userName))).size() != 0) {
				ActionHandler.click(driver.findElement(By.xpath(enquiryMaxContainer.getInfo(userName))));

				// *********Personal details verification

				extenVerificationObj.extentVerification("Email ID does not match with saved data ", email,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpEmail, "value"), "Yes");

				extenVerificationObj.extentVerification("First name does not match with saved data", firstName,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpForename, "value"), "Yes");

				extenVerificationObj.extentVerification("Last name does not match with saved data", lastName,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpSurname, "value"), "Yes");

				extenVerificationObj.extentVerification("Mobile number does not match with saved data", mobile,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpMobile, "value"), "Yes");

				extenVerificationObj.extentVerification("Address does not match with saved data", true,
						addr.contains(ActionHandler.getAttributeValue(enquiryMaxContainer.popUpAddress1, "value")),
						"Yes");
				extenVerificationObj.extentVerification("Address does not match with saved data", true,
						addr.contains(ActionHandler.getAttributeValue(enquiryMaxContainer.popUpAddress2, "value")),
						"Yes");
				extenVerificationObj.extentVerification("Address does not match with saved data", true,
						addr.contains(ActionHandler.getAttributeValue(enquiryMaxContainer.popUpAddress3, "value")),
						"Yes");

				extenVerificationObj.extentVerification("Post code does not match with saved data ", addr.split(",")[1],
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpPostcode, "value"), "Yes");

				// ***********Vehicle Details*******************************
				extenVerificationObj.extentVerification("vehicle model does not match with saved data", models,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpModel, "value"), "Yes");

				extenVerificationObj.extentVerification("vehicle engine does not match with saved data", engine,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpDerivative, "value"), "Yes");

				extenVerificationObj.extentVerification("vehicle price does not match with saved data", finalValue,
						ActionHandler.getAttributeValue(enquiryMaxContainer.popUpPrice, "value"), "Yes");
			} else {
				extenVerificationObj.extentVerification(
						"Record for Customer Name " + userName + " not present in enquiryMax portal", true, false, "");
			}

			ActionHandler.click(enquiryMaxContainer.logOutMenu);
			ActionHandler.wait(2);
			ActionHandler.click(enquiryMaxContainer.logOut);
		} catch (Exception e) {
			extenVerificationObj.extentError("Exception - enquiryMax Verification : " + e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		// commonFuncObject.userLogsOut();
		getDriver().quit();
	}

	public void createEnquiryMaxExcel(String journeyName, String models, String engine, String colours,
			String headlining, String paymentOpt, String totalDeposit, String deposit, String perMonth,
			String finalValue, String email, String firstName, String lastName, String mobile, String addr) {
		try {

			String[] columns = { "MODELS", "ENGINE", "COLOURS", "HEADLINING", "PAYMENTOPT", "TOTALDEPOSITE", "DEPOSITE",
					"PERMONTH", "FINALVALUE", "EMAIL", "FIRSTNAME", "LASTNAME", "MOBILE", "ADDR" };
			String fileName = Config.getPropertyValue("testData.enquiryMax.excel");
			int rowNum = 1;
			File file = new File(System.getProperty("user.dir") + "/src/test/resources/" + fileName + "");
			if ("enquiryMaxCJ2".equalsIgnoreCase(journeyName)) {
				rowNum = 1;
			} else if ("enquiryMaxCJ7".equalsIgnoreCase(journeyName)) {
				rowNum = 2;
			} else {
				rowNum = 3;
			}

			if (!file.exists()) {
				// Create a Workbook
				Workbook workbook = new XSSFWorkbook();
				// Create a Sheet
				Sheet sheet = workbook.createSheet("EnquiryMAx");
				// Create a Row
				Row headerRow = sheet.createRow(0);

				// Create cells
				for (int i = 0; i < columns.length; i++) {
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(columns[i]);
				}
				// Create Other rows and cells with employees data
				int count = 1;
				while (count < 4) {
					Row row = sheet.createRow(count++);

					row.createCell(0).setCellValue("");

					row.createCell(1).setCellValue("");

					row.createCell(2).setCellValue("");

					row.createCell(3).setCellValue("");

					row.createCell(4).setCellValue("");

					row.createCell(5).setCellValue("");

					row.createCell(6).setCellValue("");

					row.createCell(7).setCellValue("");

					row.createCell(8).setCellValue("");

					row.createCell(9).setCellValue("");

					row.createCell(10).setCellValue("");

					row.createCell(11).setCellValue("");

					row.createCell(12).setCellValue("");

					row.createCell(13).setCellValue("");
				}
				FileOutputStream fileOut = new FileOutputStream(file);
				workbook.write(fileOut);
				fileOut.close();
			}
			// Opening the workbook
			FileInputStream ft = new FileInputStream(
					new File(System.getProperty("user.dir") + "/src/test/resources/" + fileName + ""));
			XSSFWorkbook wb = new XSSFWorkbook(ft);
			Sheet sh = wb.getSheet("EnquiryMAx");

			// Create Other rows and cells with employees data

			Row row = sh.getRow(rowNum++);

			row.getCell(0).setCellValue(models);

			row.getCell(1).setCellValue(engine);

			row.getCell(2).setCellValue(colours);

			row.getCell(3).setCellValue(headlining);

			row.getCell(4).setCellValue(paymentOpt);

			row.getCell(5).setCellValue(totalDeposit);

			row.getCell(6).setCellValue(deposit);

			row.getCell(7).setCellValue(perMonth);

			row.getCell(8).setCellValue(finalValue);

			row.getCell(9).setCellValue(email);

			row.getCell(10).setCellValue(firstName);

			row.getCell(11).setCellValue(lastName);

			row.getCell(12).setCellValue(mobile);

			row.getCell(13).setCellValue(addr);

			FileOutputStream fileOut = new FileOutputStream(file);
			wb.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Map<String, String> readEcquiryMaxExcel() throws Exception {
		String fileName = "abc2.xlsx";
		String journeyName = "CJ7";
		int rowNum = 1;
		Map<String, String> enquiryMaxData = new HashMap<String, String>();
		DataFormatter df = new DataFormatter();
		FileInputStream ft = new FileInputStream(
				new File(System.getProperty("user.dir") + "/src/test/resources/" + fileName + ""));
		XSSFWorkbook wb = new XSSFWorkbook(ft);
		Sheet sh = wb.getSheet("EnquiryMAx");

		if ("CJ2".equalsIgnoreCase(journeyName)) {
			rowNum = 1;
		} else if ("CJ7".equalsIgnoreCase(journeyName)) {
			rowNum = 2;
		} else {
			rowNum = 3;
		}
		Row row = sh.getRow(rowNum);

		enquiryMaxData.put("models", df.formatCellValue(row.getCell(0)));
		enquiryMaxData.put("engine", df.formatCellValue(row.getCell(1)));
		enquiryMaxData.put("colours", df.formatCellValue(row.getCell(2)));
		enquiryMaxData.put("headlining", df.formatCellValue(row.getCell(3)));
		enquiryMaxData.put("paymentOpt", df.formatCellValue(row.getCell(4)));
		enquiryMaxData.put("totalDeposit", df.formatCellValue(row.getCell(5)));
		enquiryMaxData.put("deposit", df.formatCellValue(row.getCell(6)));
		enquiryMaxData.put("perMonth", df.formatCellValue(row.getCell(7)));
		enquiryMaxData.put("finalValue", df.formatCellValue(row.getCell(8)));
		enquiryMaxData.put("email", df.formatCellValue(row.getCell(9)));
		enquiryMaxData.put("firstName", df.formatCellValue(row.getCell(10)));
		enquiryMaxData.put("lastName", df.formatCellValue(row.getCell(11)));
		enquiryMaxData.put("mobile", df.formatCellValue(row.getCell(12)));
		enquiryMaxData.put("addr", df.formatCellValue(row.getCell(13)));

		return enquiryMaxData;
	}
}
