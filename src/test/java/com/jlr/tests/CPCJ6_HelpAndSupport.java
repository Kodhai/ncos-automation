package com.jlr.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HelpAndSupportContainer;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ6_HelpAndSupport extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static HelpAndSupportContainer helpAndSupportContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ6_HelpAndSupport.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	
	public CPCJ6_HelpAndSupport(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
		extenVerificationObj = new ExtenVerification(extentLogger);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
		helpAndSupportContainer = PageFactory.initElements(driver, HelpAndSupportContainer.class);	
	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			helpAndSupportContainer = PageFactory.initElements(driver, HelpAndSupportContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void cpcj6HelpSupport(String question) {
		try {

			cpcj6(question);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj6(String question) {
		try {
			// Click on options and select vehicle
			ActionHandler.click(homePageContainer.moreOption);

			ActionHandler.click(helpAndSupportContainer.helpSupportLink);

			ActionHandler.waitForElement(helpAndSupportContainer.helpAndSupportTitle, 15);

			assertEquals(helpAndSupportContainer.helpAndSupportTitle.getText(), Constants.HELPSUPPORTTITLE);
			extenVerificationObj.extentVerification("Help And Support Page Title Validation:",
					Constants.HELPSUPPORTTITLE, helpAndSupportContainer.helpAndSupportTitle.getText(), "");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.GENERAL + "':",
					Constants.GENERAL,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.GENERAL))).getText(), "");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.LOGINACCESS + "':",
					Constants.LOGINACCESS,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.LOGINACCESS))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.DASHBOARD + "':",
					Constants.DASHBOARD,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.DASHBOARD))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.MYPROFILE + "':",
					Constants.MYPROFILE,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.MYPROFILE))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.VEHICLEMGMT + "':",
					Constants.VEHICLEMGMT,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.VEHICLEMGMT))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.REMINDERS + "':",
					Constants.REMINDERS,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.REMINDERS))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.ADDVEHICLE + "':",
					Constants.ADDVEHICLE,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.ADDVEHICLE))).getText(),
					"");

			extenVerificationObj.extentVerification("Header Validation Of '" + Constants.HANDBOOKSGUIDES + "':",
					Constants.HANDBOOKSGUIDES,
					driver.findElement(By.xpath(helpAndSupportContainer.headerText(Constants.HANDBOOKSGUIDES)))
							.getText(),
					"");

			extentLogger.log(Status.INFO, "Selecting Question: " + question);
			driver.findElement(By.xpath(helpAndSupportContainer.questionSearch(question))).click();
			// ActionHandler.scrollAndClick(driver.findElement(By.xpath(helpAndSupportContainer.questionSearch(question))));
			ActionHandler.wait(4);

			ActionHandler.scrollToView(driver.findElement(By.xpath(helpAndSupportContainer.answer(question))));
			String answer = driver.findElement(By.xpath(helpAndSupportContainer.answer(question))).getText();
			extentLogger.log(Status.INFO, "Printing Answer: " + answer);

			assertFalse(answer.isEmpty());

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
