package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.w3c.dom.Document;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.AccessoriesContainer;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.OptionsContainer;
import com.jlr.containers.SVCRM4Container;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.models.VehicleDetails;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.ExtenVerification;

public class CJ1 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static OptionsContainer optionsContainer = null;
	private static AccessoriesContainer accessoriesContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static SVCRM4Container svcrm4Container = null;
	private static ExtenVerification extenVerificationObj = null;
	private static EnquiryMax enquiryMaxObj = null;
	public static String journeyName = "";
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ1.class);
	// CommonFunctions commonFuncObject = new CommonFunctions();
	CommonFunctionsNcos commonFuncObject = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			journeyName = context.getCurrentXmlTest().getName();
			setupTest(journeyName);
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			enquiryMaxObj = new EnquiryMax();
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			optionsContainer = PageFactory.initElements(driver, OptionsContainer.class);
			accessoriesContainer = PageFactory.initElements(driver, AccessoriesContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
			svcrm4Container = PageFactory.initElements(driver, SVCRM4Container.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ1(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String options, String convenience,
			String accessories, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {
		try {
			String finalValue = "";
			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.LANDROVER);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(4);
				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				ActionHandler.click(driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))));
			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(4);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				ActionHandler.scrollDown();
				ActionHandler.click(driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))));
			}

			if (("Yes").equals(options)) {
				ActionHandler.click(optionsContainer.optionsTab);
				ActionHandler.wait(4);
				ActionHandler.click(optionsContainer.convenienceInfotainment);
				ActionHandler.wait(4);
				ActionHandler.click(driver.findElement(By.xpath(optionsContainer.selectGivenCategory(convenience))));
			}

			if (("Yes").equals(accessories)) {
				ActionHandler.click(accessoriesContainer.accessoriesTab);
				ActionHandler.wait(4);
				// ActionHandler.click(accessoriesContainer.touringAccessories);
				ActionHandler.click(driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))));
				ActionHandler.wait(4);
			}
			ActionHandler.wait(2);
			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(4);
			LOGGER.info(homePageContainer.onTheRoadPrice.getText());
			LOGGER.info(summaryContainer.grandTotal.getText());
			assertEquals(summaryContainer.grandTotal.getText(), homePageContainer.onTheRoadPrice.getText());
			finalValue = summaryContainer.grandTotal.getText();
			// click on finance quote button
			ActionHandler.click(financeContainer.financeQuoteButton);

			// click on self finance cash purchase
			extentLogger.log(Status.INFO, "selecting finance option cash purchase");
			ActionHandler.click(financeContainer.selfFinanceCashPurchase);

			ActionHandler.click(summaryContainer.summaryTab);

			commonFuncObject.pdfValidation(Constants.LANDROVER, models, engine, specif, nextStep, exterior, colours,
					visualPacks, interior, headlining, options, "infotainment", "jaguarGear", "extAccessories");
			// Personal details
		/*	ActionHandler.click(summaryContainer.sendToRetailer);
			extentLogger.log(Status.INFO, "Filling Personal details");
			String oldTab = commonFuncObject.completePersonalDetails_R2(Constants.LANDROVER);
*/
			/*enquiryMaxObj.createEnquiryMaxExcel(defaultTerm, models, engine, colours, headlining, paymentOpt, "", "",
					"", finalValue, Config.getPropertyValue("email"), Config.getPropertyValue("firstName"),
					Config.getPropertyValue("lastName"), Config.getPropertyValue("mobileNo"),
					Config.getPropertyValue("address"));*/

			// Switching to Parent window
		//	driver.switchTo().window(oldTab);
			
		} catch (Exception e) {
			extentLogger.error("Exception - ncosCJ1 : " + e);
		}
	}

	public VehicleDetails getDetails(VehicleDetails vehicleDetails, String models, String engine, String specif,
			String nextStep, String exterior, String colours, String visualPacks, String interior, String headlining,
			String options, String convenience, String accessories, String touringAcc, String paymentOpt,
			String advRetail, String defaultAr, String term, String defaultTerm, String annualMileage,
			String defaultMileage, String deposit, String defaultDeposit) {

		vehicleDetails.setModels(models);
		vehicleDetails.setEngines(engine);
		vehicleDetails.setSpecificationPack(specif);
		vehicleDetails.setPaymentOption(nextStep);
		vehicleDetails.setColours(colours);
		vehicleDetails.setVisualPack(visualPacks);
		vehicleDetails.setInterior(interior);
		vehicleDetails.setHeadlining(headlining);
		vehicleDetails.setOptions(options);
		vehicleDetails.setConvenienceAndInfotainment(convenience);
		vehicleDetails.setAccessories(accessories);
		vehicleDetails.setTouringaccessories(touringAcc);
		vehicleDetails.setPaymentOption(paymentOpt);
		vehicleDetails.setAdvRetail(advRetail);
		vehicleDetails.setDefault_AR(defaultAr);
		vehicleDetails.setTerm(term);
		vehicleDetails.setDefaultTerm(defaultTerm);
		vehicleDetails.setAnnualMileage(annualMileage);
		vehicleDetails.setDefault_MIL(defaultMileage);
		vehicleDetails.setDeposit(deposit);
		vehicleDetails.setDefaultDeposit(defaultDeposit);

		return vehicleDetails;
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}



	public void avokaTransact(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String options, String convenience,
			String accessories, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit,
			String summaryBasePrice) {
		try {

			driver.get(Config.getPropertyValue("AVOKA.url"));
			ActionHandler.setText(svcrm4Container.avokaID, Config.getPropertyValue("valid.avokaEmail"));
			ActionHandler.setText(svcrm4Container.avokaPass, Config.getPropertyValue("valid.avokaPassword"));
			ActionHandler.click(svcrm4Container.avokaSubButton);
			ActionHandler.click(svcrm4Container.avokaTrans); // click on latest
																// transaction
			ActionHandler.click(svcrm4Container.avokaXMLMenu);
			ActionHandler.wait(4);
			Actions action = new Actions(driver);
			ActionHandler.click(svcrm4Container.avokaXMLEle); // click on xml
																// element to
																// set focus on
																// xml code.
			action.doubleClick();
			action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).keyDown(Keys.CONTROL).sendKeys("c")
					.keyUp(Keys.CONTROL).build().perform();

			// copy clipboard value into string variable
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable contents = clipboard.getContents(null);
			String str = (String) contents.getTransferData(DataFlavor.stringFlavor);

			// create xml file from string
			File newTextFile = new File(System.getProperty("user.dir") + "/src/test/resources/thexmlfile.xml");
			FileWriter fw = new FileWriter(newTextFile);
			fw.write(str);
			fw.close();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(newTextFile);

			extenVerificationObj.extentVerification("Email ID in XML", Config.getPropertyValue("email"),
					doc.getElementsByTagName("AccountEmailAddress").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("First name in XML ", Config.getPropertyValue("firstName"),
					doc.getElementsByTagName("AccountFirstName").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Last name in XML ", Config.getPropertyValue("lastName"),
					doc.getElementsByTagName("AccountLastName").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Mobile number in XML ", Config.getPropertyValue("mobileNo"),
					doc.getElementsByTagName("AccountMobilePhoneNumber").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Lead Vehicle Use Type in XML ",
					Config.getPropertyValue("buisnessUseOption"),
					doc.getElementsByTagName("LeadVehicleUseType").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Car Engine in XML ", true,
					doc.getElementsByTagName("Detail").item(3).getTextContent().contains(engine), "Yes");
			extenVerificationObj.extentVerification("Car Colour in XML ", true,
					doc.getElementsByTagName("Detail").item(5).getTextContent().contains(colours), "Yes");
			extenVerificationObj.extentVerification("Car headlining in XML ", true,
					doc.getElementsByTagName("Detail").item(7).getTextContent().contains(headlining), "Yes");
			extenVerificationObj.extentVerification("Car Country Code in XML ", "GB",
					doc.getElementsByTagName("CountryCode").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Car Model in XML ", models,
					doc.getElementsByTagName("ModelDerivative").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Car Base Price in XML ", "",
					doc.getElementsByTagName("BasePrice").item(0).getTextContent(), "Yes");

		} catch (Exception e) {
			extentLogger.error("Exception - avoka : " + e);
		}

	}

}
