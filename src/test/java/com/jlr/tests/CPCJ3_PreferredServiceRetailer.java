package com.jlr.tests;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.CPcontainers.PreferredServiceRetailerContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ3_PreferredServiceRetailer extends TestBase {

	private static PreferredServiceRetailerContainer preferredServiceRetailerContainer = null;
	private static HomePageContainer homePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ3_PreferredServiceRetailer.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;

	public CPCJ3_PreferredServiceRetailer(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUAREPACE);
		extenVerificationObj = new ExtenVerification(extentLogger);
		preferredServiceRetailerContainer = PageFactory.initElements(driver,
				PreferredServiceRetailerContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

	}
	
	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUAREPACE);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			preferredServiceRetailerContainer = PageFactory.initElements(driver,
					PreferredServiceRetailerContainer.class);
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void preferredServiceRetailer(String postORAddrCode, String retailer) {
		try {

			cpcj3(postORAddrCode, retailer);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj3(String postORAddrCode, String retailer) throws Exception {
		try {

			ActionHandler.wait(2);
			extenVerificationObj.extentInfo("Click on Preferred service retailer");
			// Click on Preferred Service retailer
			if (!VerifyHandler.verifyElementPresent(preferredServiceRetailerContainer.serviceRetailerextendLink)) {
				ActionHandler.scrollToView(preferredServiceRetailerContainer.serviceRetailerLink);
				ActionHandler.click(preferredServiceRetailerContainer.serviceRetailerLink);
			}

			try {
				if (preferredServiceRetailerContainer.changeRetailerButton.isDisplayed()) {
					extentLogger.log(Status.INFO, "Clicking on changed retailer");
					ActionHandler.click(preferredServiceRetailerContainer.changeRetailerButton);
					ActionHandler.wait(2);
				}
			} catch (NoSuchElementException e) {
				extentLogger.error("Change Retailer Button is not displayed!!!  ");
			}
			

			// Verifying car info on book A service page
			extenVerificationObj.extentInfo("enter valid postal or Address code and click search button");
			ActionHandler.clearAndSetText(preferredServiceRetailerContainer.changeRetailerTextBox, postORAddrCode); // text
			ActionHandler.click(preferredServiceRetailerContainer.changeRetailerSearchBtn);
			ActionHandler.wait(2);
			List<WebElement> retailerEle = preferredServiceRetailerContainer.nearestRetailerList;
			for (WebElement nearestRetailer : retailerEle) {
				if (retailer.equalsIgnoreCase(nearestRetailer.getText())) {
					nearestRetailer.click();
					ActionHandler.wait(2);
					extenVerificationObj.extentVerification("Retailer Saved Message verification: ", true,
							VerifyHandler.verifyElementPresent(homePageContainer.notificationMsg), "Yes");
					extenVerificationObj.extentVerification("Retailer Saved Message verification:",
							Constants.RETAILERSAVED, homePageContainer.notificationMsg.getText(), "Yes");
					break;
				}
			}

			extenVerificationObj.extentVerification("Verifying Retailer Name", retailer,
					ActionHandler.getElementText(preferredServiceRetailerContainer.retailerName), "");

			extenVerificationObj.extentVerification("verifying GET DIRECTION button", true, VerifyHandler
					.verifyElementPresent(preferredServiceRetailerContainer.retailerServiceDirectionButton), "");
			// Get Direction
			ActionHandler.click(preferredServiceRetailerContainer.retailerServiceDirectionButton);
			commonFuncObject.getDirections();
		} catch (Exception e) {
			extenVerificationObj.extentError("cpcj3 - Exception :: " + e.toString());
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
