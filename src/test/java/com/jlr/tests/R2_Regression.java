package com.jlr.tests;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.AccessoriesContainer;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.OptionsContainer;
import com.jlr.containers.SVCRM4Container;
import com.jlr.containers.ShoppingBasketContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class R2_Regression extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static OptionsContainer optionsContainer = null;
	private static AccessoriesContainer accessoriesContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static SVCRM4Container svcrm4Container = null;
	private static ExtenVerification extenVerificationObj = null;
	private static ShoppingBasketContainer shoppingBasketContainer = null;
	private static EnquiryMax enquiryMaxObj = null;
	public static String journeyName = "";
	private WebDriver driver;
	public ExtentTest extentLogger;
	public static int testCaseNum = 0;
	private static final Logger LOGGER = LoggerFactory.getLogger(R2_Regression.class);
	// CommonFunctions commonFuncObject = new CommonFunctions();
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			++testCaseNum;
			journeyName = context.getCurrentXmlTest().getName() + "_" + testCaseNum;
			setupTest(journeyName);
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			enquiryMaxObj = new EnquiryMax();

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			optionsContainer = PageFactory.initElements(driver, OptionsContainer.class);
			accessoriesContainer = PageFactory.initElements(driver, AccessoriesContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
			svcrm4Container = PageFactory.initElements(driver, SVCRM4Container.class);
			shoppingBasketContainer = PageFactory.initElements(driver, ShoppingBasketContainer.class);
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdataR2", dataProviderClass = DataReader.class)
	public void r2_regression(String serial_Number, String url, String namePlate, String models, String engine,
			String specif, String nextStep, String exterior, String colours, String visualPacks, String interior,
			String headlining, String options, String convenience, String accessories, String touringAcc,
			String paymentOpt, String px, String pxRegistration, String mileage, String condition, String advRetail,
			String defaultAr, String term, String defaultTerm, String annualMileage, String defaultMileage,
			String deposit, String defaultDeposit, String basketChoice, String Register, String emailId, String fName,
			String lName, String password, String logId, String pwdDelMethod, String retailerName, String telEmail) {
		try {
			// TODO - Change last parameter according to excel
			commonFuncObject.userLogin(url);
			// shoppingBasket("shoppingBasket");

			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.LANDROVER);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				ActionHandler.click(driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))));
				ActionHandler.wait(4);
				if (StringUtils.isNotBlank(visualPacks) && !"NA".equals(visualPacks)) {
					ActionHandler.click(exteriorContainer.visualPacks);
					ActionHandler.wait(4);
					ActionHandler
							.click(driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))));
				}
			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(4);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				ActionHandler.scrollDown();
				ActionHandler.click(driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))));
				ActionHandler.wait(2);
			}

			if (("Yes").equals(options)) {
				ActionHandler.click(optionsContainer.optionsTab);
				ActionHandler.wait(4);
				if (VerifyHandler.verifyElementPresent(accessoriesContainer.jaguarGearTab)) {
					if (VerifyHandler.verifyElementPresent(optionsContainer.infotainment)) {
						ActionHandler.click(optionsContainer.infotainment);
					} else if (VerifyHandler.verifyElementPresent(optionsContainer.convenience)) {
						ActionHandler.click(optionsContainer.convenience);
					}
				} else {
					if (VerifyHandler.verifyElementPresent(optionsContainer.convenienceInfotainment)) {
						ActionHandler.click(optionsContainer.convenienceInfotainment);
					} else {
						ActionHandler.click(optionsContainer.convenience);
					}
				}
				ActionHandler.wait(4);
				ActionHandler.click(driver.findElement(By.xpath(optionsContainer.selectGivenCategory(convenience))));
			}

			if (("Yes").equals(accessories)) {
				if (VerifyHandler.verifyElementPresent(accessoriesContainer.jaguarGearTab)) {
					ActionHandler.click(accessoriesContainer.jaguarGearTab);
				} else {
					ActionHandler.click(accessoriesContainer.accessoriesTab);
				}
				ActionHandler.wait(4);
				int counter = 0;
				// ActionHandler.scrollToView(driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))));
				if (StringUtils.isNotBlank(touringAcc) && !"NA".equals(touringAcc)) {
					ActionHandler.click(accessoriesContainer.touringAccessories);
					ActionHandler.wait(3);
					if (!VerifyHandler.verifyElementPresent(
							driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))))) {
						while (VerifyHandler.verifyElementPresent(
								driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))))) {
							ActionHandler.pageScrollDown();
							ActionHandler.wait(1);
							counter++;
							if (counter > 12) {
								break;
							}
						}
					}
					ActionHandler
							.click(driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))));
					ActionHandler.wait(4);
				}
			}
			ActionHandler.wait(2);
			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(4);

			// click on finance quote button
			ActionHandler.click(financeContainer.financeQuoteButton);
			ActionHandler.wait(3);

			extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);
			ActionHandler.click(driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));

			ActionHandler.scrollToView(
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
			ActionHandler.wait(2);

			if (!"NA".equals(term)) {
				moveSlider(Constants.TERM, term);
			}
			if (!"NA".equals(annualMileage)) {
				moveSlider(Constants.ANNUALMILEAGE, annualMileage);
			}

			if ("Y".equals(px)) {
				// Part Exchange Validations
				commonFuncObject.partExchange(pxRegistration, condition, mileage);
				ActionHandler.click(financeContainer.useTowardDepositButton);
				ActionHandler.wait(3);

			} else {
				if (StringUtils.isNotBlank(defaultDeposit) && !"NA".equals(defaultDeposit)) {
					double otr = Double
							.parseDouble(homePageContainer.onTheRoadPrice.getText().replaceAll("[^\\d.]", ""));
					int depositPercent = Integer.parseInt(defaultDeposit);
					double val = (otr * depositPercent) / 100;
					ActionHandler.setText(financeContainer.totalDepositDefault, String.valueOf(Math.round(val)));
					ActionHandler.wait(3);
				}

			}
			ActionHandler.click(financeContainer.getYourQuote);

			StringBuilder sb = new StringBuilder();
			sb.append(serial_Number);
			sb.append(".");
			sb.append(journeyName);
			sb.append(".");
			sb.append(namePlate);
			sb.append(".");
			sb.append(models);
			sb.append(".");
			sb.append(engine);
			sb.append(".");
			sb.append(specif);
			sb.append(".");

			ActionHandler.wait(4);
			extentLogger.info("Before logOut from Portal: " + sb.toString(),
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot(sb.toString())).build());
			ActionHandler.wait(2);
			ActionHandler.pageScrollDown();
			// takeFullScreenshots(sb);
			extentLogger.info("Before logOut from Portal: " + sb.toString(),
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot(sb.toString())).build());
			ActionHandler.wait(2);
			// STOP HERE
			sendToRetailerBasket(basketChoice, Register, emailId, fName, lName, password, logId, pwdDelMethod,
					retailerName, telEmail);
		} catch (Exception e) {
			extentLogger.error("Exception - R2_Regression : " + e);
			LOGGER.error("Exception - R2_Regression : ", e);
		}
	}

	public void takeFullScreenshots(StringBuilder sb) {
		final Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(500))
				.takeScreenshot(driver);
		final BufferedImage image = screenshot.getImage();
		try {
			ImageIO.write(image, "jpg", new File("C:/screenshots/" + sb.toString() + ".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void moveSlider(String sliderName, String newVal) {

		int displayedValue = 0;

		if (Constants.TOTALDEPOSIT.equals(sliderName)) {
			displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
		} else if (Constants.TERM.equals(sliderName)) {
			displayedValue = getValue(ActionHandler.locateElement(
					"//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
					"xpath"), false);
		} else {
			displayedValue = getValue(ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"),
					false);
		}

		int newValue = Integer.parseInt(newVal);
		/**
		 * displayedValueTemp is used To exit loop in case of slider boundary
		 * value reached. conditions
		 */
		int displayedValueTemp = displayedValue;
		if (displayedValue < newValue) {
			while (displayedValue < newValue) {
				System.out.println("Moving slider to right, displayedValue:" + displayedValue);
				if (Constants.TOTALDEPOSIT.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.totalDepositSliderRight);
					displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
				} else if (Constants.TERM.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.termSliderRight);
					displayedValue = getValue(ActionHandler.locateElement(
							"//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
							"xpath"), false);
				} else {
					ActionHandler.clickElement(financeContainer.annualMileageSliderRight);
					displayedValue = getValue(
							ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"), false);
				}
				if (displayedValueTemp == displayedValue) {
					break;
				} else {
					displayedValueTemp = displayedValue;
				}
			}
		} else {
			while (displayedValue > newValue) {
				System.out.println("Moving slider to left, displayedValue:" + displayedValue);
				if (Constants.TOTALDEPOSIT.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.totalDepositSliderLeft);
					displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
				} else if (Constants.TERM.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.termSliderLeft);
					displayedValue = getValue(ActionHandler.locateElement(
							"//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
							"xpath"), false);
				} else {
					ActionHandler.clickElement(financeContainer.annualMileageSliderLeft);
					displayedValue = getValue(
							ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"), false);
				}
				if (displayedValueTemp == displayedValue) {
					break;
				} else {
					displayedValueTemp = displayedValue;
				}
			}
		}

	}

	private int getValue(WebElement w, boolean flag) {

		String s = "";
		if (StringUtils.isNotBlank(w.getAttribute("value"))) {
			s = w.getAttribute("value");
		} else {
			s = w.getText();
		}
		String[] a = s.split(" ");
		if (flag) {
			return Integer.parseInt(a[1].replaceAll(",", ""));
		}
		return Integer.parseInt(a[0].replaceAll(",", ""));
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		//// commonFuncObject.userLogsOut();
		getDriver().quit();
	}

	

	public void sendToRetailerBasket(String basketChoice, String Register, String emailId, String fName, String lName,
			String password, String logId, String pwdDelMethod, String retailerName, String telEmail) {
		try {
			// telEmail = "Email";

			extentLogger.info("Click on 'QA Order This Vehicle Link' Button");
			ActionHandler.click(financeContainer.orderVehicleLink);
			ActionHandler.wait(2);
			// ######################################################################################################################
			extentLogger.info("Click on Send To Retailer Button");
			ActionHandler.waitForElement(shoppingBasketContainer.continueBtn, 10);
			ActionHandler.click(shoppingBasketContainer.sendToRetailerLink);
			extentLogger.info("Navigate to delivery method Log in page");

			extentLogger.info("Select Log in Option");
			// ActionHandler.wait(15);
			deliveryMethodLogin(logId, pwdDelMethod);
			// ##########################################################
			ActionHandler.wait(15);
			if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.sendToRetailerLink)) {
				ActionHandler.click(shoppingBasketContainer.sendToRetailerLink, 60);
				ActionHandler.wait(10);
			}
			extentLogger.info("Selecting " + telEmail + " Option");
			ActionHandler.click(driver.findElement(By.xpath(shoppingBasketContainer.retailerContactPref(telEmail))),
					10);

			extentLogger.info("click on Send To Retailer Button");
			ActionHandler.click(shoppingBasketContainer.retailerTermsCond);
			ActionHandler.click(shoppingBasketContainer.sendToRetailerBtn);
			ActionHandler.wait(10);

			extenVerificationObj.extentVerification("Validate success meassage", true,
					VerifyHandler.verifyElementPresent(shoppingBasketContainer.retilerSuccessMsg), "Yes");

			extentLogger.info("Reference number generated : " + shoppingBasketContainer.refNumTxt.getText());

			extentLogger.info("Send to retailer final page : ", MediaEntityBuilder
					.createScreenCaptureFromPath(Utility.takeScreenShot("RetailerFinalPage")).build());
		} catch (Exception e) {
			LOGGER.error("Exception - sendToRetailerBasket : " + e);
		}
	}

	public void deliveryMethodLogin(String userName, String password) {
		try {
			/*
			 * (new WebDriverWait(getDriver(),
			 * 60)).until(ExpectedConditions.visibilityOf(
			 * driver.findElement(By.
			 * xpath("//div[@class='loading-mask'][@style='display: none;']"))))
			 * ;
			 */
			/*
			 * ActionHandler.waitForElement( driver.findElement(By.
			 * xpath("//div[@class='loading-mask'][@style='display: none;']")),
			 * 120);
			 */
			ActionHandler.wait(17);
			ActionHandler.click(shoppingBasketContainer.lonInRadioBtn, 60);
			ActionHandler.wait(3);
			extentLogger.info("Login with " + userName);
			shoppingBasketContainer.emailTxtBox.sendKeys(userName);
			shoppingBasketContainer.passwordTxtBox.sendKeys(password);
			ActionHandler.click(shoppingBasketContainer.loginBtn);
		} catch (Exception e) {
			LOGGER.error("Exception : " + e);
		}
	}
}
