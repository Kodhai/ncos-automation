package com.jlr.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.CPcontainers.MyAccountContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ2_ProfileMgmt extends TestBase {

	private static MyAccountContainer myAccountContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ2_ProfileMgmt.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	private static HomePageContainer homePageContainer = null;
	
	public CPCJ2_ProfileMgmt(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUARIPACE);
		extenVerificationObj = new ExtenVerification(extentLogger);
		myAccountContainer = PageFactory.initElements(driver, MyAccountContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUARIPACE);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			myAccountContainer = PageFactory.initElements(driver, MyAccountContainer.class);
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void cpcj2PersonalInfo(String firstName, String lastName, String mobile, String buildName, String cityName,
			String country, String postCod, String oldPassword, String newPassword, String distanceUnit,
			String dateFormat) {
		try {

			cpcj2(firstName, lastName, mobile, buildName, cityName, country, postCod, oldPassword, newPassword,
					distanceUnit, dateFormat);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj2(String firstName, String lastName, String mobile, String buildName, String cityName,
			String country, String postCod, String oldPassword, String newPassword, String distanceUnit,
			String dateFormat) {
		try {
			// Click on My account Link
		/*	ActionHandler.scrollToView(myAccountContainer.myAccountLink);
			ActionHandler.click(myAccountContainer.myAccountLink);*/
			ActionHandler.click(homePageContainer.moreOption);
			ActionHandler.wait(2);
			ActionHandler.click(homePageContainer.myAccount);
			ActionHandler.wait(3);
			if (VerifyHandler.verifyElementPresent(myAccountContainer.personalInfoExpandLink)) {
				ActionHandler.click(myAccountContainer.personalInfoExpandLink);
			}

			updateAndVerifyPersonalInfo(firstName, lastName, mobile, buildName, cityName, country, postCod);

			if (VerifyHandler.verifyElementPresent(myAccountContainer.securityExpandLink)) {
				ActionHandler.click(myAccountContainer.securityExpandLink);
			}

			if (!"NA".equals(oldPassword) && !"NA".equals(newPassword)) {
				ActionHandler.setText(myAccountContainer.securityOldPassword, oldPassword);
				ActionHandler.setText(myAccountContainer.securityPassword, newPassword);
				ActionHandler.click(myAccountContainer.securitySaveButton);

				ActionHandler.wait(1);
				extenVerificationObj.extentVerification("Verifying Update Password Saved Message ", true,
						VerifyHandler.verifyElementPresent(myAccountContainer.personalInfoSavedMsg), "");
			}

			if (VerifyHandler.verifyElementPresent(myAccountContainer.preferenceExpandLink)) {
				ActionHandler.click(myAccountContainer.preferenceExpandLink);
			}

			if (!"NA".equals(distanceUnit)) {
				ActionHandler.click(myAccountContainer.preferenceDistUnit);
				ActionHandler.selectByVisibleText((myAccountContainer.preferenceDistUnit), distanceUnit);
				ActionHandler.click(myAccountContainer.preferenceDistUnit);
			}
			if (!"NA".equals(dateFormat)) {
				ActionHandler.click(myAccountContainer.preferenceDateFormat);
				ActionHandler.selectByVisibleText((myAccountContainer.preferenceDateFormat), dateFormat);
				ActionHandler.click(myAccountContainer.preferenceDateFormat);
			}

			if (!"NA".equals(distanceUnit) || !"NA".equals(dateFormat)) {
				ActionHandler.click(myAccountContainer.preferenceButton);
				ActionHandler.wait(1);
				extenVerificationObj.extentVerification("Verifying preference Saved Message ", true,
						VerifyHandler.verifyElementPresent(myAccountContainer.personalInfoSavedMsg), "");

				// Verifying preference data
				if (!"NA".equals(distanceUnit)) {
					Select sel = new Select(myAccountContainer.preferenceDistUnit);
					extenVerificationObj.extentVerification("verifying distance Unit",
							sel.getFirstSelectedOption().getText(), distanceUnit, "");
				}
				if (!"NA".equals(dateFormat)) {
					Select sel = new Select(myAccountContainer.preferenceDateFormat);
					extenVerificationObj.extentVerification("verifying preference Date Format",
							sel.getFirstSelectedOption().getText(), dateFormat, "");
				}
			}
		} catch (Exception e) {
			extentLogger.error("CPCJ2 Exception ::  " + e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

	public void updateAndVerifyPersonalInfo(String firstName, String lastName, String mobile, String buildName,
			String cityName, String country, String postCode) {
		try {
			boolean flag = false;
			extentLogger.info("Updating personal info ");

			if (!"NA".equals(firstName)) {
				ActionHandler.setText(myAccountContainer.personalInfoFName, firstName);
				flag = true;
			}
			if (!"NA".equals(lastName)) {
				ActionHandler.setText(myAccountContainer.personalInfoLName, lastName);
				flag = true;
			}
			if (!"NA".equals(mobile)) {
				ActionHandler.setText(myAccountContainer.personalInfoMobile, mobile);
				flag = true;
			}
			if (!"NA".equals(buildName)) {
				ActionHandler.setText(myAccountContainer.personalInfoBuildName, buildName);
				flag = true;
			}
			if (!"NA".equals(cityName)) {
				ActionHandler.setText(myAccountContainer.personalInfoCity, cityName);
				flag = true;
			}
			if (!"NA".equals(country)) {
				ActionHandler.click(myAccountContainer.personalInfoCountry);
				ActionHandler.selectByVisibleText((myAccountContainer.personalInfoCountry), country);
				flag = true;
			}
			if (!"NA".equals(postCode)) {
				ActionHandler.setText(myAccountContainer.personalInfoPostCode, postCode); // w21ea
				flag = true;
			}

			if (flag == true) {
				ActionHandler.click(myAccountContainer.personalInfoSaveButton);

				ActionHandler.wait(1);
				extenVerificationObj.extentVerification("Verifying Personal Info Saved Message ", true,
						VerifyHandler.verifyElementPresent(myAccountContainer.personalInfoSavedMsg), "");

				// Verified saved data
				extentLogger.info("Validating personal info ");
				if (!"NA".equals(firstName)) {
					extenVerificationObj.extentVerification("verifying FirstName",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoFName, "value"), firstName,
							"");
				}
				if (!"NA".equals(lastName)) {
					extenVerificationObj.extentVerification("verifying LastName",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoLName, "value"), lastName,
							"");
				}
				if (!"NA".equals(mobile)) {
					extenVerificationObj.extentVerification("verifying Mobile",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoMobile, "value"), mobile,
							"");
				}
				if (!"NA".equals(buildName)) {
					extenVerificationObj.extentVerification("Verifying Buid Name",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoBuildName, "value"),
							buildName, "");
				}
				if (!"NA".equals(cityName)) {
					extenVerificationObj.extentVerification("Verifying City Name",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoCity, "value"), cityName,
							"");
				}
				if (!"NA".equals(country)) {
					Select sel = new Select(myAccountContainer.personalInfoCountry);
					extenVerificationObj.extentVerification("verifying country Name",
							sel.getFirstSelectedOption().getText(), country, "");
				}
				if (!"NA".equals(postCode)) {
					extenVerificationObj.extentVerification("verifying postCode",
							ActionHandler.getAttributeValue(myAccountContainer.personalInfoPostCode, "value"), postCode,
							"");
				}
			}

		} catch (Exception e) {
			extentLogger.error("Exception - updateAndVerifyPErsonalInfo :: " + e);
		}
	}
}
