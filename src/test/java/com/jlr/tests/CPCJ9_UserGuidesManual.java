package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HandbookManualContainer;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ9_UserGuidesManual extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static HandbookManualContainer handbookManualContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ9_UserGuidesManual.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	
	public CPCJ9_UserGuidesManual(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
		extenVerificationObj = new ExtenVerification(extentLogger);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
		handbookManualContainer = PageFactory.initElements(driver, HandbookManualContainer.class);

	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("Starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			handbookManualContainer = PageFactory.initElements(driver, HandbookManualContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void cpcj9UserGuidesAndManual(String vehicleName, String language, String country) {
		try {

			cpcj9(vehicleName, language, country);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj9(String vehicleName, String language, String country) {
		try {
			// Click on options and select vehicle
			ActionHandler.click(homePageContainer.expandIcon);
			driver.findElement(By.xpath(homePageContainer.vehicleSelection(vehicleName))).click();
			extentLogger.log(Status.INFO, "Selecting Vehicle: " + vehicleName);

			ActionHandler.wait(3);
			assertEquals(homePageContainer.vehicleNameHeader.getText(), vehicleName);
			extenVerificationObj.extentVerification("Verify Vehicle Selection",
					homePageContainer.vehicleNameHeader.getText(), vehicleName, "");

			extenVerificationObj.extentVerification("Handbook And Manual Header Verification: ",
					homePageContainer.handbookManualHeader.getText(), Constants.HANDBOOKHEADER, "Yes");

			String oldTab = driver.getWindowHandle();
			ActionHandler.click(homePageContainer.exploreHandbookManualLink);
			ActionHandler.wait(3);
			extentLogger.log(Status.INFO, "Clicked on Handbooks And Manual Link");

			List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(newTab.get(1));
			ActionHandler.wait(4);
			// Select Language
			extentLogger.log(Status.INFO, "Selecting Language: " + language);
			driver.findElement(By.xpath(handbookManualContainer.countryLangSelection(language))).click();
			// Select Country
			ActionHandler.wait(2);
			extentLogger.log(Status.INFO, "Selecting Country: " + country);
			driver.findElement(By.xpath(handbookManualContainer.countryLangSelection(country))).click();
			ActionHandler.waitForElement(handbookManualContainer.selectVehicleText, 20);
			assertEquals(handbookManualContainer.selectVehicleText.getText(), "SELECT YOUR VEHICLE AND MODEL YEAR");
			extenVerificationObj.extentVerification("Verifying Page loading: ", "SELECT YOUR VEHICLE AND MODEL YEAR",
					handbookManualContainer.selectVehicleText.getText(), "Yes");
			ActionHandler.wait(2);

			// Switch to parent window
			driver.switchTo().window(oldTab);
			extentLogger.log(Status.INFO, "Navigating To Parent Window");

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
