package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.AddVehicleContainer;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ5_AddVehicle extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static AddVehicleContainer addVehiclePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ5_AddVehicle.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;

	public CPCJ5_AddVehicle() {
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
		extenVerificationObj = new ExtenVerification(extentLogger);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
		addVehiclePageContainer = PageFactory.initElements(driver, AddVehicleContainer.class);
	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			addVehiclePageContainer = PageFactory.initElements(driver, AddVehicleContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void cpcj5AddVehicle(String vehicleName, String vinNumber, String engineNumber, String ownerDriver) {
		try {

			cpcj5(vehicleName, vinNumber, engineNumber, ownerDriver);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj5(String vehicleName, String vinNumber, String engineNumber, String ownerDriver) {
		try {
			// Click on options and select vehicle

			ActionHandler.click(homePageContainer.moreOption);

			ActionHandler.click(homePageContainer.addVehicle);

			ActionHandler.wait(3);

			assertEquals(addVehiclePageContainer.addVehiceTitle.getText(), "ADD A VEHICLE");

			ActionHandler.setText(addVehiclePageContainer.vinNumber, vinNumber);
			extentLogger.log(Status.INFO, "Entering VIN: " + vinNumber);

			driver.findElement(By.xpath(addVehiclePageContainer.ownerDriverSelection(ownerDriver))).click();

			ActionHandler.click(addVehiclePageContainer.findMyVehicle);
			extentLogger.log(Status.INFO, "Clicking on Find My Vehicle");

			ActionHandler.waitForElement(addVehiclePageContainer.engineNumber, 20);

			ActionHandler.setText(addVehiclePageContainer.engineNumber, engineNumber);
			extentLogger.log(Status.INFO, "Entering Engine Number: " + engineNumber);

			ActionHandler.click(addVehiclePageContainer.addToMyGarage);

			ActionHandler.waitForElement(homePageContainer.notificationMsg, 10);

			LOGGER.info("Msg:" + homePageContainer.notificationMsg.getText());
/*			extenVerificationObj.extentVerification("Adding New Vehicle Verification:", Constants.ADDINGVEHICLEMSG,
					homePageContainer.notificationMsg.getText(), "");
			ActionHandler.wait(1);
			extenVerificationObj.extentVerification("Vehicle Added Successfully message Verification:",
					Constants.NEWVEHICLEADDEDSUCCESSMSG, homePageContainer.notificationMsg.getText(), "Yes");*/
			extenVerificationObj.extentVerification("Notification Message:",VerifyHandler.verifyElementPresent(homePageContainer.notificationMsg),true,"Yes");
			
			ActionHandler.wait(4);
			
			// Go To My Garage and select Vehicle
			ActionHandler.click(homePageContainer.expandIcon);
			driver.findElement(By.xpath(homePageContainer.vehicleSelection(vehicleName))).click();
			ActionHandler.wait(3);
			assertEquals(homePageContainer.vehicleNameHeader.getText(), vehicleName);
			extenVerificationObj.extentVerification("Verify Vehicle Selection",
					homePageContainer.vehicleNameHeader.getText(), vehicleName, "");

			assertEquals(homePageContainer.vinNumber.getText(), vinNumber);
			extenVerificationObj.extentVerification("VIN Verification:", vinNumber,
					homePageContainer.vinNumber.getText(), "");

			assertEquals(homePageContainer.engineNumber.getText(), engineNumber);
			extenVerificationObj.extentVerification("Engine Number Verification:", engineNumber,
					homePageContainer.engineNumber.getText(), "");

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
