package com.jlr.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.Magento_AdminContainer;
import com.jlr.containers.VISTAContainer;
import com.jlr.utilities.CommonFunctionsNcos;

public class MailinatorEmailTesting {

	private static WebDriver driver;
	public static ExtentTest extentLogger;
	public static CommonFunctionsNcos commonFuncObject = null;
	private static Magento_AdminContainer Magento_AdminContainer = null;
	private static VISTAContainer VISTAContainer = null;
	public static void salesOrder() throws Exception {
		driver.findElement(By.xpath("(//span[text()='Orders'])[1]")).click();
		ActionHandler.wait(10);
		extentLogger.info("Customer name validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Customer_Name")).build());
	}
	public static void main(String args[]) {
		String Brand = "Landrover";
		String flow = "LTO";
		String Res_ID = "";
		
		//Open VISTA URL
		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://www.jlrvistaqa1.jlrint.com/Vista-NewWeb/rest/HomeController/VIEW/dashboard");
		ActionHandler.wait(30);
		
		//Choose Identity
		//driver.findElement(By.xpath("//*[@tooltip='Change Identity']")).click();
		driver.findElement(By.xpath("(//*[@class='no-resize'])[1]")).click();
		ActionHandler.wait(2);
		driver.switchTo().activeElement();
		ActionHandler.wait(2);
		
		if (Brand.equalsIgnoreCase("Jaguar")) {
			driver.findElement(By.xpath("//*[@class='ng-valid ng-dirty ng-touched']")).click();
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJImarket")).clear();
			driver.findElement(By.id("SAJImarket")).sendKeys("United Kingdom");
			driver.findElement(By.id("SAJImarket")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJIdistributor")).clear();
			driver.findElement(By.id("SAJIdistributor")).sendKeys("J2060 - Jaguar UK Ltd");
			driver.findElement(By.id("SAJIdistributor")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJIdealer")).clear();
			driver.findElement(By.id("SAJIdealer")).sendKeys("J0793 - Lancaster Jaguar, Wolverhampton");
			driver.findElement(By.id("SAJIdealer")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
		}
		
		if (Brand.equalsIgnoreCase("Landrover")) {
			driver.findElement(By.xpath("//*[@class='ng-valid ng-dirty ng-touched ng-valid-parse']")).click();
			ActionHandler.wait(2);
			driver.findElement(By.id("SALImarket")).clear();
			driver.findElement(By.id("SALImarket")).sendKeys("United Kingdom");
			driver.findElement(By.id("SALImarket")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SALIdistributor")).clear();
			driver.findElement(By.id("SALIdistributor")).sendKeys("M2400 - Land Rover");
			driver.findElement(By.id("SALIdistributor")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SALIdealer")).clear();
			driver.findElement(By.id("SALIdealer")).sendKeys("H0875 - Lancaster Land Rover, Wolverhampton");
			driver.findElement(By.id("SALIdealer")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
		}
			
		//Save identity
		//driver.findElement(By.xpath("(//button[@class='btn form-btn'])[1]")).click();
		driver.findElement(By.xpath("//input[@class='ng-pristine ng-untouched ng-valid'][1]")).click();
		ActionHandler.wait(10);
		
		//Choose LTO & BTO 
		driver.findElement(By.xpath("(//*[@ng-click='notificationCountClick(notification.notificationType)'])[1]")).click();
		ActionHandler.wait(2);
		driver.findElement(By.xpath("(//*[contains(text(),'Ordered Online - Validate Configuration')])[1]")).click();
		
		//Sort Reservation Ids
		driver.findElement(By.xpath("//span[@id='orderlist_title']")).click();
		ActionHandler.wait(1);
		driver.findElement(By.xpath("//*[@class='glyphicon glyphicon-arrow-up']")).click();
		ActionHandler.wait(1);
		driver.findElement(By.xpath("(//*[contains(text(),'Sort Z to A')])[1]")).click();
		ActionHandler.wait(7);
		
		//Validate Reservation ID in VISTA
		driver.findElement(By.xpath("(//span[@class='ng-binding ng-scope'])[1]")).click();
		ActionHandler.wait(1);
		String a = driver.findElement(By.xpath("(//span[@class='ng-binding ng-scope'])[1]")).getText();
		
		driver.quit();
	}
			
	}