package com.jlr.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.ManageVehiclePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;

public class JaguarCPE2EJourney extends TestBase {

	private static ManageVehiclePageContainer manageVehiclePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(JaguarCPE2EJourney.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUAREPACE);
			commonFuncObject.userLogin();
			manageVehiclePageContainer = PageFactory.initElements(driver, ManageVehiclePageContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void landRoverCp(String serviceRetailerCode, String selectServiceRetailer, String salesRetailerCode,
			String selectSalesRetailer, String month, String day, String timeSlot, String note, String title,
			String firstName, String lastName, String mobileNo, String houseNo, String street, String city,
			String postCode, String buildName, String country, String oldPassword, String newPassword,
			String distanceUnit, String dateFormat) {
		try {

			landRoverCpE2E2(serviceRetailerCode, selectServiceRetailer, salesRetailerCode, selectSalesRetailer, month,
					day, timeSlot, note, title, firstName, lastName, mobileNo, houseNo, street, city, postCode,
					buildName, country, oldPassword, newPassword, distanceUnit, dateFormat);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void landRoverCpE2E2(String serviceRetailerCode, String selectServiceRetailer, String salesRetailerCode,
			String selectSalesRetailer, String month, String day, String timeSlot, String note, String title,
			String firstName, String lastName, String mobileNo, String houseNo, String street, String city,
			String postCode, String buildName, String country, String oldPassword, String newPassword,
			String distanceUnit, String dateFormat) {
		try {

			// Preferred Sales Retailer
			CPCJ4_SalesRetailer salesRetailer = new CPCJ4_SalesRetailer();
			salesRetailer.cpcj4(salesRetailerCode, selectSalesRetailer);
			extentLogger.log(Status.INFO, "Preferred Sales Retailer Test Completed!!");
			ActionHandler.wait(2);

			// Preferred Service Retailer
			CPCJ3_PreferredServiceRetailer serviceRetailer = new CPCJ3_PreferredServiceRetailer();
			serviceRetailer.cpcj3(serviceRetailerCode, selectServiceRetailer);
			extentLogger.log(Status.INFO, "Preferred Service Retailer Test Completed!!");
			ActionHandler.wait(2);

			CPCJ7_BookAService bookService = new CPCJ7_BookAService();
			bookService.cpcj7(month, day, timeSlot, note, title, firstName, lastName, mobileNo, houseNo, street, city,
					postCode);
			ActionHandler.click(manageVehiclePageContainer.backButton);
			ActionHandler.wait(2);
			extentLogger.log(Status.INFO, "Book A Service Test Completed!!");

			CPCJ2_ProfileMgmt profileMgmt = new CPCJ2_ProfileMgmt();
			profileMgmt.cpcj2(firstName, lastName, mobileNo, buildName, city, country, postCode, oldPassword,
					newPassword, distanceUnit, dateFormat);
			extentLogger.log(Status.INFO, "Profile Management Test Completed!!");

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
