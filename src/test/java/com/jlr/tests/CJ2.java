package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.JaguarGearContainer;
import com.jlr.containers.OptionsContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class CJ2 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static OptionsContainer optionsContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static JaguarGearContainer jaguarGearContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	public static String journeyName = "";

	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ2.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			journeyName = context.getCurrentXmlTest().getName();
			setupTest(journeyName);
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.JAGUAREPACE);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			optionsContainer = PageFactory.initElements(driver, OptionsContainer.class);
			jaguarGearContainer = PageFactory.initElements(driver, JaguarGearContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ2(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String options, String convenience,
			String jaguarGear, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {

		testNcosCJ2(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, options,
				convenience, jaguarGear, touringAcc, paymentOpt, advRetail, defaultAr, term, defaultTerm, annualMileage,
				defaultMileage, deposit, defaultDeposit);

	}

	public void testNcosCJ2(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String options, String convenience,
			String jaguarGear, String touringAcc, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {
		try {
			String perMonth = "", finalValue = "";
			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.JAGUAREPACE);

			ActionHandler.wait(5);
			// Navigates to Finance Quote
			List<WebElement> defaultValueList = financeContainer.defaultValueList;
			String termValue = defaultValueList.get(1).getText().replaceAll("[^\\d.]", "");
			String annualMileageValue = defaultValueList.get(2).getText().replaceAll("[^\\d.]", "");

			extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);

			// Validating default options of PCP
			commonFuncObject.verifyFinanceQuoteDetails(paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit, driver);

			// Part Exchange Validations
			commonFuncObject.partExchange_R1(Constants.MILEAGE);
			commonValidationObj.partExchangeValidations();
		
			ActionHandler.wait(3);
			ActionHandler.click(financeContainer.useTowardDepositButton);

			assertEquals(financeContainer.surplusMsg1.getText(), Constants.SURPLUSMSG1);
			extenVerificationObj.extentVerification("Surplus Msg Validation: ", financeContainer.surplusMsg1.getText(),
					Constants.SURPLUSMSG1, "");
			assertEquals(financeContainer.surplusMsg2.getText(), Constants.SURPLUSMSG2);
			extenVerificationObj.extentVerification("Surplus Msg Validation:", financeContainer.surplusMsg2.getText(),
					Constants.SURPLUSMSG2, "");

			String financeAgreement = financeContainer.exchangeEquityTotalValue.getText().replaceAll("[^\\d.]", "");
			String totalDepositValue = financeContainer.totalDepositDefault.getAttribute("value").replaceAll("[^\\d.]",
					"");
			String cashBackValue = financeContainer.cashBack.getText().replaceAll("[^\\d.]", "");
			String expectedCashback = String
					.valueOf(Double.valueOf(financeAgreement) - Double.valueOf(totalDepositValue))
					.replaceAll("(?<=^\\d+)\\.0*$", "");
			LOGGER.info("Exp:" + expectedCashback + " Actual:" + cashBackValue);
			assertEquals(cashBackValue, expectedCashback);
			extenVerificationObj.extentVerification("Verify Cashback : ", cashBackValue, expectedCashback, "");
			ActionHandler.click(financeContainer.getYourQuote);
			ActionHandler.wait(3);
			// Quote Validations
			commonValidationObj.financeQuoteValidations(termValue, annualMileageValue,
					homePageContainer.onTheRoadPrice.getText(), totalDepositValue, paymentOpt);
			assertEquals(Double.valueOf(driver.findElement(By.xpath(financeContainer.getQuote("Customer Cashback")))
					.getText().replaceAll("[^\\d.]", "")), Double.valueOf(cashBackValue));

			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText()
					.replace(",", ""), annualMileageValue);
			extenVerificationObj.extentVerification("Verify Mileage per annum ", driver
					.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText().replace(",", ""),
					annualMileageValue, "");

			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Total Deposit"))).getText()
					.replaceAll("[^\\d.]", ""), totalDepositValue + ".00");
			extenVerificationObj.extentVerification("Verify Total Deposit",
					driver.findElement(By.xpath(financeContainer.getQuote("Total Deposit"))).getText()
							.replaceAll("[^\\d.]", ""),
					totalDepositValue + ".00", "");

			assertEquals(
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(Constants.PCP))).getText(),
					Constants.PCP);
			extenVerificationObj.extentVerification("Verify PCP Text",
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(Constants.PCP))).getText(),
					Constants.PCP, "");

			ActionHandler.click(financeContainer.continueToBuild);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(2);
				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior");
			}else{
				colours=Constants.DEFAULTCOLOURJAGUAR;
			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(2);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Interior");

			}else{
				headlining=Constants.DEFAULTHEADLININGJAGUAR;
			}

			if (("Yes").equals(options)) {
				ActionHandler.click(optionsContainer.optionsTab);
				ActionHandler.wait(2);
				ActionHandler.click(optionsContainer.convenienceInfotainment);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(optionsContainer.selectGivenCategory(convenience))).click();
				// Existence of p/m value
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Options");

			}

			if (("Yes").equals(jaguarGear)) {
				ActionHandler.click(jaguarGearContainer.jaguarGearTab);
				ActionHandler.wait(2);
				ActionHandler.click(jaguarGearContainer.TouringAcc);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(touringAcc))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
						"Jaguar Gear: Touring Accessories");

			}
			ActionHandler.wait(2);
			ActionHandler.scrollDown();
			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(2);
			String split[] = summaryContainer.grandTotal.getText().split(" ");

			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar OTR Validation: ", split[0],
					homePageContainer.onTheRoadPrice.getText(), "");
			finalValue = homePageContainer.onTheRoadPrice.getText();

			assertEquals(split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar Per Month Validation: ",
					split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText(), "");

			perMonth = homePageContainer.perMonthInfoBarValue.getText();

			// Personal Details
			ActionHandler.click(summaryContainer.sendToRetailer);
			ActionHandler.wait(2);
			String oldTab = commonFuncObject.completePersonalDetails(Constants.JAGUAREPACE);

			commonFuncObject.avokaTransact(models, engine, colours, headlining, paymentOpt, totalDepositValue,
					expectedCashback, perMonth, finalValue,"Yes",Constants.VEHICLENAMER1,Constants.FUELTYPER1,Constants.REGISTRATIONNUMBER1);
	
			// Switching to Parent window
			driver.switchTo().window(oldTab);
		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		// commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
