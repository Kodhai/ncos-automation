package com.jlr.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.base.TestBase;

public class Test {

	private static WebDriver driver;

	public static void main(String args[]) {
		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.get("http://www.mailinator.com/");
		ActionHandler.wait(2);

		driver.findElement(By.id("inboxfield")).sendKeys("automationTesting");
		driver.findElement(By.xpath("//BUTTON[@class='btn btn-dark'][contains(text(),'Go!')]")).click();

		List<WebElement> inbox = driver.findElements(By.xpath("//div[contains(@class,'all_message-min_text-3')]"));
		System.out.println("Size Of inbox:" + inbox.size());

		String headerText = "Your InControl Subscription for your Jaguar XE will expire in 30 days";

		for (WebElement webElement : inbox) {
			System.out.println("Header:" + webElement.getText());

			if (webElement.getText().equals(headerText)) {
				webElement.click();
				ActionHandler.wait(2);
				driver.switchTo().frame(driver.findElement(By.id("msg_body")));
			
				WebElement ele=driver.findElement(By.xpath("//span[contains(text(),'Find')]"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", ele);
				js.executeScript("arguments[0].click();", ele);
				ActionHandler.wait(5);
				
				List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(newTab.get(1));
				try {									
					Runtime.getRuntime().exec(System.getProperty("user.dir") + "/src/test/resources/authentication.exe");
				} catch (IOException e) {
					System.out.println("IOException:"+e);
				}
			
				System.out.println("URL:"+driver.getCurrentUrl());
				/*WebDriverWait wait = new WebDriverWait(driver, 10);      
				Alert alert = wait.until(ExpectedConditions.alertIsPresent());     
				alert.authenticateUsing(new UserAndPassword("jlr", "Sidecars"));*/
				//driver.get("https://jlr:Sidecars@jaguar.jlrcc-stage.test.summit.co.uk/");
				ActionHandler.wait(5);
				break;
			}
		}

		driver.quit();
	}

}
