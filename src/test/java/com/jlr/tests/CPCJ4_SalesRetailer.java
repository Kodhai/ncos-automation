package com.jlr.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ4_SalesRetailer extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ4_SalesRetailer.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	
	public CPCJ4_SalesRetailer(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUARIPACE);
		extenVerificationObj = new ExtenVerification(extentLogger);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUARIPACE);
			extenVerificationObj = new ExtenVerification(extentLogger);
			
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void cpcj4SalesRetailer(String retailer, String selectRetailer) {
		try {

			cpcj4(retailer, selectRetailer);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj4(String retailer, String selectRetailer) {
		try {

			assertEquals(homePageContainer.mySalesRetailer.getText(), "MY SALES RETAILER");
			extenVerificationObj.extentVerification("Sales Retailer Header Verification: ",
					homePageContainer.mySalesRetailer.getText(), "MY SALES RETAILER", "");
			try {
				if (homePageContainer.changeSalesRetailer.isDisplayed()) {
					extentLogger.log(Status.INFO, "Clicking on changed retailer");
					ActionHandler.click(homePageContainer.changeSalesRetailer);
					ActionHandler.wait(2);
				}
			} catch (NoSuchElementException e) {
				extentLogger.error("Change Retailer Button is not displayed!!!  ");
			}

			extentLogger.log(Status.INFO, "Searching Retailer :" + retailer);
			ActionHandler.clearAndSetText(homePageContainer.enterSalesRetailer, retailer);
			//ActionHandler.scrollToView(homePageContainer.searchButton);
			ActionHandler.click(homePageContainer.searchButton);

			ActionHandler.wait(3);

			List<WebElement> retailerSearchList = homePageContainer.retailerSearchList;
			LOGGER.info("Size: " + retailerSearchList.size());

			for (WebElement distance : homePageContainer.salesRetailerDistance) {
				extenVerificationObj.extentVerification("Distance Verification: ", distance.getText().contains("miles") || distance.getText().contains("MILES"),
						true, "Yes");

			}
			extentLogger.log(Status.INFO, "Search Results has Miles/Distance Data!!");
			assertTrue(homePageContainer.salesRetailerDistance.size() == retailerSearchList.size());

			for (WebElement name : homePageContainer.salesRetailerName) {
				extenVerificationObj.extentVerification("Name Verification: ", name.getText().contains("Jaguar") || name.getText().contains("Land Rover"), true,
						"Yes");
				extenVerificationObj.extentVerification("Name Verification: ", name.getText().isEmpty(), false, "Yes");

			}

			extentLogger.log(Status.INFO, "Search Results has Retailer's Name !!");
			assertTrue(homePageContainer.salesRetailerName.size() == retailerSearchList.size());

			for (WebElement address : homePageContainer.salesRetailerAddress) {
				extenVerificationObj.extentVerification("Address Verification: ", address.getText().isEmpty(), false,
						"Yes");
			}

			extentLogger.log(Status.INFO, "Search Results has Retailer's Address!!");
			assertTrue(homePageContainer.salesRetailerAddress.size() == retailerSearchList.size());

			ActionHandler.click(driver.findElement(By.xpath(homePageContainer.selectRetailer(selectRetailer))));
			ActionHandler.wait(4);
			extentLogger.log(Status.INFO, "Selected Retailer Containing Name:" + selectRetailer);

			assertTrue(homePageContainer.retailerName.getText().contains(selectRetailer));
			extentLogger.log(Status.INFO, "Retailer is Selected:" + homePageContainer.retailerName.getText());

			extenVerificationObj.extentVerification("Retailer Saved Message verification: ", true,
					VerifyHandler.verifyElementPresent(homePageContainer.notificationMsg), "Yes");
			LOGGER.info("Msg:" + homePageContainer.notificationMsg.getText());
			extenVerificationObj.extentVerification("Retailer Saved Message verification:", Constants.RETAILERSAVED,
					homePageContainer.notificationMsg.getText(), "Yes");

			LOGGER.info("Content size:" + homePageContainer.retailerContentDetail.size());
			extentLogger.log(Status.INFO, "Verifying Presence Of Content Tags: Phone,Address,Email,Website");
			assertTrue(homePageContainer.retailerContentDetail.size() == 4);

			extenVerificationObj.extentVerification("Existence of Changed Retailer button:", true,
					VerifyHandler.verifyElementPresent(homePageContainer.changeSalesRetailer), "Yes");

			ActionHandler.click(homePageContainer.retailerGetDirections);
			commonFuncObject.getDirections();

		} catch (Exception e) {
			extentLogger.error("Exception ::  " + e);
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
