package com.jlr.tests;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.AccessoriesContainer;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.OptionsContainer;
import com.jlr.containers.ShopBasketFinanceServiceContainer;
import com.jlr.containers.ShopBasketReserveVehicleContainer;
import com.jlr.containers.ShoppingBasketContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;
import org.openqa.selenium.OutputType;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class R3_Regression extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static OptionsContainer optionsContainer = null;
	private static AccessoriesContainer accessoriesContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	private static ShoppingBasketContainer shoppingBasketContainer = null;
	private static ShopBasketReserveVehicleContainer shopBasketReserveVehicleContainer = null;
	private static ShopBasketFinanceServiceContainer basketFinanceServiceContainer = null;
	public static String journeyName = "";
	private WebDriver driver;
	public ExtentTest extentLogger;
	public static int testCaseNum = 0;
	public HashMap<String, String> configValues = new HashMap<String, String>();
	private static final Logger LOGGER = LoggerFactory.getLogger(R3_Regression.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;
	public static String Number;
    public static String screenshot_path = "C:\\Users\\pkodhai\\Documents\\Automation Code_Latest\\NCOS_Config\\Configurator_NCOS_E2E\\screenshots";
	@BeforeMethod
	
		public void init(ITestContext context) {
		try {
			
			LOGGER.info("starting test Before Method");
			++testCaseNum;
			journeyName = context.getCurrentXmlTest().getName() + "_" + testCaseNum;
			setupTest(journeyName);
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			optionsContainer = PageFactory.initElements(driver, OptionsContainer.class);
			accessoriesContainer = PageFactory.initElements(driver, AccessoriesContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
			shoppingBasketContainer = PageFactory.initElements(driver, ShoppingBasketContainer.class);
			shopBasketReserveVehicleContainer = PageFactory.initElements(driver, ShopBasketReserveVehicleContainer.class);
			basketFinanceServiceContainer = PageFactory.initElements(driver, ShopBasketFinanceServiceContainer.class);
		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdataR2", dataProviderClass = DataReader.class)
	public void r2_regression(String serial_Number, String url, String Brand, String namePlate, String models, String engine,
			String specif, String nextStep, String exterior, String colours, String visualPacks, String interior,
			String headlining, String options, String convenience, String accessories, String touringAcc,
			String paymentOpt, String px, String pxRegistration, String mileage, String condition, String advRetail,
			String defaultAr, String term, String defaultTerm, String annualMileage, String defaultMileage,
			String deposit, String defaultDeposit, String basketChoice, String Register, String regEmailId,
			String fName, String lName, String regPassword, String logId, String pwd, String retailerName,
			String telEmail, String onlineDeliverOpt, String addrFlatNo, String addrCity, String addrPostCode,
			String financeApp, String appTitle, String forename, String appMiddle, String appsurname, String appHTel,
			String appEmail, String appGender, String appCountryOrig, String appMarStatus, String appDepend,
			String appvehicUsg, String appPostCosd, String appCountry, String appYrsAddr, String appMonthAddr,
			String appAccomd, String appPost, String appContryAddr, String appEmplr, String appPhoneEmpr,
			String appOcup, String appBasis, String appCategEmpr, String appYrEmpr, String appMonthEmpr,
			String appExisAggr, String appAnnualIncom, String appMonthlyMortg, String appOtherExp, String appExpChange,
			String appAcType, String appSortCode1, String appSortCode2, String appSortCode3, String appAcNum,
			String appAcYrBank) throws Exception {
		driver.manage().window().maximize();
		
	    //  imports parameter according to excel
		String appGrossIncome = "12345";
	    if (("SalesDX").equals(namePlate)) {
		driver.get(url);
		ActionHandler.wait(30);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		extentLogger.info("List of Vehicles",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("List of Vehicles")).build());
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		extentLogger.info("List of Vehicles1",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("List of Vehicles1")).build());
				
		//Select Model
		commonFuncObject.SalesDx_Model_Select(models);
		extentLogger.info("Selected Models",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Model Selection")).build());	
		
		//Get Finance Options
		ActionHandler.wait(8);
		if (VerifyHandler.verifyElementPresent(financeContainer.Disclaimer)) {
			ActionHandler.click(driver.findElement(By.xpath("//span[text()='OK']")));
			}
		
		//filter my Model year
		 if(url.contains("LTO"))
	        {
	            //click on filter by drop down
	            driver.findElement(By.xpath("//div[@class='Dropdown-root']")).click();
	            ActionHandler.wait(2);
	            //select model year
	            driver.findElement(By.xpath("//div[contains(text(),'2020 Model Year')]")).click();
	        }
		 
		
		ActionHandler.click(driver.findElement(By.xpath("(//span[@class='info-bar__item__button__label'])[2]")));
		ActionHandler.wait(8);
		
		if ("Cash".equals(paymentOpt) && "N".equals(px)) {
			ActionHandler.click(financeContainer.continueToBuild);
			ActionHandler.wait(5);
			}
		
		else if ("Cash".equals(paymentOpt) && "Y".equals(px)) {
			if (VerifyHandler.verifyElementPresent(financeContainer.PartExchangeYes)) {
			    commonFuncObject.partExchange("BJ67UMO", "Good", "1250");
			    }
			ActionHandler.wait(5);
			if (VerifyHandler.verifyElementPresent(financeContainer.DEDUCTFROMONTHEROADPRICE)) {
			ActionHandler.click(financeContainer.DEDUCTFROMONTHEROADPRICE);
			ActionHandler.wait(2);
			}
			if (VerifyHandler.verifyElementPresent(financeContainer.useTowardDepositButton)) {
				ActionHandler.click(financeContainer.useTowardDepositButton);
				ActionHandler.wait(2);
			}
			ActionHandler.click(financeContainer.acceptAndContinue);
			ActionHandler.wait(3);
			ActionHandler.click(financeContainer.continueToBuild);
			ActionHandler.wait(3);	
		}
		else {
		ActionHandler.click(driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
		
		if (!"NA".equals(term)) {
			moveSlider(Constants.TERM, term);
		}
		if (!"NA".equals(annualMileage)) {
			moveSlider(Constants.ANNUALMILEAGE, annualMileage);
		}		
		else {
			if (StringUtils.isNotBlank(defaultDeposit) && !"NA".equals(defaultDeposit)) {
				double otr = Double.parseDouble(homePageContainer.onTheRoadPrice.getText().replaceAll("[^\\d.]", ""));
				int depositPercent = Integer.parseInt(defaultDeposit);
				double val = (otr * depositPercent) / 100;
				ActionHandler.clearAndSetText(financeContainer.totalDepositDefault, String.valueOf(Math.round(val)));
				ActionHandler.wait(3);
			}
				
		}
		if ("Y".equals(px)) {
			// Part Exchange Validations
			commonFuncObject.partExchange(pxRegistration, condition, mileage);
			ActionHandler.wait(3);
			ActionHandler.click(financeContainer.useTowardDepositButton);
			ActionHandler.wait(3);
		}
		
		extentLogger.info("Finance Quote",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote")).build());
		ActionHandler.pageScrollDown();
		extentLogger.info("Finance Quote_2",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote_2")).build());
		
		if(VerifyHandler.verifyElementPresent(financeContainer.getYourQuote)) {
			ActionHandler.click(financeContainer.getYourQuote);
			ActionHandler.wait(2);
		}
		if(VerifyHandler.verifyElementPresent(financeContainer.acceptAndContinue)) {
			ActionHandler.click(financeContainer.acceptAndContinue);
			ActionHandler.wait(2);
		}    		           
		if (VerifyHandler.verifyElementPresent(financeContainer.continueToBuild)) {
			ActionHandler.click(financeContainer.continueToBuild);
			ActionHandler.wait(3);
		}
	}
		
		//Summary	
		ActionHandler.wait(10);
		ActionHandler.click(driver.findElement(By.xpath("//*[text()='Select options']")));
		ActionHandler.wait(15);
		if (Brand.equalsIgnoreCase("Landrover")) {
			ActionHandler.click(driver.findElement(By.xpath("(//*[text()='View summary'])[1]")));
			ActionHandler.wait(10);
		}
		if (Brand.equalsIgnoreCase("Jaguar")) {
			ActionHandler.click(driver.findElement(By.xpath("(//*[text()='View summary'])[2]")));
			ActionHandler.wait(10);
		}
		
		//Basket
		  shoppingBasketContinue(models, engine, Brand, basketChoice, Register, regEmailId, fName, lName, regPassword, logId, pwd,
				retailerName, telEmail, onlineDeliverOpt, addrFlatNo, addrCity, addrPostCode, financeApp, appTitle,
				forename, appMiddle, appsurname, appHTel, appEmail, appGender, appCountryOrig, appMarStatus,
				appDepend, appvehicUsg, appPostCosd, appCountry, appYrsAddr, appMonthAddr, appAccomd, appPost,
				appContryAddr, appEmplr, appPhoneEmpr, appOcup, appBasis, appCategEmpr, appYrEmpr, appMonthEmpr,
				appExisAggr, appGrossIncome, appAnnualIncom, appMonthlyMortg, appOtherExp, appExpChange, appAcType,
				appSortCode1, appSortCode2, appSortCode3, appAcNum, appAcYrBank);  
		
		}
		
	else {
		//Open Url for configuration
		commonFuncObject.userLogin(url);
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(financeContainer.Disclaimer)) {
		ActionHandler.click(driver.findElement(By.xpath("//span[text()='OK']")));
		}
		
		//Sales DX Journey ends here, Configurator Journey starts here
		//Choose Model, Engine and spec pack based on inputs from excel
		commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.LANDROVER);

		
		//Choose exteriors and additional features if given as Yes in excel
		if (("Yes").equals(exterior)) {

			ActionHandler.click(exteriorContainer.exteriorTab);
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))));
			ActionHandler.wait(4);
			if (StringUtils.isNotBlank(visualPacks) && !"NA".equals(visualPacks)) {
				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				ActionHandler.click(driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))));
			}
		}

		if (("Yes").equals(interior)) {
			ActionHandler.click(interiorContainer.interiorTab);
			ActionHandler.wait(6);
			ActionHandler.click(interiorContainer.headlining);
			ActionHandler.wait(4);
			ActionHandler.scrollDown();
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))));
			ActionHandler.wait(2);
		}

		if (("Yes").equals(options)) {
			ActionHandler.click(optionsContainer.optionsTab);
			ActionHandler.wait(4);
			if (VerifyHandler.verifyElementPresent(accessoriesContainer.jaguarGearTab)) {
				if (VerifyHandler.verifyElementPresent(optionsContainer.infotainment)) {
					ActionHandler.click(optionsContainer.infotainment);
				} else if (VerifyHandler.verifyElementPresent(optionsContainer.convenience)) {
					ActionHandler.click(optionsContainer.convenience);
				}
			} else {
				if (VerifyHandler.verifyElementPresent(optionsContainer.convenienceInfotainment)) {
					ActionHandler.click(optionsContainer.convenienceInfotainment);
				} else if (VerifyHandler.verifyElementPresent(optionsContainer.convenience)) {
					ActionHandler.click(optionsContainer.convenience);
				}
				else {
					ActionHandler.click(optionsContainer.infotainment);
				}
			}
			ActionHandler.wait(4);
			ActionHandler.click(driver.findElement(By.xpath(optionsContainer.selectGivenCategory(convenience))));
		}

		if (("Yes").equals(accessories)) {
			if (VerifyHandler.verifyElementPresent(accessoriesContainer.jaguarGearTab)) {
				ActionHandler.click(accessoriesContainer.jaguarGearTab);
			} else {
				ActionHandler.click(accessoriesContainer.accessoriesTab);
			}
			ActionHandler.wait(4);
			int counter = 0;
			// ActionHandler.scrollToView(driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))));
			if (StringUtils.isNotBlank(touringAcc) && !"NA".equals(touringAcc)) {
				ActionHandler.click(accessoriesContainer.touringAccessories);
				ActionHandler.wait(3);
				if (!VerifyHandler.verifyElementPresent(
						driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))))) {
					while (VerifyHandler.verifyElementPresent(
							driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))))) {
						ActionHandler.pageScrollDown();
						ActionHandler.wait(1);
						counter++;
						if (counter > 12) {
							break;
						}
					}
				}
				ActionHandler.click(driver.findElement(By.xpath(accessoriesContainer.selectGivenCategory(touringAcc))));
				ActionHandler.wait(4);
			}
		}

		// click on finance quote button
		if(!VerifyHandler.verifyElementPresent(financeContainer.selfFinanceCashPurchase)) {
		if(VerifyHandler.verifyElementPresent(financeContainer.financeQuoteButton1)) {
			ActionHandler.click(financeContainer.financeQuoteButton1);
			ActionHandler.wait(5);
		}
		else if(VerifyHandler.verifyElementPresent(financeContainer.financeQuoteButton)) {
			ActionHandler.click(financeContainer.financeQuoteButton);
			ActionHandler.wait(5);
		}
		}
        extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);
        ActionHandler.wait(10);
		ActionHandler.click(driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
	
		if ("Cash".equals(paymentOpt) && "N".equals(px)) {
			ActionHandler.click(financeContainer.continueToBuild);
			ActionHandler.wait(5);
			if(!VerifyHandler.verifyElementPresent(financeContainer.orderVehicleLink)) {
				ActionHandler.click(summaryContainer.summaryTab);
				ActionHandler.wait(4);
		        }
		}
		
		else if ("Cash".equals(paymentOpt) && "Y".equals(px)) {
			commonFuncObject.partExchange(pxRegistration, condition, mileage);
			ActionHandler.wait(5);
			ActionHandler.click(financeContainer.DEDUCTFROMONTHEROADPRICE);
			ActionHandler.wait(2);
			ActionHandler.click(financeContainer.acceptAndContinue);
			ActionHandler.wait(3);
		//	ActionHandler.click(financeContainer.continueToBuild);
			ActionHandler.wait(3);
			
		}
		else {
		ActionHandler.scrollToView(driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
		ActionHandler.wait(2);
		
		if (!"NA".equals(term)) {
			moveSlider(Constants.TERM, term);
		}
		if (!"NA".equals(annualMileage)) {
			moveSlider(Constants.ANNUALMILEAGE, annualMileage);
		}
	
/*		extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);
		ActionHandler.scrollToView(driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
		ActionHandler.wait(2);
		WebElement paytype = (driver.findElement(By.xpath(financeContainer.summaryFinanceProductOption(paymentOpt))));
	//	ActionHandler.click(paytype);
		ActionHandler.scrollToView(paytype);
		ActionHandler.waitForElement(paytype, 3);
		ActionHandler.locateElement("productSelector2", "id");
	//	ActionHandler.clickElement(paytype); 

		ActionHandler.click(paytype); 
		
		if (!"NA".equals(term)) {
			moveSlider(Constants.TERM, term);
		}
		if (!"NA".equals(annualMileage)) {
			moveSlider(Constants.ANNUALMILEAGE, annualMileage);
		} */
		
		
		if ("Y".equals(px)) {
			// Part Exchange Validations
			commonFuncObject.partExchange(pxRegistration, condition, mileage);
			ActionHandler.wait(3);
			ActionHandler.click(financeContainer.useTowardDepositButton);
			ActionHandler.wait(3);
			
		}
		
		 else {
			if (StringUtils.isNotBlank(defaultDeposit) && !"NA".equals(defaultDeposit)) {
				double otr = Double.parseDouble(homePageContainer.onTheRoadPrice.getText().replaceAll("[^\\d.]", ""));
				int depositPercent = Integer.parseInt(defaultDeposit);
				double val = (otr * depositPercent) / 100;
				ActionHandler.clearAndSetText(financeContainer.totalDepositDefault, String.valueOf(Math.round(val)));
				ActionHandler.wait(3);
			}
				
		}
		
		if (VerifyHandler.verifyElementPresent(financeContainer.acceptAndContinue)) {
			ActionHandler.click(financeContainer.acceptAndContinue);
			ActionHandler.wait(3);
			}
		else {
		ActionHandler.click(financeContainer.getYourQuote);
		ActionHandler.wait(3);
		}
		}
		StringBuilder sb = new StringBuilder();
		sb.append(serial_Number);
		sb.append(".");
		sb.append(journeyName);
		sb.append(".");
		sb.append(namePlate.replace("*", ""));
		sb.append(".");
		sb.append(models.replace("*", ""));
		sb.append(".");
		sb.append(engine.replace("*", ""));
		sb.append(".");
		sb.append(specif.replace("*", ""));
		sb.append(".");

		ActionHandler.wait(4);
		extentLogger.info("Configuration & Finance Quote" + sb.toString(),
				MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot(sb.toString())).build());
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();

		extentLogger.info("Configuration & Finance Quote" + sb.toString(),
				MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot(sb.toString())).build());
		ActionHandler.wait(2);
		setConfigValues(); 
		extentLogger.info("Selecting Basket Choice :: " + basketChoice);
		if ("Send to retailer".equalsIgnoreCase(basketChoice)) {
			sendToRetailerBasket(models, engine, basketChoice, Register, regEmailId, fName, lName, regPassword, logId, pwd,
					retailerName, telEmail);
		} else if ("Save Shopping Basket".equalsIgnoreCase(basketChoice)) {
	//		saveShoppingBasket(basketChoice, Register, regEmailId, fName, lName, regPassword, logId, pwd, retailerName,
	//				telEmail);
		} else if ((basketChoice).contains("Continue")) {
			shoppingBasketContinue(models, engine, Brand, basketChoice, Register, regEmailId, fName, lName, regPassword, logId, pwd,
					retailerName, telEmail, onlineDeliverOpt, addrFlatNo, addrCity, addrPostCode, financeApp, appTitle,
					forename, appMiddle, appsurname, appHTel, appEmail, appGender, appCountryOrig, appMarStatus,
					appDepend, appvehicUsg, appPostCosd, appCountry, appYrsAddr, appMonthAddr, appAccomd, appPost,
					appContryAddr, appEmplr, appPhoneEmpr, appOcup, appBasis, appCategEmpr, appYrEmpr, appMonthEmpr,
					appExisAggr, appGrossIncome, appAnnualIncom, appMonthlyMortg, appOtherExp, appExpChange, appAcType,
					appSortCode1, appSortCode2, appSortCode3, appAcNum, appAcYrBank); 
		} 
			} 

	} 

	public void setConfigValues() {
		try {
			configValues.put("OTR", financeContainer.otrTxt.getText());
			configValues.put("MonthlyCost", financeContainer.monthlyCostTxt.getText());
			configValues.put("PartExchange", VerifyHandler.verifyElementPresent(financeContainer.partExchangeCostTxt)
					? financeContainer.partExchangeCostTxt.getText() : "");
			configValues.put("FinanceOption", financeContainer.financeOptionTxt.getText());

		//	 LOGGER.info(configValues);

			// Basket Page Finance Option
			/*
			 * LOGGER.info("Basket Page Finance Option>>>>>>>> ");
			 * LOGGER.info(shopBasketReserveVehicleContainer.otrTxt.getText());
			 * LOGGER.info(shopBasketReserveVehicleContainer.monthlyCostTxt.
			 * getText());
			 * LOGGER.info(shopBasketReserveVehicleContainer.financeOptionTxt.
			 * getText());
			 * 
			 * // nextPage if (VerifyHandler.verifyElementPresent(
			 * shopBasketReserveVehicleContainer.financeDetailExpBtn)) {
			 * ActionHandler.click(shopBasketReserveVehicleContainer.
			 * financeDetailExpBtn); }
			 */
		}  catch (Exception e) {
			LOGGER.error(e.toString());
		}
	}

	public void takeFullScreenshots(StringBuilder sb) {
		final Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(500))
				.takeScreenshot(driver);
		final BufferedImage image = screenshot.getImage();
		try {
			ImageIO.write(image, "jpg", new File("C:/screenshots/" + sb.toString() + ".jpg"));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	private void moveSlider(String sliderName, String newVal) {

		int displayedValue = 0;

		if (Constants.TOTALDEPOSIT.equals(sliderName)) {
			displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
		} else if (Constants.TERM.equals(sliderName)) {
			displayedValue = getValue(ActionHandler.locateElement(
					"//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
					"xpath"), false);
		} else {
			
			if(VerifyHandler.verifyElementPresent(financeContainer.AnnualMileage)) {
			displayedValue = getValue(ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"),
					false);
			}
		}

		int newValue = Integer.parseInt(newVal);
		/**
		 * displayedValueTemp is used To exit loop in case of slider boundary
		 * value reached. conditions
		 */
		int displayedValueTemp = displayedValue;
		if (displayedValue < newValue) {
			while (displayedValue < newValue) {
				LOGGER.info("Moving slider to right, displayedValue:" + displayedValue);
				if (Constants.TOTALDEPOSIT.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.totalDepositSliderRight);
					displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
				} else if (Constants.TERM.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.termSliderRight);
					displayedValue = getValue(ActionHandler.locateElement(
							"//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
							"xpath"), false);
				} else {
					if(VerifyHandler.verifyElementPresent(financeContainer.AnnualMileage)) {
					ActionHandler.clickElement(financeContainer.annualMileageSliderRight);
					displayedValue = getValue(
							ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"), false);
				}
				}
				if (displayedValueTemp == displayedValue) {
					break;
				} else {
					displayedValueTemp = displayedValue;
				}
			}
		} else {
			while (displayedValue > newValue) {
				LOGGER.info("Moving slider to left, displayedValue:" + displayedValue);
				if (Constants.TOTALDEPOSIT.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.totalDepositSliderLeft);
					displayedValue = getValue(ActionHandler.locateElement("DepositManual", "id"), true);
				} else if (Constants.TERM.equals(sliderName)) {
					ActionHandler.clickElement(financeContainer.termSliderLeft);
					displayedValue = getValue(ActionHandler.locateElement("//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span",
							"xpath"), false);
				} else {
					if(VerifyHandler.verifyElementPresent(financeContainer.AnnualMileage)) {
					ActionHandler.clickElement(financeContainer.annualMileageSliderLeft);
					displayedValue = getValue(
							ActionHandler.locateElement("//*[contains(text(),'miles per annum')]", "xpath"), false);
					}
					}
				if (displayedValueTemp == displayedValue) {
					break;
				} else {
					displayedValueTemp = displayedValue;
				}
			}
		}

	}

	private int getValue(WebElement w, boolean flag) {

		String s = "";
		if (StringUtils.isNotBlank(w.getAttribute("value"))) {
			s = w.getAttribute("value");
		} else {
			s = w.getText();
		}
		String[] a = s.split(" ");
		if (flag) {
			return Integer.parseInt(a[1].replaceAll(",", ""));
		}
		return Integer.parseInt(a[0].replaceAll(",", ""));
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(
					result.getName() + " Test case failed. @ScreenShot : "
							+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot("failed")),
					ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		// commonFuncObject.userLogsOut();
		getDriver().quit();
	}

	public void shoppingBasketContinue(String Model, String flow, String Brand, String basketChoice, String Register, String regEmailId, String fName,
			String lName, String regPass, String logId, String pwd, String retailerName, String telEmail,
			String onlineDeliverOpt, String addrFlatNo, String addrCity, String addrPostCode, String financeApp,
			String appTitle, String forename, String appMiddle, String appsurname, String appHTel, String appEmail,
			String appGender, String appCountryOrig, String appMarStatus, String appDepend, String appvehicUsg,
			String appPostCosd, String appCountry, String appYrsAddr, String appMonthAddr, String appAccomd,
			String appPost, String appContryAddr, String appEmplr, String appPhoneEmpr, String appOcup, String appBasis,
			String appCategEmpr, String appYrEmpr, String appMonthEmpr, String appExisAggr, String appGrossIncome,
			String appAnnualIncom, String appMonthlyMortg, String appOtherExp, String appExpChange, String appAcType,
			String appSortCode1, String appSortCode2, String appSortCode3, String appAcNum, String appAcYrBank)
			throws Exception {
            System.out.print("check");
        
            if(VerifyHandler.verifyElementPresent(summaryContainer.summaryTab)) {
		            ActionHandler.click(summaryContainer.summaryTab);    
		            ActionHandler.wait(2);
            }
        if(VerifyHandler.verifyElementPresent(financeContainer.orderVehicleLinksalesDX)) {
        	//Order this vehicle
    		ActionHandler.click(driver.findElement(By.xpath("//*[text()='Order This Vehicle']")));
    		ActionHandler.wait(10);
        }
        else  {             
		extentLogger.info("Click on '[Staging] Order This Vehicle Link' Button");
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath("//span[contains(text(),'[STAGING] ORDER THIS VEHICLE')]")));
			ActionHandler.wait(10);	
			if(!VerifyHandler.verifyElementPresent(shoppingBasketContainer.continueBtn)) {
				ActionHandler.click(driver.findElement(By.xpath("//span[contains(text(),'[STAGING] ORDER THIS VEHICLE')]")));
				ActionHandler.wait(10);	
			}
       }
        extentLogger.info("Basket Content",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Basket Content")).build());
       
       //Accept cookies
        if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.Acceptcookies)) {
        	ActionHandler.click(shoppingBasketContainer.Acceptcookies);
        	ActionHandler.wait(2);
        }
        
       if ("Continue&Edit".equalsIgnoreCase(basketChoice)) {
        	String FP_Current = ActionHandler.getElementText(shoppingBasketContainer.choosenFP);
        	extentLogger.log(Status.INFO, "Current Finance Product is : " +FP_Current);
        	ActionHandler.wait(2);
        	
        	 //Click on Edit button on Basket 
        	ActionHandler.wait(7);
        	ActionHandler.click(shoppingBasketContainer.Edit_FromBasket);    
            ActionHandler.wait(7);
        	
        	//Handle Sales DX login popup when clicked from Configurator flow basket
        	if (!"LTO".equals(flow)) {
        		if (!"BTO".equals(flow)) {
        			SalesDX_Login_PopUp();
        		}
        	}
        	
        	ActionHandler.wait(25);
        	
         if (VerifyHandler.verifyElementPresent(financeContainer.EditFP)) {
        		ActionHandler.click(financeContainer.EditFP);
	            ActionHandler.wait(10);
           }
            
            // In the edit flow, If current FP is HP, then change it to Cash
            if ((FP_Current).contains("HIRE PURCHASE")) {
            	ActionHandler.click(financeContainer.Cash);
                ActionHandler.wait(2);
                ActionHandler.click(financeContainer.Cash);
                ActionHandler.wait(2);
                extentLogger.log(Status.INFO, "Selecting Finance Product to 'Cash Purchase' from HP ");
        	}
            
            // In the edit flow, If current FP is PCP, then change it to HP
            if ((FP_Current).contains("PERSONAL CONTRACT PURCHASE (PCP)")) {
            	ActionHandler.click(financeContainer.HP);
                ActionHandler.wait(2);
                ActionHandler.click(financeContainer.HP);
                ActionHandler.wait(2);
                extentLogger.log(Status.INFO, "Selecting Finance Product to 'Hire Purchase' from PCP ");
        	}
            
            // In the edit flow, If current FP is Cash, then change it to PCP
            if ((FP_Current).contains("CASH")) {
            	ActionHandler.click(financeContainer.PCP);
                ActionHandler.wait(2);
                ActionHandler.click(financeContainer.PCP);
                ActionHandler.wait(2);
                extentLogger.log(Status.INFO, "Selecting Finance Product to 'Personal Contract Purchase' from Cash");
        	}
            extentLogger.info("Edit Finance Product",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Product_Edit")).build());
    		
            //Add in PX
            if (!VerifyHandler.verifyElementPresent(financeContainer.PartExchangeYes)) {
            	driver.navigate().refresh();
            }
            ActionHandler.wait(20);
            if (VerifyHandler.verifyElementPresent(financeContainer.PartExchangeYes)) {
            	commonFuncObject.partExchange("BJ67UMO", "Good", "1250");
            }
            
            extentLogger.info("Add Part Exchange  : ",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Add PX")).build());
            
            //For Cash, deduct from OTR
            if (VerifyHandler.verifyElementPresent(financeContainer.DEDUCTFROMONTHEROADPRICE)) {
    			ActionHandler.click(financeContainer.DEDUCTFROMONTHEROADPRICE);
    			ActionHandler.wait(4);
            }
            
            //For HP/PCP, use towards my deposit.
            if (VerifyHandler.verifyElementPresent(financeContainer.useTowardDepositButton)) {
    			ActionHandler.click(financeContainer.useTowardDepositButton);
    			ActionHandler.wait(4);
            }
            
            // click on get quote button
    		if(VerifyHandler.verifyElementPresent(financeContainer.getYourQuote)) {
    			ActionHandler.click(financeContainer.getYourQuote);
    			ActionHandler.wait(4);
    		}
    		if(VerifyHandler.verifyElementPresent(financeContainer.acceptAndContinue)) {
    			ActionHandler.click(financeContainer.acceptAndContinue);
    			ActionHandler.wait(4);
    		}    		           
    		if (VerifyHandler.verifyElementPresent(financeContainer.continueToBuild)) {
    			ActionHandler.click(financeContainer.continueToBuild);
    			ActionHandler.wait(6);
    		}
    		
    		ActionHandler.wait(4);
    		
    		//Accept cookies
            if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.Acceptcookies)) {
            	ActionHandler.click(shoppingBasketContainer.Acceptcookies);
            	ActionHandler.wait(2);
            }
    		
    		extentLogger.info("Finance Quote",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote")).build());
    		ActionHandler.pageScrollDown();
    		extentLogger.info("Finance Quote_2",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote_2")).build());
     }
        
        //Validate Finance Product post editing
        String FP_Current = ActionHandler.getElementText(shoppingBasketContainer.choosenFP);
    	extentLogger.log(Status.INFO, "Updated Finance Product is : " +FP_Current);
    	extentLogger.info("Basket_after edit",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Basket_After_edit")).build());
		
		// Validating config Data
	    // validateConfigData("ShoppingBasketPage"); 
		extentLogger.info("Click on continue Button");
		ActionHandler.waitForElement(shoppingBasketContainer.continueBtn, 10);
		ActionHandler.click(shoppingBasketContainer.continueBtn);
		extentLogger.info("Navigate to delivery method Log in page");

		extentLogger.info("Select Log in Option");
		ActionHandler.wait(28);
		
		//Accept cookies
        if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.Acceptcookies)) {
        	ActionHandler.click(shoppingBasketContainer.Acceptcookies);
        	ActionHandler.wait(2);
        }
		
		if ("Yes".equalsIgnoreCase(Register)) {
			extentLogger.info("Select Register in Option");
			ActionHandler.click(shoppingBasketContainer.registerRadioBtn);
			basketUserReg(regEmailId, regPass, fName, lName);
			ActionHandler.wait(15);
			}
		else {
		ActionHandler.click(shoppingBasketContainer.lonInRadioBtn, 60);
		ActionHandler.wait(15);
		deliveryMethodLogin(logId, pwd);
		}
		ActionHandler.wait(40);
		  if(!VerifyHandler.verifyElementPresent(driver.findElement(By.xpath("//*[text()='Deliver to my home']"))))
	        {
	           driver.navigate().refresh();
	        }
		ActionHandler.wait(60);
		extentLogger.info("Select delivery Option :: " + onlineDeliverOpt);
		ActionHandler.scrollToView(driver.findElement(By.xpath(shoppingBasketContainer.deliveryMethod(onlineDeliverOpt))));
		ActionHandler.click(driver.findElement(By.xpath(shoppingBasketContainer.deliveryMethod(onlineDeliverOpt))));
		// if delivery to home
		ActionHandler.wait(6);
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.Addresssearch)) {
			deliveryToHome(addrFlatNo, addrCity, addrPostCode);
		}
		if ("Deliver to my home".equalsIgnoreCase(onlineDeliverOpt)) {	
			if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.editHomeDelivAddrLink)) {
				if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.homephone)) {
				deliveryToHome(addrFlatNo, addrCity, addrPostCode);
				}
			}
		}
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.Addnewaddress)) {
			ActionHandler.click(shoppingBasketContainer.Addnewaddress);
			deliveryToHome(addrFlatNo, addrCity, addrPostCode);
		}
		ActionHandler.wait(5);
		selectingRetailer(retailerName);
		ActionHandler.wait(5);
		ActionHandler.click(shoppingBasketContainer.contReservationBtn);
		ActionHandler.wait(10);
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.retailerPriceTxt)) {
			//ActionHandler.click(shoppingBasketContainer.retailerPriceAck);
			extentLogger.info("RPO validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Retailer Price Overlay")).build());
			ActionHandler.pageScrollDown();
			extentLogger.info("RPO validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Retailer Price Overlay")).build());
			
			//If Finance product is Cash
			String FP = ActionHandler.getElementText(shoppingBasketContainer.FP);
			if ("CASH".equals(FP)) {
				String OTR1 = ActionHandler.getElementText(shoppingBasketContainer.OTR_1);
				String OTR2 = ActionHandler.getElementText(shoppingBasketContainer.OTR_2);
				extentLogger.info(MarkupHelper.createLabel("OTR1 is : "+OTR1 + "After RPO, OTR is : "+OTR2, ExtentColor.GREEN));
			}
			else {
				
			//Gather RPO Updates
			String OTR1 = ActionHandler.getElementText(shoppingBasketContainer.OTR_1);
			String OTR2 = ActionHandler.getElementText(shoppingBasketContainer.OTR_2);
			extentLogger.info(MarkupHelper.createLabel("OTR1 is : "+OTR1 + "After RPO, OTR is : "+OTR2, ExtentColor.GREEN));
            
			String TotalCredit1 = ActionHandler.getElementText(shoppingBasketContainer.Total_Credit_1);
			String TotalCredit2 = ActionHandler.getElementText(shoppingBasketContainer.Total_Credit_2);
			extentLogger.info(MarkupHelper.createLabel("TotalCredit is : "+TotalCredit1 + "After RPO, TotalCredit2 is : "+TotalCredit2, ExtentColor.GREEN));
			
			String totalPayment_1 = ActionHandler.getElementText(shoppingBasketContainer.totalPayment_1);
			String totalPayment_2 = ActionHandler.getElementText(shoppingBasketContainer.totalPayment_2);
			extentLogger.info(MarkupHelper.createLabel("totalPayment is : "+totalPayment_1 + "After RPO, totalPayment is : "+totalPayment_2, ExtentColor.GREEN));
			
			String firstPayment_1 = ActionHandler.getElementText(shoppingBasketContainer.firstPayment_1);
			String firstPayment_2 = ActionHandler.getElementText(shoppingBasketContainer.firstPayment_2);
			extentLogger.info(MarkupHelper.createLabel("firstPayment is : "+firstPayment_1 + "After RPO, firstPayment is : "+firstPayment_2, ExtentColor.GREEN));
			}
			ActionHandler.click(shoppingBasketContainer.retailerPriceContinueBtn);
			ActionHandler.wait(4);
		}

		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.onlineContinueBtn)) {
			shoppingBasketContainer.onlineContinueBtn.click();
			ActionHandler.wait(8);
		}
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.editAddressLink)) {
			ActionHandler.click(shoppingBasketContainer.editAddressLink);
			ActionHandler.wait(2);
			ActionHandler.click(shoppingBasketContainer.submitAddressBtn);
			ActionHandler.wait(2);
		}
		if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.submitAddressBtn))
		{
			shoppingBasketContainer.submitAddressBtn.click();
		ActionHandler.wait(2);
		}
		if ("Yes".equalsIgnoreCase(Register))  {
			ActionHandler.wait(20);
		}
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.Addresssearch)) {
			deliveryToHome(addrFlatNo, addrCity, addrPostCode);
		}
		
		if(!VerifyHandler.verifyElementPresent(shoppingBasketContainer.sagepay))
        {
            driver.navigate().refresh();
        }
		
		// Card Details
		cardPayment();

		// finance value Verification
		if (VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.financeDetailExpBtn)) {
			ActionHandler.click(shopBasketReserveVehicleContainer.financeDetailExpBtn);
		} 
	//	validateConfigData("FinanceDetails");
		if (VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.financeAppBtn)) {
		// next
		ActionHandler.click(shopBasketReserveVehicleContainer.financeAppBtn);
		extentLogger.info("Select ApplyVia Option :: " + financeApp);
		ActionHandler.wait(3);
		if ("Apply Online".equalsIgnoreCase(financeApp)) {
			onlineFinance();
			applicationForm(appTitle, forename, appMiddle, appsurname, appHTel, appEmail, appGender, appCountryOrig, appMarStatus, appDepend,
					appvehicUsg, appPostCosd, appCountry, appYrsAddr, appMonthAddr,appAccomd, appPost, appContryAddr, appEmplr, appPhoneEmpr,
					appOcup, appBasis, appCategEmpr, appYrEmpr, appMonthEmpr, appExisAggr, appGrossIncome, appAnnualIncom, appMonthlyMortg,
					appOtherExp, appExpChange, appAcType, appSortCode1, appSortCode2,
					appSortCode3, appAcNum, appAcYrBank);
		} 
		else {
			ActionHandler.click(shopBasketReserveVehicleContainer.retailerTab);
			ActionHandler.wait(2);
			ActionHandler.click(shopBasketReserveVehicleContainer.viaRetailerBtn);
			ActionHandler.wait(2);
	//		extentLogger.info(shopBasketReserveVehicleContainer.viaRetailerFinalTxt.getText());

		} 
		extentLogger.info("Final Page shopping basket Continue", MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("finalPageContinue")).build());  
		} 
		//Email
		MailinatorEmailTesting(Number,logId); 
			
		//Vista
		VISTA_Validation(Number,flow, Brand);
		
		//Magento Admins
		Magento_Admin_Validation(Number); 
		
	} 
	

	public void cardPayment() {
		try {
			ActionHandler.scrollToView(shopBasketReserveVehicleContainer.cardNameTxtBox);
			ActionHandler.setText(shopBasketReserveVehicleContainer.cardNameTxtBox, Constants.CARDNAME);
			ActionHandler.wait(2);
			ActionHandler.setText(shopBasketReserveVehicleContainer.cardNumberTxtBox, Constants.CARDNUMBER);
			System.out.print("Test");
			ActionHandler.wait(10); 
			
			ActionHandler.setText(shopBasketReserveVehicleContainer.cvvTxtBox, "123");
			ActionHandler.wait(2);
			
			ActionHandler.wait(2);
		//	ActionHandler.clickElement(shopBasketReserveVehicleContainer.expMonthDrpDown);
			ActionHandler.wait(1);
		//	new Select(shopBasketReserveVehicleContainer.expMonthDrpDown).selectByVisibleText("04-April");
		//	ActionHandler.wait(2);
		//	ActionHandler.clickElement(shopBasketReserveVehicleContainer.expYearDrpDown);
		//	ActionHandler.wait(1);
		//	new Select(shopBasketReserveVehicleContainer.expYearDrpDown).selectByValue("2021");
		
			Select selectByVisibleText = new Select (driver.findElement(By.id("sagepaysuitepi_expiration")));
            selectByVisibleText.selectByVisibleText("04 - April");
            ActionHandler.wait(1);
            Select selectByVisibleText1 = new Select (driver.findElement(By.id("sagepaysuitepi_expiration_yr")));
            selectByVisibleText1.selectByVisibleText("2023");

			
			ActionHandler.wait(5);  	
			ActionHandler.click(shopBasketReserveVehicleContainer.termConditionBtn);
			ActionHandler.wait(2);
			ActionHandler.click(shopBasketReserveVehicleContainer.paySecureBtn);

			ActionHandler.waitForElement(shopBasketReserveVehicleContainer.frame, 60);
			ActionHandler.switchToframe(shopBasketReserveVehicleContainer.frame); 

			ActionHandler.setText(shopBasketReserveVehicleContainer.framePassword, Constants.PASSWORD);
			ActionHandler.click(shopBasketReserveVehicleContainer.frameSubmitBtn); 
			ActionHandler.wait(8);
			driver.switchTo().parentFrame(); 
			LOGGER.info(shopBasketReserveVehicleContainer.paymentSuccessMsg.getText());
			extentLogger.info("Verifying payment success message"); 

			ActionHandler.wait(6);
			ActionHandler.waitForElement(shopBasketReserveVehicleContainer.paymentSuccessMsg, 100);
			LOGGER.info(shopBasketReserveVehicleContainer.paymentSuccessMsg.getText());
			//assertTrue(shopBasketReserveVehicleContainer.paymentSuccessMsg.getText().contains(Constants.PAYMENTSUCCESSMSG));
			extentLogger.info("Reservation number : " + shopBasketReserveVehicleContainer.reservationNum.getText());
			//Get Reservation number
			ActionHandler.click(shopBasketReserveVehicleContainer.reservationNum);
			 String Number1 = ActionHandler.getElementText(shopBasketReserveVehicleContainer.reservationNum);
			extentLogger.info("Total : " + shopBasketReserveVehicleContainer.reservationAmt.getText());
			extentLogger.info("Payment success Page", MediaEntityBuilder
					.createScreenCaptureFromPath(Utility.takeScreenShot("PamentsuccessPage")).build());
			extentLogger.info("vehicle Reserve Page", MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("VehicleReserve")).build()); 
			Number = Number1.toString();
			
		} catch (Exception e) {
			LOGGER.info(e.toString());  
		} 
	}

/*	public void saveShoppingBasket(String basketChoice, String Register, String regEmailId, String fName, String lName,
			String regPass, String logId, String pwd, String retailerName, String telEmail) throws Exception {

		extentLogger.info("Click on 'QA Order This Vehicle Link' Button");
		ActionHandler.click(financeContainer.orderVehicleLink);
		ActionHandler.wait(2);
	//	validateConfigData("ShoppingBasketPage");
		// ##########################################################
		extentLogger.info("Click on Save Shopping Basket Button");
		ActionHandler.waitForElement(shoppingBasketContainer.saveBasketLink, 10);
		ActionHandler.click(shoppingBasketContainer.saveBasketLink);
		extentLogger.info("Navigate to delivery method Log in page");

		// ##########################################################
		ActionHandler.wait(20);
		if ("yes".equalsIgnoreCase(Register)) {
			extentLogger.info("Select Register Option");
			 ActionHandler.click(shoppingBasketContainer.registerRadioBtn);
			ActionHandler.click(driver.findElement(By.linkText("REGISTER")));
			basketUserReg(regEmailId, regPass, fName, lName);
		} else {
			extentLogger.info("Select Log in Option");
			// ActionHandler.click(shoppingBasketContainer.lonInRadioBtn, 60);
			ActionHandler.click(driver.findElement(By.linkText("LOGIN")));
			ActionHandler.wait(5);
		}

       // ##########################################################
		deliveryMethodLogin(logId, pwd);
		// ##########################################################
		ActionHandler.wait(30);

		/*
		 * if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.
		 * saveBasketLink)) {
		 * ActionHandler.click(shoppingBasketContainer.saveBasketLink, 60);
		 * LOGGER.
		 * info("Click Save shopping Basket Link After Login >>>>>>>>>>>>>>>" );
		 * ActionHandler.wait(30); }
		 */

/*		extentLogger.info("Verified saved successfull message - " + Constants.SHOPPINGBSKETMSG + " " + logId);
		LOGGER.info(Constants.SHOPPINGBSKETMSG + " " + logId);
		assertTrue(VerifyHandler.verifyElementPresent(driver.findElement(
				By.xpath(shoppingBasketContainer.saveBasketMsg(Constants.SHOPPINGBSKETMSG + " " + logId)))));

	}  */

	public void sendToRetailerBasket(String Model, String flow, String basketChoice, String Register, String regEmailId, String fName,
			String lName, String regPass, String logId, String pwdDelMethod, String retailerName, String telEmail) throws Exception {
		        if(VerifyHandler.verifyElementPresent(summaryContainer.summaryTab)) {
		            ActionHandler.click(summaryContainer.summaryTab);    
		            ActionHandler.wait(2);
		        }
		if(VerifyHandler.verifyElementPresent(financeContainer.orderVehicleLinksalesDX)) {
			//Order this vehicle
			ActionHandler.click(driver.findElement(By.xpath("//*[text()='Order This Vehicle']")));
			ActionHandler.wait(10);
		}
		else  {             
		extentLogger.info("Click on '[Staging] Order This Vehicle Link' Button");
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath("//span[contains(text(),'[STAGING] ORDER THIS VEHICLE')]")));
		ActionHandler.wait(10);
			if(!VerifyHandler.verifyElementPresent(shoppingBasketContainer.continueBtn)) {
				ActionHandler.click(driver.findElement(By.xpath("//span[contains(text(),'[STAGING] ORDER THIS VEHICLE')]")));
				ActionHandler.wait(10);	
			}
		}
		extentLogger.info("Basket Content",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Basket Content")).build());
		
		
		if ("Continue&Edit".equalsIgnoreCase(basketChoice)) {
			String FP_Current = ActionHandler.getElementText(shoppingBasketContainer.choosenFP);
			extentLogger.log(Status.INFO, "Current Finance Product is : " +FP_Current);
			ActionHandler.wait(2);
			          
			//Handle Sales DX login popup when clicked from Configurator flow basket
			if (!"LTO".equals(flow)) {
				if (!"BTO".equals(flow)) {
					SalesDX_Login_PopUp();
				}
			}
		    
		    //Click on Edit button on Basket 
			ActionHandler.wait(7);
			ActionHandler.click(shoppingBasketContainer.Edit_FromBasket);    
		    ActionHandler.wait(7);
		    
		    ActionHandler.wait(30);
		         	 	     	 	
		   if (VerifyHandler.verifyElementPresent(financeContainer.EditFP)) {
				ActionHandler.click(financeContainer.EditFP);
		        ActionHandler.wait(5);
		   }
		    
		    // In the edit flow, If current FP is HP, then change it to Cash
		    if ((FP_Current).contains("HIRE PURCHASE")) {
		    	ActionHandler.click(financeContainer.Cash);
		        ActionHandler.wait(2);
		        ActionHandler.click(financeContainer.Cash);
		        ActionHandler.wait(2);
		        extentLogger.log(Status.INFO, "Selecting Finance Product to 'Cash Purchase' from HP ");
			}
		    
		    // In the edit flow, If current FP is PCP, then change it to HP
		    if ((FP_Current).contains("PERSONAL CONTRACT PURCHASE (PCP)")) {
		    	ActionHandler.click(financeContainer.HP);
		        ActionHandler.wait(2);
		        ActionHandler.click(financeContainer.HP);
		        ActionHandler.wait(2);
		        extentLogger.log(Status.INFO, "Selecting Finance Product to 'Hire Purchase' from PCP ");
			}
		    
		    // In the edit flow, If current FP is Cash, then change it to PCP
		    if ((FP_Current).contains("CASH PURCHASE")) {
		    	ActionHandler.click(financeContainer.PCP);
		        ActionHandler.wait(2);
		        ActionHandler.click(financeContainer.PCP);
		        ActionHandler.wait(2);
		        extentLogger.log(Status.INFO, "Selecting Finance Product to 'Personal Contract Purchase' from Cash");
			}
		    extentLogger.info("Edit Finance Product",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Product_Edit")).build());
			
		    //Add in PX
		    
		    if (VerifyHandler.verifyElementPresent(financeContainer.PartExchangeYes)) {
		    commonFuncObject.partExchange("BJ67UMO", "Good", "1250");
		    }
		    
		    extentLogger.info("Add Part Exchange  : ",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Add PX")).build());
		    
		    //For Cash, deduct from OTR
		    if (VerifyHandler.verifyElementPresent(financeContainer.DEDUCTFROMONTHEROADPRICE)) {
				ActionHandler.click(financeContainer.DEDUCTFROMONTHEROADPRICE);
				ActionHandler.wait(2);
		    }
		    
		    //For HP/PCP, use towards my deposit.
		    if (VerifyHandler.verifyElementPresent(financeContainer.useTowardDepositButton)) {
				ActionHandler.click(financeContainer.useTowardDepositButton);
				ActionHandler.wait(2);
		    }
		    
		    // click on get quote button
			if(VerifyHandler.verifyElementPresent(financeContainer.getYourQuote)) {
				ActionHandler.click(financeContainer.getYourQuote);
				ActionHandler.wait(2);
			}
			if(VerifyHandler.verifyElementPresent(financeContainer.acceptAndContinue)) {
				ActionHandler.click(financeContainer.acceptAndContinue);
				ActionHandler.wait(2);
			}    		           
			if (VerifyHandler.verifyElementPresent(financeContainer.continueToBuild)) {
				ActionHandler.click(financeContainer.continueToBuild);
				ActionHandler.wait(3);
			}
			
			extentLogger.info("Finance Quote",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote")).build());
			ActionHandler.pageScrollDown();
			extentLogger.info("Finance Quote_2",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Finance_Quote_2")).build());
		}
		
		//Validate Finance Product post editing
		String FP_Current = ActionHandler.getElementText(shoppingBasketContainer.choosenFP);
		extentLogger.log(Status.INFO, "Updated Finance Product is : " +FP_Current);
		extentLogger.info("Basket_after edit",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Basket_After_edit")).build());

		extentLogger.info("Click on Send To Retailer Button");
		ActionHandler.waitForElement(shoppingBasketContainer.continueBtn, 10);
		ActionHandler.click(shoppingBasketContainer.sendToRetailerLink);
		extentLogger.info("Navigate to delivery method Log in page");
		ActionHandler.wait(20);
		if ("yes".equalsIgnoreCase(Register)) {
			extentLogger.info("Select Register in Option");
			ActionHandler.click(shoppingBasketContainer.registerRadioBtn);
			ActionHandler.wait(10);
			basketUserReg(regEmailId, regPass, fName, lName);
		} else {
			extentLogger.info("Select Log in Option");
			ActionHandler.click(shoppingBasketContainer.lonInRadioBtn, 60);
			deliveryMethodLogin(logId, pwdDelMethod);
		}
		ActionHandler.wait(30);

		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.sendToRetailerLink)) {
			ActionHandler.click(shoppingBasketContainer.sendToRetailerLink, 60);
			LOGGER.info("Send To Retailer After Login >>>>>>>>>>>>>>>");
			ActionHandler.wait(30);
		}

		extentLogger.info("Selecting " + telEmail + " Option");
		driver.findElement(By.xpath(shoppingBasketContainer.retailerContactPref(telEmail))).click();

		if ("Telephone".equalsIgnoreCase(telEmail)) {
			ActionHandler.wait(2);
			if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.phoneEditLink))
				ActionHandler.click(shoppingBasketContainer.phoneEditLink);

			ActionHandler.wait(2);
			ActionHandler.setText(shoppingBasketContainer.homePhoneNumTxtBox, Constants.HOMEPHONENUMBER);
			ActionHandler.click(shoppingBasketContainer.phoneNumSubmitBtn);
			ActionHandler.wait(2);
		}
		
		selectingRetailer(retailerName);
		
		ActionHandler.wait(5);
		extentLogger.info("click on Send To Retailer Button");
		ActionHandler.click(shoppingBasketContainer.retailerTermsCond);
		ActionHandler.wait(8);

		extentLogger.info("Before clicking on Send to retailer  : ", MediaEntityBuilder
				.createScreenCaptureFromPath(Utility.takeScreenShot("BeforeRetailerFinalPage")).build());
		ActionHandler.click(shoppingBasketContainer.sendToRetailerBtn);
		ActionHandler.wait(15);

		extenVerificationObj.extentVerification("Validate success meassage", true,
				VerifyHandler.verifyElementPresent(shoppingBasketContainer.retilerSuccessMsg), "Yes");

		extentLogger.info("Reference number generated : " + shoppingBasketContainer.refNumTxt.getText());

		// finance value Verification
		if (VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.financeDetailExpBtn)) {
			ActionHandler.click(shopBasketReserveVehicleContainer.financeDetailExpBtn);
		}
		// Validating config Data
	//	validateConfigData("SendToRetailerfinalPage");

	} 

	public void deliveryMethodLogin(String userName, String password) throws Exception {
		try {

			ActionHandler.wait(3);
			extentLogger.info("Login with " + userName);
			shoppingBasketContainer.emailTxtBox.sendKeys(userName);
			shoppingBasketContainer.passwordTxtBox.sendKeys(password);
			ActionHandler.click(shoppingBasketContainer.loginBtn);
		} catch (Exception e) {
			LOGGER.error("Exception-deliveryMethodLogin : " + e);
			extentLogger.fail(MarkupHelper.createLabel(" deliveryMethodLogin @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail("deliveryMethodLogin");
		}
	}

	public void basketUserReg(String regEmailId, String regPass, String fName, String lName) {
		try {
				ActionHandler.wait(9);
				ActionHandler.setText(shoppingBasketContainer.emailTxtBox, regEmailId);
			//	ActionHandler.clearAndSetText(shoppingBasketContainer.regEmailTxtBox, regEmailId+i+"@mailinator.com");
				ActionHandler.setText(shoppingBasketContainer.regFNameTxtBox, fName);
				ActionHandler.setText(shoppingBasketContainer.regLNameTxtBox, lName);
				ActionHandler.setText(shoppingBasketContainer.passwordTxtBox, regPass);
				ActionHandler.click(shoppingBasketContainer.regAcceptLink);
				ActionHandler.wait(5);
				ActionHandler.click(shoppingBasketContainer.regContinueBtn);
				ActionHandler.wait(10);
				
				//Verify email, Cuurently NCOS not asking for verification
				//MailinatorEmailVerification(regEmailId);
				if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.regBackToLoginLink)) {
					ActionHandler.click(shoppingBasketContainer.regBackToLoginLink);	
					ActionHandler.wait(8);
					//shoppingBasketContainer.emailTxtBox.sendKeys(regEmailId);
					//ActionHandler.wait(2);
					//shoppingBasketContainer.passwordTxtBox.sendKeys(regPass);
					//ActionHandler.wait(2);
					//ActionHandler.click(shoppingBasketContainer.loginBtn);
					//ActionHandler.wait(30);
				}
		} catch (Exception e) {
			LOGGER.info(e.toString());
		}
	}

	public void selectingRetailer(String retailerName) {
		ActionHandler.wait(10);
		extentLogger.info("Selecting retailer " + retailerName);
		if(VerifyHandler.verifyElementPresent(shoppingBasketContainer.PreferedRetailer)) {
			String name = ActionHandler.getElementText(shoppingBasketContainer.PreferedRetailerName);
			if((name).contains("LANCASTER")) {
				ActionHandler.wait(5);
			}
			else {
				shoppingBasketContainer.retailerSearchTxtBox.clear();
				ActionHandler.wait(5);
				shoppingBasketContainer.retailerSearchTxtBox.clear();
				shoppingBasketContainer.retailerSearchTxtBox.sendKeys("LANCASTER WOLVERHAMPTON");
				ActionHandler.wait(5);
				shoppingBasketContainer.retailerSearch.click();
				ActionHandler.wait(5);
				ActionHandler.click(shoppingBasketContainer.firstRetailer);
			}
		}
		else {
		shoppingBasketContainer.retailerSearchTxtBox.clear();
		ActionHandler.wait(5);
		shoppingBasketContainer.retailerSearchTxtBox.clear();
		shoppingBasketContainer.retailerSearchTxtBox.sendKeys("LANCASTER WOLVERHAMPTON");
		ActionHandler.wait(5);
		shoppingBasketContainer.retailerSearch.click();
		ActionHandler.wait(5);
		ActionHandler.click(shoppingBasketContainer.firstRetailer);
		}
		}
/*		ActionHandler.wait(7);
		shoppingBasketContainer.retailerSearchTxtBox.clear();
		shoppingBasketContainer.retailerSearchTxtBox.sendKeys(retailerName);
		ActionHandler.wait(5);
		shoppingBasketContainer.retailerSearchResult.click();
		ActionHandler.wait(2); */
	/*	do {

			if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.findAnotherRetailerLink)) {
				ActionHandler.click(shoppingBasketContainer.findAnotherRetailerLink);
			}
			if (driver.findElements(By.xpath(shoppingBasketContainer.retailerbox(retailerName))).size() > 0) {
				ActionHandler.click(driver.findElement(By.xpath(shoppingBasketContainer.retailerbox(retailerName))));
				break;
			}

		} while (VerifyHandler.verifyElementPresent(shoppingBasketContainer.findAnotherRetailerLink));
		ActionHandler.wait(5); 
	} */

	public void deliveryToHome(String flatNo, String city, String postCode) {
		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.Addresssearch)) {
			ActionHandler.click(shoppingBasketContainer.Addresssearch);
			ActionHandler.wait(1);
			ActionHandler.clearAndSetText(shoppingBasketContainer.Addresssearch, "WV2 4HQ");
			ActionHandler.wait(2);
			ActionHandler.click(shoppingBasketContainer.EnterManually);
			ActionHandler.wait(5);
			ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrStreet1, flatNo);
			ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrCity, city);
			ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrPostCode, postCode);
			ActionHandler.wait(2);
			ActionHandler.click(shopBasketReserveVehicleContainer.addrSubmit);
			ActionHandler.wait(5);
		}
		else if (VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.addrStreet1)) {
		ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrStreet1, flatNo);
		ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrCity, city);
		ActionHandler.clearAndSetText(shopBasketReserveVehicleContainer.addrPostCode, postCode);
		ActionHandler.wait(2);
		ActionHandler.click(shopBasketReserveVehicleContainer.addrSubmit);
		ActionHandler.wait(5);
		
		} 
		if (VerifyHandler.verifyElementPresent(shoppingBasketContainer.homephone)) {
			ActionHandler.click(shoppingBasketContainer.homephone);
			ActionHandler.wait(1);
			ActionHandler.clearAndSetText(shoppingBasketContainer.homephone,"0987654321");
			ActionHandler.wait(2);
			ActionHandler.click(shoppingBasketContainer.submitphone);
			ActionHandler.wait(5);
		}
	} 

	public void onlineFinance() {
		try {
			ActionHandler.click(shopBasketReserveVehicleContainer.onlineTab);
			ActionHandler.wait(4);
			ActionHandler.click(shopBasketReserveVehicleContainer.viaOnlineBtn);
			ActionHandler.wait(4);
			ActionHandler.waitForElement(basketFinanceServiceContainer.onlineConfirmQuoteBtn, 60);
			ActionHandler.click(basketFinanceServiceContainer.onlineConfirmQuoteBtn);
			ActionHandler.wait(4);
			ActionHandler.waitForElement(basketFinanceServiceContainer.financeAckLink, 60);
			extentLogger.info("Accept Finance Term and Condition");
			ActionHandler.click(basketFinanceServiceContainer.financeAckLink);
			ActionHandler.wait(2);
			ActionHandler.click(basketFinanceServiceContainer.AgreeContinBtn);
			ActionHandler.wait(3);
		} catch (Exception e) {
			LOGGER.info(e.toString());
		}
	}         

	public void validateConfigData(String validate) throws Exception {

		extentLogger.info("validating Config details on " + validate + " Page : OTR ,Finance Option, PX, Monthly Cost",
				MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot(validate)).build());

		LOGGER.info(shopBasketReserveVehicleContainer.otrTxt.getText());
		if(VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.monthlyCostTxt)) {
		LOGGER.info(shopBasketReserveVehicleContainer.monthlyCostTxt.getText());
		}
		
		if(VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.monthlyCostTxt)) {
		LOGGER.info(shopBasketReserveVehicleContainer.financeOptionTxt.getText()); 
		
		} 
		extenVerificationObj.extentVerification("Verifying OTR " + shopBasketReserveVehicleContainer.otrTxt.getText(),
				shopBasketReserveVehicleContainer.otrTxt.getText(), configValues.get("OTR"), ""); 
		extenVerificationObj.extentVerification(
				"Verifying Monthly Cost " + shopBasketReserveVehicleContainer.monthlyCostTxt.getText(),
				shopBasketReserveVehicleContainer.monthlyCostTxt.getText(), configValues.get("MonthlyCost"), ""); 
		extenVerificationObj.extentVerification("Verifying Part Exchange " + configValues.get("PartExchange"),
				VerifyHandler.verifyElementPresent(shopBasketReserveVehicleContainer.partExCostTxt)
						? shopBasketReserveVehicleContainer.partExCostTxt.getText() : "",
				configValues.get("PartExchange"), ""); 
		extenVerificationObj.extentVerification("Verifying Finance Option " + configValues.get("FinanceOption"),
				shopBasketReserveVehicleContainer.financeOptionTxt.getText()
						.contains(configValues.get("FinanceOption")), 
				true, ""); 

	}
	
	public void SalesDX_Login_PopUp() throws Exception {
			String edit_url = driver.getCurrentUrl();
	        String[] arrSplit = edit_url.split("//");
	        for (int i=0; i < arrSplit.length; i++)
	        {
	          System.out.println(arrSplit[i]);
	        }
	        String fStr = arrSplit[0];
	        String sStr = arrSplit[1];
	        System.out.println("First String = "+fStr);
	        System.out.println("Second String = "+sStr);
	       
	        String s1=fStr+"//jlr-dev:NYeWJwJGNrTD@";
	        System.out.println("Updated String is = "+s1);
	        String s2=s1.concat(sStr);
	        System.out.println("Main String is = "+s2);
	        ActionHandler.wait(3);
	        driver.get(s2);
	        ActionHandler.wait(10);
	}

	public void applicationForm(String appTitle, String forename, String appMiddle, String appsurname, String appHTel,
			String appEmail, String appGender, String appCountryOrig, String appMarStatus, String appDepend,
			String appvehicUsg, String appPostCosd, String appCountry, String appYrsAddr, String appMonthAddr,
			String appAccomd, String appPost, String appContryAddr, String appEmplr, String appPhoneEmpr,
			String appOcup, String appBasis, String appCategEmpr, String appYrEmpr, String appMonthEmpr,
			String appExisAggr, String appGrossIncome, String appAnnualIncom, String appMonthlyMortg,
			String appOtherExp, String appExpChange, String appAcType, String appSortCode1, String appSortCode2,
			String appSortCode3, String appAcNum, String appAcYrBank) {
		ActionHandler.waitForElement(basketFinanceServiceContainer.appTitle, 100);
	
			try {
		extentLogger.info("Adding application form details");
		
		// Personal Details
		new Select(basketFinanceServiceContainer.appTitle).selectByVisibleText(appTitle);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appForeName, forename);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appMiddleName, appMiddle);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appSurname, appsurname);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appHomeTel, appHTel);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appEmail, appEmail);
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.appBirthDay).selectByValue("1");
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.appBirthMonth).selectByValue("10");
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.appBirthYear).selectByValue("1989");
		ActionHandler.wait(2);

		new Select(basketFinanceServiceContainer.appGender).selectByValue(appGender);
		ActionHandler.wait(2);

		new Select(basketFinanceServiceContainer.appCountry).selectByVisibleText(appCountryOrig);
		ActionHandler.wait(2);
		

		new Select(basketFinanceServiceContainer.appMaritalStatus).selectByVisibleText(appMarStatus);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appDependant, appDepend);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appVehicleUsage, appvehicUsg);
		ActionHandler.wait(5);
		// Click on addr History Tab

		ActionHandler.click(basketFinanceServiceContainer.appAddHistoryBtn);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHistoryPostCode, appPost);
		ActionHandler.wait(2);
		new Actions(driver).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();

		ActionHandler.wait(5);

		ActionHandler.setText(basketFinanceServiceContainer.appAddHisHouseName, "67");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisStreet, "XYZ");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisDistr, "London");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisTown, "London");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisCountry, appContryAddr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisYrsAdd, appYrsAddr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.appAddHisMonthsAdd, appMonthAddr);
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.appAddHisAcd).selectByVisibleText(appAccomd);
		ActionHandler.wait(5);
		// employee History

		ActionHandler.click(basketFinanceServiceContainer.empHistoryBtn);
		ActionHandler.wait(3);
		
		new Select(basketFinanceServiceContainer.empHisBasis).selectByVisibleText(appBasis);
		ActionHandler.wait(2);
		
		ActionHandler.setText(basketFinanceServiceContainer.empHisPostCode, appPost);
		ActionHandler.wait(2);
		new Actions(driver).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();

		ActionHandler.wait(5);

		ActionHandler.clearAndSetText(basketFinanceServiceContainer.empHisBuildName, "ABC");
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(basketFinanceServiceContainer.empHisStreet, "XYZ");
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(basketFinanceServiceContainer.empHisDistrict, "London");
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(basketFinanceServiceContainer.empHisCity, "London");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.empHisContry, appContryAddr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.empHisEmpr, appEmplr);
		ActionHandler.wait(2);
//		ActionHandler.click(basketFinanceServiceContainer.empHisPhone);
		ActionHandler.wait(2);
//		ActionHandler.setText(basketFinanceServiceContainer.empHisPhone, appPhoneEmpr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.empHisOccupation, appOcup);
		ActionHandler.wait(2);
		
		new Select(basketFinanceServiceContainer.empHisCat).selectByVisibleText(appCategEmpr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.empHisYrAtEmp, appYrEmpr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.empHisMonthemp, appMonthEmpr);
		ActionHandler.wait(5);
		// Affordability
		ActionHandler.click(basketFinanceServiceContainer.AffortdBtn);
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.afforLoan).selectByVisibleText(appExisAggr);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.afforGrossIncome, appGrossIncome);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.afforRent, appMonthlyMortg);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.afforExpend, appOtherExp);
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.afforFeatureObli).selectByVisibleText(appExpChange);
		ActionHandler.wait(5);
		// Bank Details
		ActionHandler.click(basketFinanceServiceContainer.bankDetailBtn);
		ActionHandler.wait(2);

	//	ActionHandler.setText(basketFinanceServiceContainer.bankAccountName, "ABCD");
		ActionHandler.wait(2);
		new Select(basketFinanceServiceContainer.bankAcType).selectByVisibleText(appAcType);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.BankSortCode1, appSortCode1);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.BankSortCode2, appSortCode2);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.BankSortCode3, appSortCode3);
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.bankAcNumber, appAcNum);
		ActionHandler.wait(2);
	//	ActionHandler.setText(basketFinanceServiceContainer.bankAcName, "ABCD");
		ActionHandler.wait(2);
	//	ActionHandler.setText(basketFinanceServiceContainer.bankAcBranch, "XYZ");
		ActionHandler.wait(2);
	//	ActionHandler.setText(basketFinanceServiceContainer.bankAcAddrs, "Address1");
		ActionHandler.wait(2);
		ActionHandler.setText(basketFinanceServiceContainer.bankAcYrs, appAcYrBank);
		ActionHandler.wait(5);
		extentLogger.info("Application Form Details  : ",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("ApplicationForm")).build());
		ActionHandler.wait(10);
		ActionHandler.click(basketFinanceServiceContainer.appFormSubmitBtn);
		ActionHandler.wait(20); 
		extentLogger.info("Confirmation  : ",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Confirmation")).build());
		ActionHandler.click(basketFinanceServiceContainer.FINISH);
		ActionHandler.wait(3);
		extentLogger.info("Confirmation  : ",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Confirmation")).build());
     	} catch (Exception e) {
		LOGGER.info("Exception - AppicationForm :: " + e); 
		
	} 
  }  
	
	public void VISTA_Validation(String Res_ID, String flow, String Brand) {
		//Open VISTA URL
		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://www.jlrvistaqa1.jlrint.com/Vista-NewWeb/rest/HomeController/VIEW/dashboard");
		ActionHandler.wait(35);
		
		//Choose Identity
		driver.findElement(By.xpath("//*[@tooltip='Change Identity']")).click();
		ActionHandler.wait(2);
		driver.switchTo().activeElement();
		ActionHandler.wait(2);
		
		if (Brand.equalsIgnoreCase("Jaguar")) {
			driver.findElement(By.xpath("//input[@class='ng-pristine ng-untouched ng-valid'][1]")).click();
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJImarket")).clear();
			driver.findElement(By.id("SAJImarket")).sendKeys("United Kingdom");
			driver.findElement(By.id("SAJImarket")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJIdistributor")).clear();
			driver.findElement(By.id("SAJIdistributor")).sendKeys("J2060 - Jaguar UK Ltd");
			driver.findElement(By.id("SAJIdistributor")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SAJIdealer")).clear();
			driver.findElement(By.id("SAJIdealer")).sendKeys("J0793 - Lancaster Jaguar, Wolverhampton");
			driver.findElement(By.id("SAJIdealer")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
		}
		
		if (Brand.equalsIgnoreCase("Landrover")) {
			driver.findElement(By.xpath("(//*[@class='ng-pristine ng-untouched ng-valid'])[3]")).click();
			ActionHandler.wait(2);
			driver.findElement(By.id("SALImarket")).clear();
			driver.findElement(By.id("SALImarket")).sendKeys("United Kingdom");
			driver.findElement(By.id("SALImarket")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SALIdistributor")).clear();
			driver.findElement(By.id("SALIdistributor")).sendKeys("M2400 - Land Rover");
			driver.findElement(By.id("SALIdistributor")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.id("SALIdealer")).clear();
			driver.findElement(By.id("SALIdealer")).sendKeys("H0875 - Lancaster Land Rover, Wolverhampton");
			driver.findElement(By.id("SALIdealer")).sendKeys(Keys.TAB);
			ActionHandler.wait(2);
			driver.findElement(By.xpath("//h4[@class='modal-title']")).click();
		}
			
		//Save identity
		driver.findElement(By.xpath("(//button[@class='btn form-btn'])[1]")).click();
		ActionHandler.wait(15);
		
		//Choose LTO & BTO 
		driver.findElement(By.xpath("(//*[@ng-click='notificationCountClick(notification.notificationType)'])[1]")).click();
		ActionHandler.wait(5);
		
		if (flow.equalsIgnoreCase("LTO")) {
			driver.findElement(By.xpath("(//*[contains(text(),'Located Online - Accept Ownership')])[1]")).click();
			ActionHandler.wait(5);
		}
		else {
		driver.findElement(By.xpath("(//*[contains(text(),'Ordered Online - Validate Configuration')])[1]")).click();
		ActionHandler.wait(5);
		}
		
		//Sort Reservation Ids
		driver.findElement(By.xpath("//span[@id='orderlist_title']")).click();
		ActionHandler.wait(2);
		driver.findElement(By.xpath("//*[@class='glyphicon glyphicon-arrow-up']")).click();
		ActionHandler.wait(1);
		driver.findElement(By.xpath("(//*[contains(text(),'Sort Z to A')])[1]")).click();
		ActionHandler.wait(7);
		
		//Validate Reservation ID in VISTA
		driver.findElement(By.xpath("(//span[@class='ng-binding ng-scope'])[1]")).click();
		ActionHandler.wait(1);
		String a = driver.findElement(By.xpath("(//span[@class='ng-binding ng-scope'])[1]")).getText();
		extentLogger.info("Vista Validation");
		
		if((a).equals(Res_ID)){
			extentLogger.pass(MarkupHelper.createLabel("Vista Validation Passed, Expected : "+Res_ID + "Actual: "+a , ExtentColor.GREEN));
		}
		else {
			extentLogger.fail(MarkupHelper.createLabel("Vista Validation Failed, Expected : "+Res_ID + "Actual: "+a, ExtentColor.RED));
		}
		driver.quit();
	}
	
	public void Magento_Admin_Validation(String Ref_ID) {
		
		       	//-----------------------------------code backup--------------------------------------------	
		//Open URL
		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		//Open URL
		driver.get("https://eu.ncosadmin.preview.jaguarlandrover.com/admin/admin/index/index/key/28c86ff61bc3b341e3fe30310e80b014d48150f2782a03ba342f68db82ff349c/");
		ActionHandler.wait(5);
		driver.findElement(By.xpath("//*[@name='login[username]']")).sendKeys("priya.kodhai");
		ActionHandler.wait(1);
		driver.findElement(By.xpath("//*[@name='login[password]']")).sendKeys("MRO6nvJTU4if");
		ActionHandler.wait(1);
		driver.findElement(By.xpath("//span[text()='Sign in']")).click();
		ActionHandler.wait(15);
		
		//Open NCOS-->Sales
		driver.findElement(By.xpath("(//button[@class='action-close'])[1]")).click();
		ActionHandler.wait(1);
		
		driver.findElement(By.xpath("(//span[text()='Sales'])[1]")).click();
		ActionHandler.wait(1);
		
		//Sales-->Orders
		driver.findElement(By.xpath("(//span[text()='Orders'])[1]")).click();
		ActionHandler.wait(10);
		
		//Search the reservation ID
		driver.findElement(By.xpath("(//*[@class='admin__control-text data-grid-search-control'])[1]")).click();
		ActionHandler.wait(1);
		driver.findElement(By.xpath("(//*[@class='admin__control-text data-grid-search-control'])[1]")).clear();
		//driver.findElement(By.xpath("(//*[@class='admin__control-text data-grid-search-control'])[1]")).sendKeys(Ref_ID);
		ActionHandler.wait(1);
		new Actions(driver).sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		
		//Validate Customer name in the results
		String CN = driver.findElement(By.xpath("(//div[@class='data-grid-cell-content'])[3]")).getText();
		ActionHandler.wait(1);
		System.out.print("Customer Name Validation");
		extentLogger.pass(MarkupHelper.createLabel("Customer Name Validation Passed, Expected : Kodhai TF"+"Actual : " +CN, ExtentColor.GREEN));
		
		//Validate Payment Status
		String Payment_status = driver.findElement(By.xpath("(//div[@class='data-grid-cell-content'])[5]")).getText();
		System.out.print("Validate Payment status");
		ActionHandler.wait(1);
		if(("Paid").equals(Payment_status)) {
			extentLogger.pass(MarkupHelper.createLabel("Payment status validated, Passed, Expected : Paid "+ "Actual is : "+Payment_status, ExtentColor.GREEN));
		}
		else {
			extentLogger.fail(MarkupHelper.createLabel("Payment status validated, Failed, Expected : Paid "+ "Actual is : "+Payment_status, ExtentColor.RED));
		}
		
		//Validate Retailer name
		String Retailer = driver.findElement(By.xpath("(//div[@class='data-grid-cell-content'])[7]")).getText();
		ActionHandler.wait(1);
		System.out.print("Validate Retailer Name");
		if(("Lancaster, Wolverhampton").equalsIgnoreCase(Retailer)) {
			extentLogger.pass(MarkupHelper.createLabel("Retailer validated, Expected : LandCaster "+ "Actual is : "+Retailer, ExtentColor.GREEN));
		}
		
		//View Order
		driver.findElement(By.linkText("View")).click();		
		ActionHandler.wait(5);
	
		//Validate Vista Order Status
		String VISTA_status = driver.findElement(By.xpath("//span[@id='order_status']")).getText();
		extentLogger.info("Validate VISTA Order status");
		ActionHandler.wait(1);
		if(("Order Created in Vista").equalsIgnoreCase(VISTA_status)) {
			extentLogger.pass(MarkupHelper.createLabel("Payment status validated, Expected : 'Order Created in Vista' "+ "Actual is : "+VISTA_status, ExtentColor.GREEN));
		}
		else if(("Finance Referred").equalsIgnoreCase(VISTA_status)) {
			extentLogger.pass(MarkupHelper.createLabel("Payment status validated, Expected : 'Finance Referred' "+ "Actual is : "+VISTA_status, ExtentColor.GREEN));
		}
		else if(("Finance Applied").equalsIgnoreCase(VISTA_status)) {
			extentLogger.pass(MarkupHelper.createLabel("Payment status validated, Expected : 'Finance Applied' "+ "Actual is : "+VISTA_status, ExtentColor.GREEN));
		}
		else {
			extentLogger.fail(MarkupHelper.createLabel("Payment status validated, Expected : 'Order Created in Vista/Finance Applied/Finance Referred' "+ "Actual is : "+VISTA_status, ExtentColor.RED));
		}
		
		//Validate Order number
		String Order_Number = driver.findElement(By.xpath("(//span[@class='title'])[2]")).getText();
		extentLogger.info("Validate Order_Number");
		ActionHandler.wait(1);
		//if((Order_Number).contains(Ref_ID)) {
			extentLogger.pass(MarkupHelper.createLabel("Validate Order Header, Expected : "+ Ref_ID + "Actual is : "+Order_Number, ExtentColor.GREEN));
		//}
		//else {
		//	extentLogger.fail(MarkupHelper.createLabel("Payment status Header, Expected : "+Ref_ID + "Actual is : "+Order_Number, ExtentColor.RED));
		//}
		
		try {
			extentLogger.info("Open Magento Admin  : ",	MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Magento Admin page")).build());
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		ActionHandler.pageScrollDown();
		driver.quit();
	}

	
//-------------------------------EMAIL Validation------------------------------------------
		public void MailinatorEmailTesting(String Reservation, String email) throws Exception {
		
           // Reservation = "255000002370";		
			System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.manage().window().maximize();

			driver.get("http://www.mailinator.com/");
			ActionHandler.wait(5);
			//extentLogger.info("Email validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Email")).build());
			driver.findElement(By.xpath("//*[@class='form-control input-text h-auto']")).sendKeys(email);
			ActionHandler.wait(2);
			driver.findElement(By.xpath("//button[@class='btn btn-go-public h-auto'][contains(text(),'GO')]")).click();
			ActionHandler.wait(2);
			
			//extentLogger.info("Customer name validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Customer_Name")).build());
			
			driver.findElement(By.xpath("(//*[contains(text(),'Your order is being processed')])[1]")).click();
			ActionHandler.wait(5);
			
			new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
			ActionHandler.wait(2);
			driver.switchTo().frame(driver.findElement(By.id("msg_body")));
			ActionHandler.wait(2);
			new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
			ActionHandler.wait(2);
			new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
			ActionHandler.wait(2);
			new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
			ActionHandler.wait(7);

			driver.findElement(By.xpath("//*[@class='side-text'][contains(text(),'Order number:')]")).click();
			String Order = driver.findElement(By.xpath("//*[@class='side-text'][contains(text(),'Order number:')]")).getText();
			ActionHandler.wait(5);
	     //   if((Order).contains(Reservation)) {
	        	System.out.println(Order);
	        	extentLogger.pass(MarkupHelper.createLabel("Order validated, Expected : "+Reservation + "Actual is : "+Order, ExtentColor.GREEN));
	     //   }
	        driver.quit();
		}		
		
		//Verify registration
		public void MailinatorEmailVerification(String email) throws Exception {
				System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				driver.manage().window().maximize();

				driver.get("http://www.mailinator.com/");
				ActionHandler.wait(5);
				//extentLogger.info("Email validation",MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot("Email")).build());
				driver.findElement(By.xpath("//*[@class='form-control input-text h-auto']")).sendKeys(email);
				ActionHandler.wait(2);
				driver.findElement(By.xpath("//button[@class='btn btn-go-public h-auto'][contains(text(),'GO')]")).click();
				ActionHandler.wait(2);
				driver.findElement(By.xpath("(//*[contains(text(),'You’ve successfully registered')])[1]")).click();
				ActionHandler.wait(5);
				new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
				ActionHandler.wait(2);
				driver.switchTo().frame(driver.findElement(By.id("msg_body")));
				ActionHandler.wait(2);
				new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
				ActionHandler.wait(2);
				new Actions(driver).sendKeys(Keys.PAGE_DOWN).build().perform();
				ActionHandler.wait(5); 
				driver.findElement(By.xpath("//a[contains(text(),'VERIFY')]")).click();
				ActionHandler.wait(5);
		        driver.quit();
			}		
		
		public void takeScreeshot(WebDriver driver, String filePath) throws Exception
	    {
	        TakesScreenshot screenshot = ((TakesScreenshot)this.driver);
	        File src = screenshot.getScreenshotAs(OutputType.FILE);
	        File dest = new File(filePath);
	        FileUtils.copyFile(src, dest);
	    }
 }
