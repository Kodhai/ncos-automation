package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.CPcontainers.ManageVehiclePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ8_ReminderRoadTax extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ManageVehiclePageContainer manageVehiclePageContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ8_ReminderRoadTax.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.LANDROVER);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();

			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			manageVehiclePageContainer = PageFactory.initElements(driver, ManageVehiclePageContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void reminderRoadTax(String vehicleName, String reminderDate, String note) {
		try {

			cpcj8(vehicleName, reminderDate, note);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj8(String vehicleName, String reminderDate, String note) {
		try {

			// Click on options and select vehicle
			ActionHandler.click(homePageContainer.expandIcon);
			driver.findElement(By.xpath(homePageContainer.vehicleSelection(vehicleName))).click();
			ActionHandler.wait(3);
			assertEquals(homePageContainer.vehicleNameHeader.getText(), vehicleName);
			extenVerificationObj.extentVerification("Verify Vehicle Selection",
					homePageContainer.vehicleNameHeader.getText(), vehicleName, "");

			// Click on edit reminders
			ActionHandler.click(homePageContainer.editReminders);
			ActionHandler.waitForElement(manageVehiclePageContainer.manageMyVehicleHeader, 20);

			// Enter Date
			commonFuncObject.enterDate(reminderDate, Constants.ROADTAX);

			// Enter Text
			ActionHandler.clearAndSetText(manageVehiclePageContainer.notes, note);
			ActionHandler.wait(4);
			// Click on Save and go back
			ActionHandler.scrollAndClick(manageVehiclePageContainer.saveButton);
			ActionHandler.wait(3);
			extenVerificationObj.extentVerification("Changes Saved Pop Up Verification:", Constants.CHANGESSAVED,
					manageVehiclePageContainer.changesSaved.getText(), "");
			ActionHandler.wait(3);
			ActionHandler.scrollAndClick(manageVehiclePageContainer.backButton);
			ActionHandler.wait(3);

			commonFuncObject.remainingDaysMonthsYearsValidations(reminderDate);
			
			

		} catch (Exception e) {
			extentLogger.error(e);
		}

	}



	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
