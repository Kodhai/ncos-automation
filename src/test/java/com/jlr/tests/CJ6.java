package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.JaguarGearContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class CJ6 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static JaguarGearContainer jaguarGearContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;

	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ6.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.JAGUARIPACE);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			jaguarGearContainer = PageFactory.initElements(driver, JaguarGearContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		} catch (Exception e) {

			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ6(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String jaguarGear, String wheelAccessories,
			String carryingTowing, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {

		testNcosCJ6(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, jaguarGear,
				wheelAccessories, carryingTowing, paymentOpt, advRetail, defaultAr, term, defaultTerm, annualMileage,
				defaultMileage, deposit, defaultDeposit);

	}

	public void testNcosCJ6(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String jaguarGear,
			String wheelAccessories, String carryingTowing, String paymentOpt, String advRetail, String defaultAr,
			String term, String defaultTerm, String annualMileage, String defaultMileage, String deposit,
			String defaultDeposit) {
		try {

			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.JAGUARIPACE);

			ActionHandler.wait(4);
			// Navigates to Finance Quote
			List<WebElement> defaultValueList = financeContainer.defaultValueList;
			String termValue = defaultValueList.get(1).getText().replaceAll("[^\\d.]", "");
			String annualMileageValue = defaultValueList.get(2).getText().replaceAll("[^\\d.]", "");
			// On UI decimal numbers are not allowed
			String totalDeposit = financeContainer.totalDepositDefault.getAttribute("value").replaceAll("[^\\d.]", "");

			// PCH
			driver.findElement(By.xpath(financeContainer.financeProduct(Constants.PCH))).click();
			extentLogger.log(Status.INFO, "Selecting Finance Product : PERSONAL CONTRACT HIRE (NON-MAINTAINED) ");
			assertEquals(ActionHandler.getElementText(financeContainer.partExchangeUnavailable),
					Constants.PARTEXCHANGEUNAVAILABLE);
			extenVerificationObj.extentVerification("Part Exchange Unavailable: ",
					ActionHandler.getElementText(financeContainer.partExchangeUnavailable),
					Constants.PARTEXCHANGEUNAVAILABLE, "");
			ActionHandler.wait(2);
			extentLogger.log(Status.INFO, "Selecting Finance Product : " + paymentOpt);
			ActionHandler.scrollToView(
					driver.findElement(By.xpath("//*[text()='1. CHOOSE THE BEST FINANCE OPTION FOR YOU']")));
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(financeContainer.financeProduct(paymentOpt))));
			ActionHandler.wait(2);
			// Validating default options of PCP
			commonFuncObject.verifyFinanceQuoteDetails(paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit, driver);

			// Part Exchange Validations
			ActionHandler.wait(2);
			commonFuncObject.partExchange_R1(Constants.NEGATIVEMILEAGE);
			assertEquals(financeContainer.negativeEquityMsg1.getText(), Constants.NEGATIVEEQUITYMSG1);
			extenVerificationObj.extentVerification("Negative Equity Validation: ",
					financeContainer.negativeEquityMsg1.getText(), Constants.NEGATIVEEQUITYMSG1, "");
			assertEquals(financeContainer.negativeEquityMsg2.getText(), Constants.NEGATIVEEQUITYMSG2);
			extenVerificationObj.extentVerification("Negative Equity Validation:",
					financeContainer.negativeEquityMsg2.getText(), Constants.NEGATIVEEQUITYMSG2, "");
			ActionHandler.click(financeContainer.acceptValuesCalculate);
			ActionHandler.waitForElement(financeContainer.continueToBuild, 20);
			// Quote Validations
			commonValidationObj.financeQuoteValidations(termValue, annualMileageValue,
					homePageContainer.onTheRoadPrice.getText(), totalDeposit, paymentOpt);
			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText()
					.replace(",", ""), annualMileageValue);
			extenVerificationObj.extentVerification("Verify Mileage per annum ", driver
					.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText().replace(",", ""),
					annualMileageValue, "");

			assertEquals(
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(Constants.PCP))).getText(),
					Constants.PCP);
			extenVerificationObj.extentVerification("Verify PCP Text",
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(Constants.PCP))).getText(),
					Constants.PCP, "");

			ActionHandler.click(financeContainer.continueToBuild);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(2);
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior : Colors");

				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Exterior : Wheels");

			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(2);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))).click();
				ActionHandler.wait(3);
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock, "Interior : Headlining");

			}

			if (("Yes").equals(jaguarGear)) {
				ActionHandler.click(jaguarGearContainer.jaguarGearTab);
				ActionHandler.wait(2);
				ActionHandler.click(jaguarGearContainer.wheelsAcc);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(wheelAccessories))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
						"Jaguar Gear: Wheel Accessories");

				ActionHandler.click(jaguarGearContainer.carryingTowing);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(carryingTowing))).click();
				commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
						"Jaguar Gear: Carrying & Towing");

			}

			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(2);

			LOGGER.info(homePageContainer.onTheRoadPrice.getText());
			LOGGER.info(summaryContainer.grandTotal.getText());
			String split[] = summaryContainer.grandTotal.getText().split(" ");
			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());

			String finalValue = homePageContainer.onTheRoadPrice.getText().replaceAll("[^\\d.]", "");
			System.out.println(summaryContainer.olevGrantValue.get(0).getText());
			if (summaryContainer.olevGrantValue.size() > 0)
				finalValue = String.valueOf(Double.parseDouble(finalValue) + Double
						.parseDouble(summaryContainer.olevGrantValue.get(0).getText().replaceAll("[^\\d.]", "")));

			finalValue = finalValue.contains(".00") ? finalValue.replace(".00", "") : finalValue;
			finalValue = finalValue.contains(".0") ? finalValue.replace(".0", "") : finalValue;

			String perMonth = homePageContainer.perMonthInfoBarValue.getText().replace(",", "");

			commonFuncObject.pdfValidation(Constants.JAGUAREPACE, models, engine, specif, nextStep, exterior, colours,
					visualPacks, interior, headlining, "No", "infotainment", jaguarGear, wheelAccessories);
			// Personal Details
		//	ActionHandler.click(summaryContainer.sendToRetailer);
			ActionHandler.wait(2);
			// String oldTab =
			// commonFuncObject.completePersonalDetails(Constants.JAGUARIPACE);

			// In IPace first edition case
			models = models.split(" ")[0];

			/*
			 * commonFuncObject.avokaTransact(models, engine, colours,
			 * headlining, paymentOpt, totalDeposit, "", perMonth,
			 * finalValue,"NO",Constants.VEHICLENAMER1,Constants.FUELTYPER1,
			 * Constants.REGISTRATIONNUMBER1);
			 */

			// Switching to Parent window
			// driver.switchTo().window(oldTab);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
