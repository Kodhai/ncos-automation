package com.jlr.tests;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.CPcontainers.BookAServiceContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsCustomerPortal;
import com.jlr.utilities.ExtenVerification;

public class CPCJ7_BookAService extends TestBase {

	private static BookAServiceContainer bookAServiceContainer = null;
	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CPCJ7_BookAService.class);
	CommonFunctionsCustomerPortal commonFuncObject = null;
	private static ExtenVerification extenVerificationObj = null;
	
	public CPCJ7_BookAService(){
		extentLogger = getextentLogger();
		driver = getDriver();
		commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUAREPACE);
		extenVerificationObj = new ExtenVerification(extentLogger);
		bookAServiceContainer = PageFactory.initElements(driver, BookAServiceContainer.class);
	}

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			setupTest(context.getCurrentXmlTest().getName());
			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsCustomerPortal(driver, extentLogger, Constants.JAGUAREPACE);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			bookAServiceContainer = PageFactory.initElements(driver, BookAServiceContainer.class);

		} catch (Exception e) {
			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void bookService(String month, String day, String timeSlot, String note, String title, String FirstName,
			String LastName, String mobileNo, String houseNo, String street, String city, String postCode) {
		try {

			cpcj7(month, day, timeSlot, note, title, FirstName, LastName, mobileNo, houseNo, street, city, postCode);

		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	public void cpcj7(String month, String day, String timeSlot, String note, String title, String firstName,
			String lastName, String mobileNo, String houseNo, String street, String city, String postCode)
			throws Exception {
		try {

			ActionHandler.wait(5);
			// get home page vehicle details
			String vehicleName = ActionHandler.getElementText(bookAServiceContainer.homePageVehicleName);
			String model = ActionHandler.getElementText(bookAServiceContainer.homePageVehicleModel);
			String engine = ActionHandler.getElementText(bookAServiceContainer.homePageVehicleEngine);
			String transmission = ActionHandler.getElementText(bookAServiceContainer.homePageVehicleTrans);

			// Click on edit Book A Service
			ActionHandler.scrollToView(bookAServiceContainer.bookAServiceLink);
			ActionHandler.click(bookAServiceContainer.bookAServiceLink);
			ActionHandler.wait(3);
			//ActionHandler.waitForElement(bookAServiceContainer.bookServiceHeader, 20);
			// Verifying car info on book A service page
			extenVerificationObj.extentInfo("Verifying vehicle details on Book a Service Page");
			ActionHandler.wait(10);
			extenVerificationObj.extentVerification("verifying vehicle Name",
					vehicleName.contains(ActionHandler.getElementText(bookAServiceContainer.bookSeviceVehicleName)),
					true, "");
			extenVerificationObj.extentVerification("verifying model Name",
					model.contains(ActionHandler.getElementText(bookAServiceContainer.bookServiceModelYear)), true, "");
			extenVerificationObj.extentVerification("verifying engine Name",
					engine.contains(
							ActionHandler.getElementText(bookAServiceContainer.bookServiceEngineSize).split(" ")[0]),
					true, "");
			extenVerificationObj.extentVerification("verifying Transmission",
					transmission.contains(
							ActionHandler.getElementText(bookAServiceContainer.bookServiceTrans).split(" ")[0]),
					true, "");

			List<WebElement> retailerNamesEle = bookAServiceContainer.bSNearestRetailerName;
			ArrayList<String> retailerText = new ArrayList<>();
			for (WebElement retailerNames : retailerNamesEle) {
				retailerText.add(retailerNames.getText());
			}

			// Selecting nearest retailer
			extenVerificationObj.extentInfo("Selecting Retailer - " + retailerText.get(0));

			ActionHandler.click(bookAServiceContainer.bSNearestRetailer);

			// Verifying service page message, select and get default
			// services
			ActionHandler.wait(5);
			ArrayList<String> defaultServicesText = defaultServices();
			// Click on Continue button
			ActionHandler.click(bookAServiceContainer.bSServiceContinueBtn);
			ActionHandler.wait(8);

			// Verifying Calendar tab message
			extenVerificationObj.extentVerification("Verifying Service Page", Constants.BSCALENDARPAGEMSG,
					ActionHandler.getElementText(bookAServiceContainer.bSCalTabMsg), "");
			ActionHandler.scrollToView(bookAServiceContainer.bSCalTabMsg);
			extenVerificationObj.extentInfo("Selecting " + month + " month & year");
			driver.findElement(By.xpath(bookAServiceContainer.calMonth(month))).click();
			ActionHandler.wait(4);
			extenVerificationObj.extentInfo("Selecting " + day + " day");
			driver.findElement(By.xpath(bookAServiceContainer.calDay(day))).click();
			ActionHandler.wait(2);
			extenVerificationObj.extentInfo("Selecting " + timeSlot + " Time slot");
			driver.findElement(By.xpath(bookAServiceContainer.timeSlot(timeSlot))).click();
			ActionHandler.click(bookAServiceContainer.bSCalContnBtn);
			ActionHandler.wait(8);
			// Personal Info Page
			setPersonalInfo(title, firstName, lastName, mobileNo, houseNo, street, city, postCode);

			verifySummeryData(vehicleName, model, engine, transmission, title, firstName, lastName, mobileNo, houseNo,
					street, city, month, day, timeSlot, postCode, retailerText, defaultServicesText);
			// extenVerificationObj.extentInfo("Clicking On amend button");
			// ActionHandler.click(bookAServiceContainer.bSPDAmendBtn);
		} catch (Exception e) {
			extenVerificationObj.extentError("cpcj7 - Exception :: " + e.toString());
		}

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

	public void setPersonalInfo(String title, String FirstName, String LastName, String mobileNo, String houseNo,
			String street, String city, String postCode) throws Exception {
		try {

			if (!bookAServiceContainer.bSPDTitleBtn.getText().equalsIgnoreCase(title)) {
				bookAServiceContainer.bSPDTitleBtn.click();
				ActionHandler.wait(2);
				driver.findElement(By.xpath(bookAServiceContainer.title(title))).click();
			}
			ActionHandler.setText(bookAServiceContainer.bSPDFirstName, FirstName);
			ActionHandler.setText(bookAServiceContainer.bSPDLastName, LastName);
			ActionHandler.setText(bookAServiceContainer.bSPDTelephone, "+"+mobileNo);
			ActionHandler.setText(bookAServiceContainer.bSPDHouseNumber, houseNo);
			ActionHandler.setText(bookAServiceContainer.bSPDStreet, street);
			ActionHandler.setText(bookAServiceContainer.bSPDCity, city);
			ActionHandler.setText(bookAServiceContainer.bSPDPostcode, postCode);
			bookAServiceContainer.bSPDPolicyChk.click();
			bookAServiceContainer.bSPDTermChk.click();

			ActionHandler.click(bookAServiceContainer.bSPDContinueBtn);
			ActionHandler.wait(5);
		} catch (Exception e) {
			extenVerificationObj.extentError(e.toString());
		}
	}

	public ArrayList<String> defaultServices() throws Exception {
		extenVerificationObj.extentVerification("Verifying Service Page", Constants.BSSERVICEPAGEMSG,
				ActionHandler.getElementText(bookAServiceContainer.bSServiceTabMsg), "");
		extenVerificationObj.extentInfo("Click on 'I have service plan' option ");
		bookAServiceContainer.bSServicePlanChk.click();
		extenVerificationObj.extentInfo("Verifying COMPLIMENTARY default Options");
		List<WebElement> defaultOptions = bookAServiceContainer.bSServiceDefaultPlanChk;
		ActionHandler.scrollToView(defaultOptions.get(0));
		ActionHandler.wait(2);
		List<WebElement> defaultServiceEle = bookAServiceContainer.bSDefaultServicesList;
		ArrayList<String> defaultServicesText = new ArrayList<>();
		for (WebElement servicesNames : defaultServiceEle) {
			defaultServicesText.add(servicesNames.getText());
		}

		int count = 0;
		for (WebElement defaultOption : defaultOptions) {
			if (ActionHandler.getAttributeValue(defaultOption, "class").contains("ng-not-empty")) {
				count = count + 1;
			}
		}
		extenVerificationObj.extentVerification("" + count + " COMPLIMENTARY default Options are selected ",
				String.valueOf(defaultOptions.size()), String.valueOf(count), "");

		return defaultServicesText;
	}

	public void verifySummeryData(String vehicleName, String model, String engine, String transmission, String title,
			String firstName, String lastName, String mobileNo, String houseNo, String street, String city,
			String month, String day, String timeSlot, String postCode, ArrayList<String> retailerText,
			ArrayList<String> defaultServicesText) throws Exception {
		try {
			extenVerificationObj.extentInfo("Verifying data on Summary Page");
			Boolean flag = false;
			if (vehicleName.contains(ActionHandler.getElementText(bookAServiceContainer.bookSeviceVehicleName))
					&& model.contains(ActionHandler.getElementText(bookAServiceContainer.bookServiceModelYear))
					&& engine.contains(
							ActionHandler.getElementText(bookAServiceContainer.bookServiceEngineSize).split(" ")[0])
					&& transmission.contains(
							ActionHandler.getElementText(bookAServiceContainer.bookServiceTrans).split(" ")[0])) {
				flag = true;
			}
			extenVerificationObj.extentVerification("Verified Personal data on Summary Page", true, flag, "");

			extenVerificationObj.extentVerification("Verified retailer data on Summary Page", true,
					ActionHandler.getElementText(bookAServiceContainer.bSPDSummaryRetailerName)
							.contains(retailerText.get(0).replace(",", "")),
					"");
			ActionHandler.scrollToView(bookAServiceContainer.bSPDSummaryDateTime);
			String time = timeSlot.split("-")[0];
			String dateExp = day + " " + month.toUpperCase() + " " + time.trim();
			String dateAct = ActionHandler.getElementText(bookAServiceContainer.bSPDSummaryDateTime).toUpperCase();
			extenVerificationObj.extentVerification("Verified Schedule on Summary Page", true,
					dateAct.contains(dateExp), "");

			flag = false;
			String name = firstName + " " + lastName;
			String address = houseNo + " " + street + ", " + city + ", " + postCode;
			if (name.equalsIgnoreCase(ActionHandler.getElementText(bookAServiceContainer.bSPDSummaryName))
					&& ("+"+mobileNo).contains(ActionHandler.getElementText(bookAServiceContainer.bSPDSummaryMobile))
					&& address.equalsIgnoreCase(ActionHandler.getElementText(bookAServiceContainer.bSPDSummaryAddr))) {
				flag = true;
			}
			extenVerificationObj.extentVerification("Verified personal data on Summary Page", true, flag, "");

			ActionHandler.scrollToView(bookAServiceContainer.bSSummaryConfirmationLabel);
			ActionHandler.click(bookAServiceContainer.bSSummaryContinueBtn);
			ActionHandler.wait(10);

			LOGGER.info(Constants.BSCONFIRMMSG + " " + dateAct.replace(time.trim(), "AT " + time.trim()));

			extenVerificationObj.extentVerification("Verifying confirmation Message ",
					Constants.BSCONFIRMMSG + ": " + dateAct.replace(time.trim(), "AT " + time.trim()),
					ActionHandler.getElementText(bookAServiceContainer.bSPDConfirmedMsg), "");
		} catch (Exception e) {
			extenVerificationObj.extentError(e.toString());
		}
	}
}
