package com.jlr.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.ExteriorContainer;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.InteriorContainer;
import com.jlr.containers.JaguarGearContainer;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;
import com.jlr.data.DataReader;
import com.jlr.utilities.CommonFunctionsNcos;
import com.jlr.utilities.CommonValidations;
import com.jlr.utilities.ExtenVerification;

public class CJ16 extends TestBase {

	private static HomePageContainer homePageContainer = null;
	private static ExteriorContainer exteriorContainer = null;
	private static InteriorContainer interiorContainer = null;
	private static JaguarGearContainer jaguarGearContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static FinanceContainer financeContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	public static String journeyName = "";

	private WebDriver driver;
	public ExtentTest extentLogger;
	private static final Logger LOGGER = LoggerFactory.getLogger(CJ16.class);
	CommonFunctionsNcos commonFuncObject = null;
	CommonValidations commonValidationObj = null;

	@BeforeMethod
	public void init(ITestContext context) {
		try {
			LOGGER.info("starting test Before Method");
			journeyName = context.getCurrentXmlTest().getName();
			setupTest(journeyName);

			extentLogger = getextentLogger();
			driver = getDriver();
			commonFuncObject = new CommonFunctionsNcos(driver, extentLogger, Constants.JAGUARIPACE);
			commonValidationObj = new CommonValidations(driver, extentLogger);
			extenVerificationObj = new ExtenVerification(extentLogger);
			commonFuncObject.userLogin();
			homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
			exteriorContainer = PageFactory.initElements(driver, ExteriorContainer.class);
			interiorContainer = PageFactory.initElements(driver, InteriorContainer.class);
			jaguarGearContainer = PageFactory.initElements(driver, JaguarGearContainer.class);
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);
			financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		} catch (Exception e) {

			extentLogger.error("init : " + e);
		}
	}

	@Test(dataProvider = "getdata", dataProviderClass = DataReader.class)
	public void ncosCJ16(String models, String engine, String specif, String nextStep, String exterior, String colours,
			String visualPacks, String interior, String headlining, String jaguarGear, String wheelAccessories,
			String carryingTowing, String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileage, String defaultMileage, String deposit, String defaultDeposit) {

		testNcosCJ16(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, jaguarGear,
				wheelAccessories, carryingTowing, paymentOpt, advRetail, defaultAr, term, defaultTerm, annualMileage,
				defaultMileage, deposit, defaultDeposit);

	}

	public void testNcosCJ16(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String jaguarGear,
			String wheelAccessories, String carryingTowing, String paymentOpt, String advRetail, String defaultAr,
			String term, String defaultTerm, String annualMileage, String defaultMileage, String deposit,
			String defaultDeposit) {
		try {
			String totalDeposit = "", perMonth = "", finalValue = "", expectedCashback = "";
			commonFuncObject.homePage(models, engine, specif, nextStep, paymentOpt, Constants.JAGUARIPACE);

			ActionHandler.wait(4);

			// Part Exchange Validations
			extentLogger.log(Status.INFO, "Selecting Finance Product :" + Constants.PCP + " With Part Exchange");
			ActionHandler.wait(2);
			commonFuncObject.partExchange("", "", Constants.MILEAGE);
			ActionHandler.click(financeContainer.useTowardDepositButton);

			// PCH
			extentLogger.log(Status.INFO, "Selecting Finance Product : PERSONAL CONTRACT HIRE (NON-MAINTAINED) ");
			ActionHandler.pageCompleteScrollUp();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(financeContainer.financeProduct(Constants.PCH))));
			assertEquals(ActionHandler.getElementText(financeContainer.partExchangeUnavailable),
					Constants.PARTEXCHANGEUNAVAILABLE);
			extenVerificationObj.extentVerification("Part Exchange Unavailable: ",
					ActionHandler.getElementText(financeContainer.partExchangeUnavailable),
					Constants.PARTEXCHANGEUNAVAILABLE, "");

			// Navigates to Finance Quote
			List<WebElement> defaultValueList = financeContainer.defaultValueList;
			String termValue = defaultValueList.get(1).getText().replaceAll("[^\\d.]", "");
			String annualMileageValue = defaultValueList.get(2).getText().replaceAll("[^\\d.]", "");

			// Validating default options of PCH
			commonFuncObject.verifyFinanceQuoteDetails(paymentOpt, advRetail, defaultAr, term, defaultTerm,
					annualMileage, defaultMileage, deposit, defaultDeposit, driver);

			ActionHandler.click(financeContainer.getYourQuote);
			ActionHandler.waitForElement(financeContainer.continueToBuild, 20);

			// Quote Validations
			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Duration Of Agreement"))).getText()
					.replaceAll("[^\\d.]", ""), termValue);
			extenVerificationObj.extentVerification("Verify Duration Of Agreement ",
					driver.findElement(By.xpath(financeContainer.getQuote("Duration Of Agreement"))).getText()
							.replaceAll("[^\\d.]", ""),
					termValue, "");

			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText()
					.replace(",", ""), annualMileageValue);
			extenVerificationObj.extentVerification("Verify Mileage per annum ", driver
					.findElement(By.xpath(financeContainer.getQuote("Mileage per annum"))).getText().replace(",", ""),
					annualMileageValue, "");

			assertEquals(
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(paymentOpt))).getText(),
					Constants.PCH);
			extenVerificationObj.extentVerification("Verify PCH Text",
					driver.findElement(By.xpath(financeContainer.summaryFinanceProductValue(paymentOpt))).getText(),
					Constants.PCH, "");

			// Validating finance quote has some number
			String perMonthValue = homePageContainer.perMonthInfoBarValue.getText().replaceAll("£", "");
			try {
				extentLogger.log(Status.INFO,
						Double.parseDouble(perMonthValue) + " is a valid number under Finance Quote Info Bar");
			} catch (NumberFormatException e) {
				extentLogger.log(Status.INFO, perMonthValue + " is not a valid number under Finance Quote Info Bar");
			}

			ActionHandler.click(financeContainer.continueToBuild);

			if (("Yes").equals(exterior)) {
				ActionHandler.click(exteriorContainer.exteriorTab);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(colours))).click();
				ActionHandler.wait(2);
				// commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
				// "Exterior : Colors");

				ActionHandler.click(exteriorContainer.visualPacks);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(exteriorContainer.selectGivenCategory(visualPacks))).click();
				// commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
				// "Exterior : Wheels");

			}

			if (("Yes").equals(interior)) {
				ActionHandler.click(interiorContainer.interiorTab);
				ActionHandler.wait(2);
				ActionHandler.click(interiorContainer.headlining);
				ActionHandler.wait(4);
				driver.findElement(By.xpath(interiorContainer.selectGivenCategory(headlining))).click();
				ActionHandler.wait(3);
				// commonFuncObject.validateMonthlyValues(exteriorContainer.monthlyPriceBlock,
				// "Interior : Headlining");

			}

			if (("Yes").equals(jaguarGear)) {
				ActionHandler.click(jaguarGearContainer.jaguarGearTab);
				ActionHandler.wait(2);
				ActionHandler.click(jaguarGearContainer.wheelsAcc);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(wheelAccessories))).click();
				/*
				 * commonFuncObject.validateMonthlyValues(exteriorContainer.
				 * monthlyPriceBlock, "Jaguar Gear: Wheel Accessories");
				 */

				ActionHandler.click(jaguarGearContainer.carryingTowing);
				ActionHandler.wait(2);
				driver.findElement(By.xpath(jaguarGearContainer.selectGivenCategory(carryingTowing))).click();
				/*
				 * commonFuncObject.validateMonthlyValues(exteriorContainer.
				 * monthlyPriceBlock, "Jaguar Gear: Carrying & Towing");
				 */

			}

			ActionHandler.click(summaryContainer.summaryTab);
			ActionHandler.wait(2);
			String split[] = summaryContainer.grandTotal.getText().split(" ");

			assertEquals(split[0], homePageContainer.onTheRoadPrice.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar OTR Validation: ", split[0],
					homePageContainer.onTheRoadPrice.getText(), "");
			finalValue = homePageContainer.onTheRoadPrice.getText();
			perMonth = homePageContainer.perMonthInfoBarValue.getText();
			assertEquals(split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText());
			extenVerificationObj.extentVerification("Summary and Info Bar Per Month Validation: ",
					split[1].replace(" p/m", ""), homePageContainer.perMonthInfoBarValue.getText(), "");

			// Personal Details
			ActionHandler.click(summaryContainer.orderNow);
			ActionHandler.wait(2);
			String oldTab = commonFuncObject.completePersonalDetails(Constants.JAGUARIPACE);

			// Switching to Parent window
			driver.switchTo().window(oldTab);
		} catch (Exception e) {
			extentLogger.error(e);
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {

			extentLogger.fail(MarkupHelper.createLabel(result.getName() + " Test case failed. @ScreenShot : "
					+ extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()), ExtentColor.RED));
			extentLogger.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// extentLogger.pass(MarkupHelper.createLabel(result.getName() + "
			// Test case passed", ExtentColor.GREEN));
		} else {
			extentLogger.skip(MarkupHelper.createLabel(result.getName() + " Test case skipped", ExtentColor.PINK));
		}
		commonFuncObject.userLogsOut();
		getDriver().quit();
	}

}
