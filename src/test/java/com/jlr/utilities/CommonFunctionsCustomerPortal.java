
package com.jlr.utilities;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.jlr.CPcontainers.HomePageContainer;
import com.jlr.CPcontainers.LoginContainer;
import com.jlr.CPcontainers.ManageVehiclePageContainer;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.contants.Constants;

public class CommonFunctionsCustomerPortal extends TestBase {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunctionsCustomerPortal.class.getName());
	private static LoginContainer loginContainer = null;
	private static HomePageContainer homePageContainer = null;
	private static ManageVehiclePageContainer manageVehiclePageContainer = null;
	private static ExtenVerification extenVerificationObj = null;
	public WebDriver driver = null;
	public ExtentTest extentLogger = null;
	public String LoginPortal = null;

	public CommonFunctionsCustomerPortal(WebDriver commanDriver, ExtentTest commanExtent, String portal) {
		driver = commanDriver;
		extentLogger = commanExtent;
		extenVerificationObj = new ExtenVerification(commanExtent);
		LoginPortal = portal;
	}

	public void userLogin() {
		String url = null;
		// ActionHandler.clearBrowserCache();
		loginContainer = PageFactory.initElements(driver, LoginContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
		manageVehiclePageContainer = PageFactory.initElements(driver, ManageVehiclePageContainer.class);
		try {
			if ((Constants.LANDROVER).equals(LoginPortal)) {
				url = Config.getPropertyValue("application.cpUrl.landrover");
			} else {
				url = Config.getPropertyValue("application.cp.Url.jaguar");
			}

			LOGGER.info("URL : " + url);
			driver.get(url);
			ActionHandler.wait(2);
		} catch (IOException e) {
			LOGGER.error("application url not found. Reverting to default url: " + Constants.DEFAULTURLCP);
			driver.get(Constants.DEFAULTURLCP);
		}
		userEntersUsernameForgeRock();
		userEntersPasswordForgeRock();
		userClicksOnLoginButtonForgeRock();
		ActionHandler.waitForElement(loginContainer.loginButton, 30);
		assertEquals(loginContainer.loginButton.getAttribute("value"), Constants.LOGIN);
		
		if((Constants.JAGUAREPACE).equals(LoginPortal)){
			userRegistration();
		}
		

		userEntersUsername();
		userEntersPassword();
		ActionHandler.wait(5);
		userClicksOnLoginButton();
		ActionHandler.waitForElement(homePageContainer.welcomeMessage, 30);
		assertEquals(homePageContainer.welcomeMessage.getText(), Constants.HELLO);
		LOGGER.info("User has successfully logged in");
	}

	public void userEntersUsernameForgeRock() {
		String email = null;
		try {
			email = Config.getPropertyValue("cp.email");
			assertTrue(StringUtils.isNotBlank(email));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(email));
		ActionHandler.setText(loginContainer.forgeRockUsername, email);

	}

	public void userRegistration() {
		ActionHandler.click(loginContainer.registerLink);
		ActionHandler.wait(3);
		LOGGER.info("Clicked on Register");
		try {
			ActionHandler.clearAndSetText(loginContainer.emailId, Config.getPropertyValue("register.emailId"));
			LOGGER.info("Entered Email ID");
			ActionHandler.clearAndSetText(loginContainer.firstName, Config.getPropertyValue("register.firstname"));
			LOGGER.info("Entered First Name");	
			ActionHandler.clearAndSetText(loginContainer.lastName, Config.getPropertyValue("register.lastname"));
			LOGGER.info("Entered Last Name");
			ActionHandler.clearAndSetText(loginContainer.passwordRegister,
					Config.getPropertyValue("cp.forgerock.password"));
			LOGGER.info("Entered Password");
		} catch (IOException e) {
			LOGGER.error("IOException : ", e);
		}

		ActionHandler.click(loginContainer.termsConditions);
		ActionHandler.click(loginContainer.registerNowBackToLogin);
		ActionHandler.wait(3);
		ActionHandler.click(loginContainer.registerNowBackToLogin);

	}

	public void userEntersPasswordForgeRock() {

		String password = null;
		try {
			password = Config.getPropertyValue("cp.forgerock.password");
			assertTrue(StringUtils.isNotBlank(password));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(password));
		ActionHandler.setText(loginContainer.forgeRockPassword, password);
		LOGGER.info("Entered password");
	}

	public void userClicksOnLoginButtonForgeRock() {
		ActionHandler.click(loginContainer.forgeRockLoginButton);
		LOGGER.info("Clicked final sign in button");
		ActionHandler.wait(2);
	}

	public void userEntersUsername() {
		String email = null;
		try {
			email = Config.getPropertyValue("cp.email");
			assertTrue(StringUtils.isNotBlank(email));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(email));
		ActionHandler.setText(loginContainer.username, email);

	}

	public void userEntersPassword() {

		String password = null;
		try {
			password = Config.getPropertyValue("cp.password");
			assertTrue(StringUtils.isNotBlank(password));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(password));
		ActionHandler.setText(loginContainer.password, password);
		LOGGER.info("Entered password");
	}

	public void userClicksOnLoginButton() {
		ActionHandler.click(loginContainer.loginButton);
		LOGGER.info("Clicked final sign in button");
		ActionHandler.wait(2);
	}

	public void userLogsOut() throws Exception {

		try {
			extentLogger.info("Before logOut from CustomerPortal ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			ActionHandler.click(homePageContainer.moreOption);
			ActionHandler.wait(3);
			ActionHandler.click(homePageContainer.logOut);
			ActionHandler.wait(3);
			LOGGER.info("User has successfully logged out");
		} catch (Exception e) {
			LOGGER.error(e.toString());
		}
	}

	public void enterDate(String dateToBeEntered, String dateType) {
		extentLogger.log(Status.INFO, "Entering Date in " + dateType + " With Value:" + dateToBeEntered);
		if (("false").equals(driver.findElement(By.xpath(manageVehiclePageContainer.enabledDisabledToggle(dateType)))
				.getAttribute("aria-checked"))) {
			driver.findElement(By.xpath(manageVehiclePageContainer.dateToggle(dateType))).click();
		}
		ActionHandler.wait(3);
		String splitData[] = dateToBeEntered.split("/");
		ActionHandler.clearAndSetText(driver.findElement(By.xpath(manageVehiclePageContainer.enterDay(dateType))),
				splitData[0]);
		ActionHandler.clearAndSetText(driver.findElement(By.xpath(manageVehiclePageContainer.enterMonth(dateType))),
				splitData[1]);
		ActionHandler.clearAndSetText(driver.findElement(By.xpath(manageVehiclePageContainer.enterYear(dateType))),
				splitData[2]);
		reminderInfoValidation(dateType);
	}

	public String remainingDaysMonthsYearsValidations(String reminderDate) throws Exception {

		String splitData[] = reminderDate.split("/");

		String month = splitData[0].indexOf('0') == 0 ? splitData[0].replace("0", "") : splitData[0];
		String day = splitData[1].indexOf('0') == 0 ? splitData[1].replace("0", "") : splitData[1];

		LocalDate pdate = LocalDate.of(Integer.parseInt(splitData[2]), Integer.parseInt(month), Integer.parseInt(day));
		LocalDate now = LocalDate.now();

		int diffInDays = (int) ChronoUnit.DAYS.between(now, pdate);
		int diffInMonths = (int) ChronoUnit.MONTHS.between(now, pdate);

		LOGGER.info(" Months:" + diffInMonths + " Days:" + diffInDays);

		String differenInDates = null;

		// Red
		if (diffInDays <= 30) {
			differenInDates = diffInDays + "DAYS";
			assertEquals(driver.findElement(By.xpath(homePageContainer.reminderDayColor(Constants.ROADTAX)))
					.getCssValue("background-color"), Constants.RED);
			extentLogger.log(Status.INFO, "Validation of Red Color successful");
		}
		// Amber
		else if (diffInDays >= 31 && diffInDays <= 90) {
			differenInDates = diffInMonths == 1 ? diffInMonths + "MONTH" : diffInMonths + "MONTHS";
			// dayColor[1]=Constants.AMBER;
			assertEquals(driver.findElement(By.xpath(homePageContainer.reminderDayColor(Constants.ROADTAX)))
					.getCssValue("background-color"), Constants.AMBER);
			extentLogger.log(Status.INFO, "Validation of Amber Color successful");

		} else if (diffInDays >= 91) {
			differenInDates = diffInMonths + "MONTHS";
			// dayColor[1]=Constants.WHITE;
			assertEquals(driver.findElement(By.xpath(homePageContainer.reminderDayColor(Constants.ROADTAX)))
					.getCssValue("background-color"), Constants.WHITE);
			extentLogger.log(Status.INFO, "Validation of White Color successful");

		}
		LOGGER.info(" Difference in Dates:" + differenInDates);
		LOGGER.info("Background Color Value:"
				+ driver.findElement(By.xpath("//span[text()='Road Tax due date']/parent::div//preceding::div[1]"))
						.getCssValue("background-color"));

		assertEquals(driver.findElement(By.xpath(homePageContainer.reminderDayColor(Constants.ROADTAX))).getText()
				.replaceAll("\\s", ""), differenInDates);
		extenVerificationObj.extentVerification("Remaining Days/Month validation:",
				driver.findElement(By.xpath(homePageContainer.reminderDayColor(Constants.ROADTAX))).getText()
						.replaceAll("\\s", ""),
				differenInDates, "");

		// Validate date is set correctly
		LOGGER.info(driver.findElement(By.xpath(homePageContainer.reminderDate(Constants.ROADTAX))).getText());
		assertEquals(driver.findElement(By.xpath(homePageContainer.reminderDate(Constants.ROADTAX))).getText(),
				"Due " + reminderDate);
		extenVerificationObj.extentVerification("Reminder Date Validation:",
				driver.findElement(By.xpath(homePageContainer.reminderDate(Constants.ROADTAX))).getText(),
				"Due " + reminderDate, "");

		return differenInDates;

	}

	public void reminderInfoValidation(String dateType) {
		ActionHandler.wait(2);
		extentLogger.log(Status.INFO, "Validating Info Bar of date : " + dateType);
		driver.findElement(By.xpath(manageVehiclePageContainer.info(dateType))).click();
		ActionHandler.wait(2);
		LOGGER.info("Pop Up Content: " + ActionHandler.getElementText(manageVehiclePageContainer.popupContent));
		LOGGER.info("Pop Up Header: " + ActionHandler.getElementText(manageVehiclePageContainer.popupHeader));
		assertFalse(ActionHandler.getElementText(manageVehiclePageContainer.popupContent).isEmpty());
		assertFalse(ActionHandler.getElementText(manageVehiclePageContainer.popupHeader).isEmpty());
		ActionHandler.wait(2);
		ActionHandler.click(manageVehiclePageContainer.popupClose);
	}

	public void getDirections() throws Exception {
		extenVerificationObj.extentInfo("Clicked on Get Directions!!");
		String oldTab = driver.getWindowHandle();
		List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(newTab.get(1));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//A[@class='dirBtnGo commonButton'][text()='Go']")));
		LOGGER.info("URL: " + driver.getCurrentUrl());
		assertTrue(driver.getCurrentUrl().contains("https://www.bing.com/maps"));
		extenVerificationObj.extentVerification("Bing Map in a new tab has been opened!!: ",
				driver.getCurrentUrl().contains("https://www.bing.com/maps"), true, "");

		ActionHandler.wait(4);
		driver.close();
		// Switching to Parent window
		driver.switchTo().window(oldTab);
		

	}

}
