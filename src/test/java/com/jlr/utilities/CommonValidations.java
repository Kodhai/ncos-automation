package com.jlr.utilities;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.jlr.base.TestBase;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.contants.Constants;

public class CommonValidations extends TestBase {

	public WebDriver driver = null;
	private static FinanceContainer financeContainer = null;
	private static HomePageContainer homePageContainer = null;
	public ExtentTest extentLogger = null;
	private static ExtenVerification extenVerificationObj = null;

	public CommonValidations(WebDriver commanDriver, ExtentTest commanExtent) {
		driver = commanDriver;
		extentLogger = commanExtent;
		extenVerificationObj = new ExtenVerification(commanExtent);
	}

	public void init() {
		financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);
	}

	public void financeQuoteValidations(String termValue, String annualMileageValue, String onTheRoadPrice,
			String totalDeposit, String paymentOption) {
		init();
		// Quote Validations
		try {
			assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("Duration Of Agreement"))).getText()
					.replaceAll("[^\\d.]", ""), termValue);

			extenVerificationObj.extentVerification("Verify Duration Of Agreement ",
					driver.findElement(By.xpath(financeContainer.getQuote("Duration Of Agreement"))).getText()
							.replaceAll("[^\\d.]", ""),
					termValue, "");

		/*	assertEquals(driver.findElement(By.xpath(financeContainer.getQuote("On The Road Price"))).getText(),
					onTheRoadPrice);

			extenVerificationObj.extentVerification("Verify On The Road Price",
					driver.findElement(By.xpath(financeContainer.getQuote("On The Road Price"))).getText(),
					onTheRoadPrice, "");*/

			// Validating finance quote has some number
			String perMonthValue = homePageContainer.perMonthInfoBarValue.getText().replaceAll("£", "").replace(",",
					"");
			try {
				extentLogger.log(Status.INFO,
						Double.parseDouble(perMonthValue) + " is a valid number under Finance Quote Info Bar");
			} catch (NumberFormatException e) {
				extentLogger.log(Status.INFO, perMonthValue + " is not a valid number under Finance Quote Info Bar");
			}
		} catch (Exception e) {
			extentLogger.error(e);
		}

	}
	
	public void financeQuoteValidations_R2(String termValue, String onTheRoadPrice,
			String totalDeposit, String paymentOption) {
		init();
		// Quote Validations
		try {
			assertEquals(driver.findElement(By.xpath(financeContainer.getQuoteR2("Duration Of Agreement"))).getText()
					.replaceAll("[^\\d.]", ""), termValue);

			extenVerificationObj.extentVerification("Verify Duration Of Agreement ",
					driver.findElement(By.xpath(financeContainer.getQuoteR2("Duration Of Agreement"))).getText()
							.replaceAll("[^\\d.]", ""),
					termValue, "");


			// Validating finance quote has some number
			String perMonthValue = homePageContainer.perMonthInfoBarValue.getText().replaceAll("£", "").replace(",",
					"");
			try {
				extentLogger.log(Status.INFO,
						Double.parseDouble(perMonthValue) + " is a valid number under Finance Quote Info Bar");
			} catch (NumberFormatException e) {
				extentLogger.log(Status.INFO, perMonthValue + " is not a valid number under Finance Quote Info Bar");
			}
		} catch (Exception e) {
			extentLogger.error(e);
		}

	}

	public void partExchangeValidations() {
		init();
		// Validating settlement figure
		String financeAgreement = financeContainer.financeAgreementFigure.getText().replaceAll("[^\\d.]", "");
		String estimatedValue = financeContainer.estimatedValue.getText().replaceAll("[^\\d.]", "");
		assertEquals(financeAgreement, Constants.SETTLEMENTFIGURE + ".00");

		Double exchangeTotalValue = Double.valueOf(financeContainer.exchangeEquityTotalValue.getText().replaceAll("[^\\d.]", ""));
		Double expectedTotalValue = Double.valueOf(estimatedValue) - Double.valueOf(financeAgreement);
		extentLogger.log(Status.INFO, "Validating Exchange Equity Value: Expected Value : "
				+ expectedTotalValue.toString() + " Actual Value:" + exchangeTotalValue);
		assertEquals(exchangeTotalValue,expectedTotalValue);
	}

}
