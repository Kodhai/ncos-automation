package com.jlr.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;

public class ExtenVerification extends TestBase {
	public ExtentTest extentLogger = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtenVerification.class.getName());	

	public ExtenVerification(ExtentTest extentLoggers) {
		extentLogger=extentLoggers;
	}

	public void extentVerification(String msg, String expected, String actual, String printOnlyFailLog)
			throws Exception {

		if ("Yes".equalsIgnoreCase(Config.getPropertyValue("detailReport"))) {
			printOnlyFailLog = "No";
		}

		if (!expected.equalsIgnoreCase(actual)) {
			/*
			 * extentLogger.fail(MarkupHelper.createLabel(msg + " - Expected : "
			 * + expected + " and Actual : " + actual + " @ScreenShot :: " +
			 * extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()),
			 * ExtentColor.RED));
			 */

			extentLogger.fail(msg + " - Expected : " + expected + " and Actual : " + actual,
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());

		} else if (!"Yes".equalsIgnoreCase(printOnlyFailLog)) {
			extentLogger.pass(MarkupHelper.createLabel(msg + " - Expected : " + expected + " and Actual : " + actual,
					ExtentColor.GREEN));
		}
	}

	public void extentVerification(String msg, Boolean expected, Boolean actual, String printOnlyFailLog)
			throws Exception {
		if ("Yes".equalsIgnoreCase(Config.getPropertyValue("detailReport"))) {
			printOnlyFailLog = "No";
		}

		if (!expected == actual) {
			/*
			 * extentLogger.fail(MarkupHelper.createLabel(msg + " - Expected : "
			 * + expected + " and Actual : " + actual + " @ScreenShot :: " +
			 * extentLogger.addScreenCaptureFromPath(Utility.takeScreenShot()),
			 * ExtentColor.RED));
			 */
			extentLogger.fail(msg + " - Expected : " + expected + " and Actual : " + actual,
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());

		} else if (!"Yes".equalsIgnoreCase(printOnlyFailLog)) {
			extentLogger.pass(MarkupHelper.createLabel(msg + " - Expected : " + expected + " and Actual : " + actual,
					ExtentColor.GREEN));
		}
	}

	public void extentInfo(String info) {
		extentLogger.info(info);
		LOGGER.info(info);
	}

	public void extentError(String Error) throws Exception {
		extentLogger.error(Error, MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
		LOGGER.error(Error);
	}

}
