
package com.jlr.utilities;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.AssertHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBase;
import com.jlr.containers.FinanceContainer;
import com.jlr.containers.HomePageContainer;
import com.jlr.containers.LoginContainer;
import com.jlr.containers.Magento_AdminContainer;
import com.jlr.containers.PersonalDetailsContainer;
import com.jlr.containers.PersonalDetailsContainer_R2;
import com.jlr.containers.PersonaliseBrochureContainer;
import com.jlr.containers.SVCRM4Container;
import com.jlr.containers.SummaryContainer;
import com.jlr.contants.Constants;

public class CommonFunctionsNcos extends TestBase {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunctionsNcos.class.getName());
	private static LoginContainer loginContainer = null;
	private static HomePageContainer homePageContainer = null;
	private static FinanceContainer financeContainer = null;
	private static PersonalDetailsContainer personalDetailsContainer = null;
	private static PersonaliseBrochureContainer personaliseBrochureContainer = null;
	private static SummaryContainer summaryContainer = null;
	private static PersonalDetailsContainer_R2 personalDetailsContainer_R2 = null;
	private static ExtenVerification extenVerificationObj = null;
	private static SVCRM4Container svcrm4Container = null;
	private static Magento_AdminContainer Magento_AdminContainer = null;
	public WebDriver driver = null;
	public ExtentTest extentLogger = null;
	public String LoginPortal = null;

	public CommonFunctionsNcos(WebDriver commanDriver, ExtentTest commanExtent, String portal) {
		
		driver = commanDriver;
		extentLogger = commanExtent;
		extenVerificationObj = new ExtenVerification(commanExtent);
		LoginPortal = portal;
	}

	public CommonFunctionsNcos(WebDriver commanDriver, ExtentTest commanExtent) {
		driver = commanDriver;
		extentLogger = commanExtent;
		extenVerificationObj = new ExtenVerification(commanExtent);
	}

	public void pdfValidation(String brand, String models, String engine, String specif, String nextStep,
			String exterior, String colours, String visualPacks, String interior, String headlining, String options,
			String infotainment, String jaguarGear, String extAccessories) {
		try {
			summaryContainer = PageFactory.initElements(driver, SummaryContainer.class);

			// Personalised Brochure
			extenVerificationObj.extentInfo("Click on Personalised Brochure");
			ActionHandler.click(summaryContainer.personalisedBrochure);
			ActionHandler.wait(3);

			String oldTab = personliseYourBrochure(brand);
			pdfRetrival(models, engine, specif, nextStep, exterior, colours, visualPacks, interior, headlining, options,
					infotainment, jaguarGear, extAccessories);
			driver.close();
			driver.switchTo().window(oldTab);

		} catch (Exception e) {
			LOGGER.error("Exception: " + e);
		}
	}

	public void pdfRetrival(String models, String engine, String specif, String nextStep, String exterior,
			String colours, String visualPacks, String interior, String headlining, String options, String infotainment,
			String jaguarGear, String extAccessories) {
		try {
			ActionHandler.wait(2);
			// ******************************************

			Robot robot = new Robot();
			// press Ctrl+S the Robot's way
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_S);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_S);
			// press Enter
			ActionHandler.wait(2);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			// ******************************************
			ActionHandler.wait(4);
			PDFTextStripper pdfStripper = null;
			String parsedText = null;

			try {
				String home = System.getProperty("user.home");
				File file = new File(home + "/Downloads/" + "landrover.pdf");
				PDDocument doc = PDDocument.load(file);
				pdfStripper = new PDFTextStripper();
				pdfStripper.setStartPage(1);
				// pdfStripper.setEndPage(2);
				parsedText = pdfStripper.getText(doc);
			} catch (MalformedURLException e2) {
				LOGGER.error("URL string could not be parsed " + e2.getMessage());
			} catch (IOException e) {
				LOGGER.error("Unable to open PDF Parser. " + e.getMessage());
			}

			LOGGER.info("+++++++++++++++++");
			LOGGER.info(parsedText);
			LOGGER.info("+++++++++++++++++");
			extenVerificationObj.extentInfo("Validating PDF Data");
			extenVerificationObj.extentVerification("Validation Of Model : " + models, true,
					parsedText.contains(models.substring(2)), "No");
			if (("Yes").equals(interior)) {
				extenVerificationObj.extentVerification("Validation Of Interior : " + headlining, true,
						parsedText.contains(headlining), "No");
			}
			if (("Yes").equals(exterior)) {
				extenVerificationObj.extentVerification("Validation Of Exterior 1 : " + colours, true,
						parsedText.contains(colours), "Yes");
				extenVerificationObj.extentVerification("Validation Of Exterior 2 : " + visualPacks, true,
						parsedText.contains(visualPacks), "No");
			}

			if (("Yes").equals(options)) {
				extenVerificationObj.extentVerification("Validation Of Options:", true,
						parsedText.contains(infotainment), "No");
			}
			if (("Yes").equals(jaguarGear)) {
				extenVerificationObj.extentVerification("Validation Of Jaguar Gear : " + extAccessories, true,
						parsedText.contains(extAccessories), "No");
			}

		} catch (Exception e) {
			LOGGER.error("Exception Occurred:" + e.getStackTrace());
		}
	}

	public void userLogin() {
		String url = null;
		// ActionHandler.clearBrowserCache();
		loginContainer = PageFactory.initElements(driver, LoginContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		svcrm4Container = PageFactory.initElements(driver, SVCRM4Container.class);
		try {
			if ((Constants.LANDROVER).equals(LoginPortal)) {
				url = Config.getPropertyValue("application.url.landRover");
			} else if ((Constants.JAGUARIPACE).equals(LoginPortal)) {
				url = Config.getPropertyValue("application.url.ipace");
			} else if ((Constants.JAGUAREPACE).equals(LoginPortal)) {
				url = Config.getPropertyValue("application.url.epace");
			} else if ((Constants.ENQUIRYMAX).equals(LoginPortal)) {
				url = Config.getPropertyValue("enquiryMax.url");
			}
			LOGGER.info("URL : " + url);
			driver.get(url);
		} catch (IOException e) {
			LOGGER.error("application url not found. Reverting to default url: " + Constants.DEFAULTURLNCOS);
			driver.get(Constants.DEFAULTURLNCOS);
		}

		userEntersUsername();
		userEntersPassword();
		userClicksOnLoginButton();
		AssertHandler.assertTextEqualsOnElement(homePageContainer.modelText, Constants.MODELHEADING);
		LOGGER.info("User has successfully logged in");

	}

	public void userLogin(String url) {
		loginContainer = PageFactory.initElements(driver, LoginContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		svcrm4Container = PageFactory.initElements(driver, SVCRM4Container.class);
		LOGGER.info("URL : " + url);
		driver.get(url);

		userEntersUsername();
		userEntersPassword();
		userClicksOnLoginButton();
		/*
		 * if(VerifyHandler.verifyElementPresent(homePageContainer.
		 * bodyStylesLink)) {
		 * AssertHandler.assertTextEqualsOnElement(homePageContainer.
		 * bodyStylesLink, Constants.MODELHEADING_BODYSTYLES); } else {
		 * AssertHandler.assertTextEqualsOnElement(homePageContainer.modelText,
		 * Constants.MODELHEADING);
		 * 
		 * }
		 */
		LOGGER.info("User has successfully logged in");
		
	}

	public void userEntersUsername() {
		String email = null;
		try {
			email = Config.getPropertyValue("valid.email");
			assertTrue(StringUtils.isNotBlank(email));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(email));
		ActionHandler.setText(loginContainer.username, email);

	}

	public void userEntersPassword() {

		String password = null;
		try {
			password = Config.getPropertyValue("valid.password");
			assertTrue(StringUtils.isNotBlank(password));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching email.", e);
		}
		// Utility.decodeString(password));
		ActionHandler.setText(loginContainer.password, password);
		LOGGER.info("Entered password");
	}

	public void userClicksOnLoginButton() {
		ActionHandler.click(loginContainer.loginButton);
		LOGGER.info("Clicked final sign in button");
		ActionHandler.wait(2);
	}

	public void userLogsOut() throws Exception {
		extentLogger.info("Before logOut from Portal ",
				MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
		ActionHandler.click(loginContainer.logoutButton);
		// ActionHandler.click(loginContainer.goodByeButton);
		// ActionHandler.wait(3);
		LOGGER.info("User has successfully logged out");
	}

	public void verifyFinanceQuoteDetails(String paymentOpt, String advRetail, String defaultAr, String term,
			String defaultTerm, String annualMileageRange, String defaultMileage, String deposit, String defaultDeposit,
			WebDriver driver) throws Exception {

		financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		homePageContainer = PageFactory.initElements(driver, HomePageContainer.class);

		List<WebElement> maximumRangeList = new ArrayList<>();
		List<WebElement> minimumRangeList = new ArrayList<>();
		List<WebElement> defaultValueList = new ArrayList<>();

		maximumRangeList = financeContainer.maximumRangeList;
		minimumRangeList = financeContainer.minimumRangeList;
		defaultValueList = financeContainer.defaultValueList;

		// Validate Total Deposit
		validateMaximumMinmumRange(advRetail, maximumRangeList, minimumRangeList, 0);

		validateDefaultValue(defaultAr, defaultValueList, 0);

		// Validate Term
		validateMaximumMinmumRange(term, maximumRangeList, minimumRangeList, 1);

		// Validate Default Term
		validateDefaultValue(defaultTerm, defaultValueList, 1);

		if (!"HP".equals(paymentOpt)) {
			// Validate Mileage
			validateMaximumMinmumRange(annualMileageRange, maximumRangeList, minimumRangeList, 2);

			// Validate Default mileage
			validateDefaultValue(defaultMileage, defaultValueList, 2);
		}

		// Validate percent range - deposit
		Double onTheRoadPrice = Double.valueOf(homePageContainer.onTheRoadPrice.getText().replaceAll("[^\\d.]", ""));
		// validatePercentRange(onTheRoadPrice, deposit, maximumRangeList,
		// minimumRangeList, 0); SA

		// Validate Default percent value
		// validatePercentDefaultValue(onTheRoadPrice, defaultDeposit); SA

	}

	private void validatePercentDefaultValue(Double onTheRoadPrice, String defaultDeposit) throws Exception {

		if (!("NA").equalsIgnoreCase(defaultDeposit)) {
			String defaultUiValue = financeContainer.totalDepositDefault.getAttribute("value").replaceAll("[^\\d.]",
					"");
			LOGGER.info("### Verfying Data Of :" + defaultDeposit + " ###");
			// Regex to remove trailing zeros and decimal
			String maxRange = String.valueOf(Math.ceil((onTheRoadPrice * Double.valueOf(defaultDeposit)) / 100))
					.replaceAll("(?<=^\\d+)\\.0*$", "");
			LOGGER.info("Actual Value: " + maxRange + " Expected Value:" + defaultUiValue);
			extenVerificationObj.extentVerification("Validation Of Default Value: ", maxRange, defaultUiValue, "Yes");

		}

	}

	private void validatePercentRange(Double onTheRoadPrice, String deposit, List<WebElement> maximumRangeList,
			List<WebElement> minimumRangeList, int index) throws Exception {
		if (!("NA").equalsIgnoreCase(deposit)) {
			LOGGER.info("### Verfying Data Of :" + deposit + " ###");
			String[] splitData = splitFunction(deposit);
			LOGGER.info("On The Road: " + onTheRoadPrice.intValue() + " UI Actual Value: " + deposit);
			// Regex to remove trailing zeros and decimal
			String maxRange = String.valueOf(Math.ceil((onTheRoadPrice * Double.valueOf(splitData[1])) / 100))
					.replaceAll("(?<=^\\d+)\\.0*$", "");

			String actualMinRange = minimumRangeList.get(index).getText().replaceAll("[^\\d.]", "");
			String actualMaxRange = maximumRangeList.get(index).getText().replaceAll("[^\\d.]", "");

			LOGGER.info("Minimum Actual Value:" + actualMinRange + " Minimum Expected Value: " + splitData[0]);
			extenVerificationObj.extentVerification("Validation Of Minimum Value: ", actualMinRange, splitData[0],
					"Yes");
			LOGGER.info("Maximum Actual Value:" + actualMaxRange + " Maximum Expected Value: " + maxRange);
			extenVerificationObj.extentVerification("Validation Of Maximum Value: ", maxRange, actualMaxRange, "Yes");

		}

	}

	private void validateMaximumMinmumRange(String advRetail, List<WebElement> maximumRangeList,
			List<WebElement> minimumRangeList, int index) throws Exception {

		if (!("NA").equalsIgnoreCase(advRetail)) {
			LOGGER.info("### Verfying Data Of :" + advRetail + " ###");
			String[] splitData = splitFunction(advRetail);

			// Regex to remove special character and characters
			String actualMinRange = minimumRangeList.get(index).getText().replaceAll("[^\\d.]", "");
			String actualMaxRange = maximumRangeList.get(index).getText().replaceAll("[^\\d.]", "");

			LOGGER.info("Minimum Actual Value:" + actualMinRange + " Minimum Expected Value: " + splitData[0]);
			extenVerificationObj.extentVerification("Maximum Actual Value:", splitData[0], actualMinRange, "Yes");

			LOGGER.info("Maximum Actual Value:" + actualMaxRange + " Maximum Expected Value: " + splitData[1]);
			extenVerificationObj.extentVerification("Maximum Actual Value:", splitData[1], actualMaxRange, "Yes");

		}

	}

	private void validateDefaultValue(String defaultAr, List<WebElement> defaultValueList, int index) throws Exception {

		if (!("NA").equalsIgnoreCase(defaultAr)) {
			LOGGER.info("### Verfying Data Of :" + defaultAr + " ###");
			LOGGER.info("Actual Value: " + defaultValueList.get(index).getText().replaceAll("[^\\d.]", "")
					+ " Expected Value:" + defaultAr);
			extenVerificationObj.extentVerification("Validate Default Value:",
					defaultValueList.get(index).getText().replaceAll("[^\\d.]", ""), defaultAr, "Yes");

		}

	}

	public String[] splitFunction(String dataTobeSplit) {
		String splitData[] = dataTobeSplit.split("-");
		LOGGER.info("Min Value:" + splitData[0] + " Max Value:" + splitData[1]);
		return splitData;
	}

	// User avails part exchange
	public void partExchange(String pxRegistration, String condition, String mileage) {
		financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		ActionHandler.pageUp();
		ActionHandler.click(financeContainer.PartExchangeYes);
		String REGISTRATIONNUMBER = null;
		String VEHICLENAME = null;
		String DOORS = null;
		String ENGINE = null;
		String FUELTYPE = null;
		String TRANSMISSION = null;
		String REGDATE = null;

	/*	if (StringUtils.isNotBlank(pxRegistration)) {
			if ("BJ67UMA".equals(pxRegistration)) {
				REGISTRATIONNUMBER = pxRegistration;
				VEHICLENAME = Constants.VEHICLENAME_BJ67UMA;
				DOORS = Constants.DOORS_BJ67UMA;
				ENGINE = Constants.ENGINE_BJ67UMA;
				FUELTYPE = Constants.FUELTYPE_BJ67UMA;
				TRANSMISSION = Constants.TRANSMISSION_BJ67UMA;
				REGDATE = Constants.REGDATE_BJ67UMA;
			} else {
				REGISTRATIONNUMBER = pxRegistration;
				VEHICLENAME = Constants.VEHICLENAME_BJ67UMO;
				DOORS = Constants.DOORS_BJ67UMO;
				ENGINE = Constants.ENGINE_BJ67UMO;
				FUELTYPE = Constants.FUELTYPE_BJ67UMO;
				TRANSMISSION = Constants.TRANSMISSION_BJ67UMO;
				REGDATE = Constants.REGDATE_BJ67UMO;
			}
		} else {
			REGISTRATIONNUMBER = Constants.REGISTRATIONNUMBER1;
			VEHICLENAME = Constants.VEHICLENAME;
			DOORS = Constants.DOORS;
			ENGINE = Constants.ENGINE;
			FUELTYPE = Constants.FUELTYPE;
			TRANSMISSION = Constants.TRANSMISSION;
			REGDATE = Constants.REGDATE;
		} */
		ActionHandler.setText(financeContainer.registrationNumber, pxRegistration);
		ActionHandler.click(financeContainer.submitButton);
		ActionHandler.wait(5);
		ActionHandler.click(financeContainer.continueVehicle);		
		List<WebElement> vehicleDetails = financeContainer.exchangeVehicleDetails;

		extentLogger.log(Status.INFO, "Vehicle Details Of Registration Number: " + pxRegistration);
	/*	assertEquals(vehicleDetails.get(0).getText(), VEHICLENAME);
		assertEquals(vehicleDetails.get(1).getText(), DOORS);
		assertEquals(vehicleDetails.get(2).getText(), ENGINE);
		assertEquals(vehicleDetails.get(3).getText(), FUELTYPE);
		assertEquals(vehicleDetails.get(4).getText(), TRANSMISSION);
		assertEquals(vehicleDetails.get(5).getText(), REGDATE); */

		extentLogger.log(Status.INFO, "Entering Mileage: " + mileage);
		ActionHandler.setText(financeContainer.mileageTextBox, mileage);
		if (StringUtils.isNotBlank(condition)) {
			extentLogger.log(Status.INFO, "Vehicle Condition: " + financeContainer.Excellent.getAttribute("value"));
			ActionHandler.click(financeContainer.Excellent);
		} else {
			extentLogger.log(Status.INFO, "Vehicle Condition: " + financeContainer.goodCondition.getAttribute("value"));
			ActionHandler.click(financeContainer.goodCondition);
		}

		ActionHandler.click(financeContainer.settlementFigureYes);
		extentLogger.log(Status.INFO, "Settlement Figure: " + Constants.SETTLEMENTFIGURE);
		ActionHandler.setText(financeContainer.outstandingFinanceAmount, Constants.SETTLEMENTFIGURE);
		ActionHandler.click(financeContainer.acceptTermsAndConditions);
		ActionHandler.click(financeContainer.getyourvaluation);
		ActionHandler.wait(3);
	}

	// User avails part exchange
	public void partExchange_R1(String mileage) {
		financeContainer = PageFactory.initElements(driver, FinanceContainer.class);
		ActionHandler.click(financeContainer.PartExchangeYes);
		ActionHandler.setText(financeContainer.registrationNumber, Constants.REGISTRATIONNUMBER1);
		ActionHandler.click(financeContainer.getyourvaluation);
		ActionHandler.wait(5);
		List<WebElement> vehicleDetails = financeContainer.exchangeVehicleDetails;

		extentLogger.log(Status.INFO, "Vehicle Details Of Registration Number: " + Constants.REGISTRATIONNUMBER1);
		LOGGER.info(vehicleDetails.get(0).getText());
		assertEquals(vehicleDetails.get(0).getText(), Constants.VEHICLENAMER1);
		assertEquals(vehicleDetails.get(1).getText(), Constants.DOORSR1);
		assertEquals(vehicleDetails.get(2).getText(), Constants.ENGINER1);
		assertEquals(vehicleDetails.get(3).getText(), Constants.FUELTYPER1);
		assertEquals(vehicleDetails.get(4).getText(), Constants.TRANSMISSIONR1);
		assertEquals(vehicleDetails.get(5).getText(), Constants.REGDATER1);

		ActionHandler.click(financeContainer.continueVehicle);
		extentLogger.log(Status.INFO, "Entering Mileage: " + mileage);
		ActionHandler.setText(financeContainer.mileageTextBox, mileage);
		extentLogger.log(Status.INFO, "Vehicle Condition: " + financeContainer.goodCondition.getAttribute("value"));
		ActionHandler.click(financeContainer.goodCondition);
		ActionHandler.click(financeContainer.settlementFigureYes);
		extentLogger.log(Status.INFO, "Settlement Figure: " + Constants.SETTLEMENTFIGURE);
		ActionHandler.setText(financeContainer.outstandingFinanceAmount, Constants.SETTLEMENTFIGURE);
		ActionHandler.click(financeContainer.acceptTermsAndConditions);
		ActionHandler.click(financeContainer.getyourvaluation);
		ActionHandler.wait(3);
	}

	public void validateMonthlyValues(List<WebElement> webElementList, String category) {
		extentLogger.log(Status.INFO, "Validation of p/m on " + category);
		for (WebElement webElement : webElementList) {
			assertTrue(webElement.getText().contains("p/m"), "Monthly Values does not exist!!!");
		}

	}

	public String personliseYourBrochure(String jaguarLandRover) {
		String oldTab = driver.getWindowHandle();
		try {
			extentLogger.info("Before navigating to Personalised Brochure page ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			personaliseBrochureContainer = PageFactory.initElements(driver, PersonaliseBrochureContainer.class);
			List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(newTab.get(1));
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//H3[text()='Personal Details']")));

			ActionHandler.setText(personaliseBrochureContainer.pdEmailTxt, Config.getPropertyValue("email"));
			ActionHandler.setText(personaliseBrochureContainer.firstName, Config.getPropertyValue("firstName"));
			ActionHandler.setText(personaliseBrochureContainer.lastName, Config.getPropertyValue("lastName"));
			ActionHandler.click(personaliseBrochureContainer.retailerPostCode);
			ActionHandler.scrollDown();
			ActionHandler.setText(personaliseBrochureContainer.retailerPostCode, Config.getPropertyValue("retailer"));
			ActionHandler.wait(3);
			ActionHandler.scrollDown();
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			ActionHandler.click(driver.findElement(By.xpath(personaliseBrochureContainer.contactedByRetailer("No"))));

			if (!"".equals(Config.getPropertyValue("emailchk"))) {
				ActionHandler.click(personaliseBrochureContainer.emailChkBox);
			}
			if (!"".equals(Config.getPropertyValue("telephonechk"))) {
				ActionHandler.click(personaliseBrochureContainer.telephoneChkBox);
			}
			if (!"".equals(Config.getPropertyValue("post"))) {
				ActionHandler.click(personaliseBrochureContainer.postChkBox);
			}
			if (!"".equals(Config.getPropertyValue("sms"))) {
				ActionHandler.click(personaliseBrochureContainer.smsChkBox);
			}

			ActionHandler.click(personaliseBrochureContainer.submitBtn);
			ActionHandler.wait(5);

			extenVerificationObj.extentInfo("Submiting Personalised Brochure - Personal Details form");

			// Validating Success Message
			if (Constants.LANDROVER.equals(jaguarLandRover)) {
				assertEquals(personaliseBrochureContainer.thankYouMsg.getText(), Constants.LANDROVERBROCHURESUCCESSMSG);
			} else {
				assertEquals(personaliseBrochureContainer.thankYouMsg.getText(), Constants.JAGUARBROCHURESUCCESSMSG);
			}
			extenVerificationObj.extentInfo("Validation Of Success Message Successful");
			extentLogger.info("Personalised Brochure - Personal Details form Submitted successfully...!!!",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());

			LOGGER.info("Deleting Existing Jaguar PDF File From Downloads");
			Files.deleteIfExists(Paths.get(System.getProperty("user.home") + "/Downloads/jaguar.pdf"));

			extentLogger.info("Downloading Brochure");
			ActionHandler.click(personaliseBrochureContainer.downloadBrochure);
			ActionHandler.wait(5);

		} catch (Exception e) {

			LOGGER.error("Exception Occurred", e);
		}

		return oldTab;

	}

	public String completePersonalDetails(String jaguarLandRover) {

		String oldTab = driver.getWindowHandle();
		try {
			extentLogger.info("Before navigating to personal detail page ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			personalDetailsContainer = PageFactory.initElements(driver, PersonalDetailsContainer.class);
			List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(newTab.get(1));
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//H3[text()='Personal Details']")));

			ActionHandler.setText(personalDetailsContainer.pdEmailTxt, Config.getPropertyValue("email"));
			if ("Business Use".equals(Config.getPropertyValue("use"))) {
				ActionHandler.click(personalDetailsContainer.buisnessUseButton);

				if ("Company Car Driver".equals(Config.getPropertyValue("buisnessUseOption"))) {
					ActionHandler.click(personalDetailsContainer.buisnessUseCar_DriverButton);
				} else if ("Small Business".equals(Config.getPropertyValue("buisnessUseOption"))) {
					ActionHandler.click(personalDetailsContainer.buisnessUseSmallBusButton);
				} else {
					ActionHandler.click(personalDetailsContainer.buisnessUseFleetButton);
				}
			}

			ActionHandler.click(personalDetailsContainer.titleDropButton);
			ActionHandler.wait(2);
			ActionHandler.click(personalDetailsContainer.titleDrop);

			ActionHandler.setText(personalDetailsContainer.firstNameTxt, Config.getPropertyValue("firstName"));
			ActionHandler.setText(personalDetailsContainer.lastNameTxt, Config.getPropertyValue("lastName"));
			ActionHandler.setText(personalDetailsContainer.mobileTxt, Config.getPropertyValue("mobileNoR1"));
			ActionHandler.setText(personalDetailsContainer.addrTxt, Config.getPropertyValue("address"));
			ActionHandler.wait(3);
			ActionHandler.scrollDown();
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			ActionHandler.setText(personalDetailsContainer.dateTimeCal, Config.getPropertyValue("reqDate"));
			ActionHandler.click(personalDetailsContainer.postTxt);
			ActionHandler.setText(personalDetailsContainer.postTxt, Config.getPropertyValue("retailer"));
			ActionHandler.wait(3);
			ActionHandler.scrollDown();
			ActionHandler.pressEnter();
			ActionHandler.wait(2);

			if (!"".equals(Config.getPropertyValue("emailchk"))) {
				ActionHandler.click(personalDetailsContainer.emailChkBox);
			}
			if (!"".equals(Config.getPropertyValue("telephonechk"))) {
				ActionHandler.click(personalDetailsContainer.telephoneChkBox);
			}
			if (!"".equals(Config.getPropertyValue("post"))) {
				ActionHandler.click(personalDetailsContainer.postChkBox);
			}
			if (!"".equals(Config.getPropertyValue("sms"))) {
				ActionHandler.click(personalDetailsContainer.smsChkBox);
			}

			ActionHandler.click(personalDetailsContainer.submitBtn);
			ActionHandler.wait(3);

			extenVerificationObj.extentInfo("Submiting Personal Details form");
			// Validating Success Message
			assertEquals(personalDetailsContainer.successMsg.get(0).getText(), Constants.THANKYOUMSG);
			if (Constants.LANDROVER.equals(jaguarLandRover)) {
				assertEquals(personalDetailsContainer.successMsg.get(1).getText(), Constants.LANDROVERSUCCESSMSG);
			} else {
				assertEquals(personalDetailsContainer.successMsg.get(1).getText(), Constants.JAGUARSUCCESSMSG);
			}
			extentLogger.info("Personal Details form Submitted successfully...!!!",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			ActionHandler.wait(5);

		} catch (Exception e) {

			LOGGER.error("Exception Occurred", e);
		}

		return oldTab;
	}

	public String completePersonalDetails_R2(String jaguarLandRover) {

		String oldTab = driver.getWindowHandle();
		try {

			extentLogger.info("Before navigating to personal detail page ",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			personalDetailsContainer_R2 = PageFactory.initElements(driver, PersonalDetailsContainer_R2.class);
			List<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(newTab.get(1));
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//H1[text()='Personal Information']")));

			ActionHandler.setText(personalDetailsContainer_R2.pdEmailTxt, Config.getPropertyValue("email"));
			if ("Business Use".equals(Config.getPropertyValue("use"))) {
				ActionHandler.click(personalDetailsContainer.buisnessUseButton);

				if ("Company Car Driver".equals(Config.getPropertyValue("buisnessUseOption"))) {
					ActionHandler.click(personalDetailsContainer_R2.buisnessUseCar_DriverButton);
				} else if ("Small Business".equals(Config.getPropertyValue("buisnessUseOption"))) {
					ActionHandler.click(personalDetailsContainer_R2.buisnessUseSmallBusButton);
				} else {
					ActionHandler.click(personalDetailsContainer_R2.buisnessUseFleetButton);
				}
			}

			ActionHandler.click(personalDetailsContainer_R2.titleDropButton);
			ActionHandler.wait(3);
			ActionHandler.click(personalDetailsContainer_R2.titleDrop);

			ActionHandler.setText(personalDetailsContainer_R2.firstNameTxt, Config.getPropertyValue("firstName"));
			ActionHandler.setText(personalDetailsContainer_R2.lastNameTxt, Config.getPropertyValue("lastName"));
			ActionHandler.setText(personalDetailsContainer_R2.mobileTxt, Config.getPropertyValue("mobileNo"));
			ActionHandler.setText(personalDetailsContainer_R2.addrTxt, Config.getPropertyValue("address"));
			ActionHandler.wait(3);
			ActionHandler.scrollDown();
			ActionHandler.pressEnter();
			ActionHandler.wait(2);
			ActionHandler.setText(personalDetailsContainer_R2.dateTimeCal, Config.getPropertyValue("reqDate"));
			ActionHandler.click(personalDetailsContainer_R2.postTxt);
			ActionHandler.setText(personalDetailsContainer_R2.postTxt, Config.getPropertyValue("retailer"));
			ActionHandler.wait(3);
			ActionHandler.scrollDown();
			ActionHandler.pressEnter();
			ActionHandler.wait(2);

			if (!"".equals(Config.getPropertyValue("emailchk"))) {
				ActionHandler.click(personalDetailsContainer_R2.emailChkBox);
			}
			if (!"".equals(Config.getPropertyValue("telephonechk"))) {
				ActionHandler.click(personalDetailsContainer_R2.telephoneChkBox);
			}
			if (!"".equals(Config.getPropertyValue("post"))) {
				ActionHandler.click(personalDetailsContainer_R2.postChkBox);
			}
			if (!"".equals(Config.getPropertyValue("sms"))) {
				ActionHandler.click(personalDetailsContainer_R2.smsChkBox);
			}

			ActionHandler.click(personalDetailsContainer_R2.submitBtn);
			ActionHandler.wait(3);

			extenVerificationObj.extentInfo("Submiting Personal Details form");
			// Validating Success Message
			assertEquals(personalDetailsContainer_R2.successMsg.get(0).getText(), Constants.THANKYOUMSG);
			if (Constants.LANDROVER.equals(jaguarLandRover)) {
				assertEquals(personalDetailsContainer_R2.successMsg2.get(0).getText(), Constants.LANDROVERSUCCESSMSG);
			} else {
				assertEquals(personalDetailsContainer_R2.successMsg2.get(0).getText(), Constants.JAGUARSUCCESSMSG);
			}
			extentLogger.info("Personal Details form Submitted successfully...!!!",
					MediaEntityBuilder.createScreenCaptureFromPath(Utility.takeScreenShot()).build());
			ActionHandler.wait(5);

		} catch (Exception e) {

			LOGGER.error("Exception Occurred", e);
		}

		return oldTab;
	}

	public void homePage(String model, String engBlock, String packName, String nextStep, String paymentOpt,String portals) {
		
			try {
			  ActionHandler.wait(2);
			  extentLogger.log(Status.INFO, "Selecting model : " + model);
			  ActionHandler.wait(10);
			  //if (VerifyHandler.verifyElementPresent(homePageContainer.bodyStylesLink)) {
			   //ActionHandler.click(homePageContainer.bodyStylesLink);
			   //ActionHandler.wait(15);
			  //}
			  ActionHandler.wait(3);
			  if (StringUtils.isNotBlank(model) && !model.equals("NA")) {
			   ActionHandler.click(driver.findElement(By.xpath(homePageContainer.modelBlock(model))));
			  }
			  // Engine

			  ActionHandler.wait(20);
			  LOGGER.info("Selecting  : " + engBlock);
			  extentLogger.log(Status.INFO, "Selecting engine : " + engBlock); 
			  ActionHandler.wait(3);
			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.engBlock(engBlock))));
			  ActionHandler.wait(10);
			  List<WebElement> engBlocks = homePageContainer.allEngBlocks;
			  for (WebElement engine : engBlocks) {
			   if (engBlock.equalsIgnoreCase(engine.getText())) {
			    engine.click();
			    ActionHandler.wait(20);
			   }
			  }
			  // Specification Pack

			  ActionHandler.wait(5);
			/*  if (!VerifyHandler.verifyElementPresent(homePageContainer.bodyStylesLink)) {
			 *   specificationPackValidation(portals, engBlock);

			  } */


			  System.out.println("before...........");
			  LOGGER.info("Selecting specification Pack : " + packName);
			  extentLogger.log(Status.INFO, "Selecting specification Pack : " + packName); 
			  String packValue = driver.findElement(By.xpath(homePageContainer.specificPackValue(packName))).getText();
			  System.out.println(packValue);
			  LOGGER.info("Pack Value:" + packValue);
			  System.out.println(packName);
			//  ActionHandler.click(driver.findElement(By.xpath("(//*[@class='toggle__icon'])[1]")));
			  if (("DEFENDER").equalsIgnoreCase(packName)) {
				  ActionHandler.click(driver.findElement(By.xpath("(//*[text()='DEFENDER'])[2]")));
			  }
			  if (("AUTOBIOGRAPHY DYNAMIC").equalsIgnoreCase(packName)) {
				  ActionHandler.click(driver.findElement(By.xpath("(//*[text()='AUTOBIOGRAPHY DYNAMIC'])[1]")));
			  }
			  else {
			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.specificPackValue(packName))));
			  }
			  // NextStep
			  // nextStep="CONTINUE BUILD TO ORDER";
			//  extentLogger.log(Status.INFO, "Selecting Option " + nextStep);

     		  ActionHandler.wait(15);
			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.nextStep(nextStep))));
			  ActionHandler.wait(10);
			  String headerSummaryVaue = ActionHandler.getElementText(homePageContainer.headerSummary);

	/*		  if (homePageContainer.vehicleType.getText().contains("ELECTRIC")) {


			   packValue = String.valueOf(Integer.parseInt(packValue.replaceAll("[^\\d.]", "").replaceAll(".00", "")) - 3500);

			   headerSummaryVaue = headerSummaryVaue.replaceAll("[^\\d.]", "").replaceAll(".00", "");

			  }
			  extenVerificationObj.extentVerification("Verifiying Header Summary Value", packValue, headerSummaryVaue,""); 


			   assertTrue(packValue.equals(headerSummaryVaue));

			  ActionHandler.wait(4); */


			 } catch (Exception e) {

			  extentLogger.error("Home Page : " + e);

			 }

			}

	public void specificationPackValidation(String portal, String engBlock) {
		int specfPackFeatures;
		try {

			if (portal.equals(Constants.LANDROVER)) {
				specfPackFeatures = Integer.parseInt(Config.getPropertyValue("allSpecificationPacksLandRover"));
				if (specfPackFeatures == homePageContainer.specfPackWheels.size()
						&& specfPackFeatures == homePageContainer.specfPackHEADLIGHTS.size()
						&& specfPackFeatures == homePageContainer.specfPackPOWERED.size()
						&& specfPackFeatures == homePageContainer.specfPackSEATING.size()
						&& specfPackFeatures == homePageContainer.specfPackINFOTAINMENT.size()
						&& specfPackFeatures == homePageContainer.specfPackAIDS.size()) {
					extenVerificationObj.extentVerification("All specification pack features are visible", true, true,
							"");
				} else {
					extenVerificationObj.extentVerification("All specification pack features are not visible", true,
							false, "");
				}
			}

			else if (portal.equals(Constants.JAGUARIPACE)) {
				specfPackFeatures = Integer.parseInt(Config.getPropertyValue("allSpecificationPacksJaguarIpace"));
				if (specfPackFeatures == homePageContainer.specfPackWheels.size()
						&& specfPackFeatures == homePageContainer.specfPackExteriorLighting.size()
						&& specfPackFeatures == homePageContainer.specfPackConvenience.size()
						&& specfPackFeatures == homePageContainer.specfPackSEATING.size()
						&& specfPackFeatures == homePageContainer.specfPackINFOTAINMENT.size()
						&& specfPackFeatures == homePageContainer.specfPackDriver.size()) {
					extenVerificationObj.extentVerification("All specification pack features are visible", true, true,
							"");
				} else {
					extenVerificationObj.extentVerification("All specification pack features are not visible", true,
							false, "");
				}
			}

			else if (portal.equals(Constants.JAGUAREPACE)) {
				if (engBlock.contains("MANUAL")) {
					specfPackFeatures = Integer
							.parseInt(Config.getPropertyValue("allSpecificationPacksJaguarEpaceManual"));
				} else {
					specfPackFeatures = Integer
							.parseInt(Config.getPropertyValue("allSpecificationPacksJaguarEpaceAutomatic"));
				}
				if (specfPackFeatures == homePageContainer.specfPackWheels.size()
						&& specfPackFeatures == homePageContainer.specfPackExteriorLighting.size()
						&& specfPackFeatures == homePageContainer.specfPackConvenience.size()
						&& specfPackFeatures == homePageContainer.specfPackSEATING.size()
						&& specfPackFeatures == homePageContainer.specfPackINFOTAINMENT.size()
						&& specfPackFeatures == homePageContainer.specfPackDriver.size()) {
					extenVerificationObj.extentVerification("All specification pack features are visible", true, true,
							"");
				} else {
					extenVerificationObj.extentVerification("All specification pack features are not visible", true,
							false, "");
				}
			}
		} catch (Exception e) {
			extentLogger.error("Specification Pack : " + e);
		}

	}

	public void avokaTransact(String models, String engine, String colours, String headlining, String paymentOpt,
			String totalDeposit, String deposit, String perMonth, String finalValue, String partExchange,
			String VehicleMake, String FuelType, String registrationNumber) {
		try {
			driver.get(Config.getPropertyValue("AVOKA.url"));
			ActionHandler.setText(svcrm4Container.avokaID, Config.getPropertyValue("valid.avokaEmail"));
			ActionHandler.setText(svcrm4Container.avokaPass, Config.getPropertyValue("valid.avokaPassword"));
			ActionHandler.click(svcrm4Container.avokaSubButton);
			ActionHandler.wait(2);
			// click on latest transaction
			ActionHandler.click(svcrm4Container.avokaTrans);
			ActionHandler.wait(2);
			ActionHandler.click(svcrm4Container.avokaXMLMenu);
			ActionHandler.wait(4);
			Actions action = new Actions(driver);
			// click on xml element to set focus on xml code.
			ActionHandler.click(svcrm4Container.avokaXMLEle);

			action.doubleClick();
			action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).keyDown(Keys.CONTROL).sendKeys("c")
					.keyUp(Keys.CONTROL).build().perform();

			// copy clipboard value into string variable
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable contents = clipboard.getContents(null);
			String str = (String) contents.getTransferData(DataFlavor.stringFlavor);

			// create xml file from string
			File newTextFile = new File(System.getProperty("user.dir") + "/src/test/resources/avokaXmlFile.xml");
			FileWriter fw = new FileWriter(newTextFile);
			fw.write(str);
			fw.close();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(newTextFile);
			extenVerificationObj.extentVerification("Email ID in XML", Config.getPropertyValue("email"),
					doc.getElementsByTagName("AccountEmailAddress").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("First name in XML ", Config.getPropertyValue("firstName"),
					doc.getElementsByTagName("AccountFirstName").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Last name in XML ", Config.getPropertyValue("lastName"),
					doc.getElementsByTagName("AccountLastName").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Mobile number in XML ", Config.getPropertyValue("mobileNoR1"),
					doc.getElementsByTagName("AccountMobilePhoneNumber").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Lead Vehicle Use Type in XML ",
					Config.getPropertyValue("use").split(" ")[0],
					doc.getElementsByTagName("LeadVehicleUseType").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Car Engine in XML ", true,
					doc.getElementsByTagName("Detail").item(3).getTextContent().contains(engine), "Yes");
			extenVerificationObj.extentInfo("Engine : " + doc.getElementsByTagName("Detail").item(3).getTextContent());
			extenVerificationObj.extentVerification("Car Colour in XML ", true,
					doc.getElementsByTagName("Detail").item(5).getTextContent().contains(colours), "Yes");
			extenVerificationObj.extentInfo("Colors : " + doc.getElementsByTagName("Detail").item(5).getTextContent());
			extenVerificationObj.extentVerification("Car headlining in XML ", true,
					doc.getElementsByTagName("Detail").item(7).getTextContent().contains(headlining), "Yes");
			extenVerificationObj
					.extentInfo("headlining : " + doc.getElementsByTagName("Detail").item(7).getTextContent());
			extenVerificationObj.extentVerification("Car Country Code in XML ", "GB",
					doc.getElementsByTagName("CountryCode").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentVerification("Car Model in XML ", models,
					doc.getElementsByTagName("Name").item(0).getTextContent(), "Yes");
			extenVerificationObj.extentInfo(
					"Exp: " + finalValue + " , Act: " + doc.getElementsByTagName("BasePrice").item(0).getTextContent());
			extenVerificationObj.extentVerification("Car Final Price in XML ", true, finalValue.replace(",", "")
					.contains(doc.getElementsByTagName("BasePrice").item(0).getTextContent()), "Yes");
			if (!"HIRE PURCHASE".equalsIgnoreCase(paymentOpt))
				paymentOpt = paymentOpt.substring(0, paymentOpt.indexOf(" ("));

			/*
			 * extentVerification("Car Payment Option in XML ", paymentOpt,
			 * doc.getElementsByTagName("FinanceProduct").item(0).getTextContent
			 * (), "Yes");
			 */
			if (!"".equals(totalDeposit))
				extenVerificationObj.extentVerification("Car Total Deposit in XML ", totalDeposit,
						doc.getElementsByTagName("TotalDeposit").item(0).getTextContent(), "Yes");
			if (!"".equals(deposit))
				extenVerificationObj.extentVerification("Car Deposit/cashback in XML ", "-" + deposit,
						doc.getElementsByTagName("Deposit").item(0).getTextContent(), "Yes");
			if (!"".equals(perMonth))
				extenVerificationObj.extentVerification("Car Installment Repayment in XML ", perMonth.substring(1),
						doc.getElementsByTagName("InstallmentRepayments").item(0).getTextContent(), "Yes");

			if (partExchange.equalsIgnoreCase("Yes")) {
				extenVerificationObj.extentInfo("PartExchange validation in XML");
				extenVerificationObj.extentVerification("VehicleMake in XML ", VehicleMake.split(" ")[0],
						doc.getElementsByTagName("VehicleMake").item(0).getTextContent(), "Yes");
				extenVerificationObj.extentVerification("Part Exchange Vehicle FuelType in XML ",
						FuelType.split(": ")[1], doc.getElementsByTagName("FuelType").item(1).getTextContent(), "Yes");
				extenVerificationObj.extentVerification("Part Exchange Vehicle Current Mileage in XML ",
						Constants.MILEAGE, doc.getElementsByTagName("VehicleCurrentMileage").item(0).getTextContent(),
						"Yes");

				extenVerificationObj.extentVerification("Part Exchange Vehicle Mileage Type in XML ", "Miles",
						doc.getElementsByTagName("VehicleMileageType").item(0).getTextContent(), "Yes");
				extenVerificationObj.extentVerification("Part Exchange Vehicle Condition in XML ", "Good",
						doc.getElementsByTagName("VehicleCondition").item(0).getTextContent(), "Yes");

				extenVerificationObj.extentVerification("Part Exchange Vehicle Settlement figure in XML ",
						Constants.SETTLEMENTFIGURE,
						doc.getElementsByTagName("OutstandingFinance").item(0).getTextContent(), "Yes");
				extenVerificationObj.extentVerification("Part Exchange Registration Number in XML ", registrationNumber,
						doc.getElementsByTagName("Registration").item(0).getTextContent(), "Yes");
			}

			ActionHandler.click(svcrm4Container.avokaLogOut);
		} catch (Exception e) {
			extentLogger.error("Exception - avoka : " + e);
		}

	}
	public void SalesDX(String model, String engBlock, String packName, String nextStep, String paymentOpt,
			  String portals) {
			 try {
			  ActionHandler.wait(2);
			  extentLogger.log(Status.INFO, "Selecting model : " + model);
			  ActionHandler.wait(10);
			  if (VerifyHandler.verifyElementPresent(homePageContainer.bodyStylesLink)) {
			   ActionHandler.click(homePageContainer.bodyStylesLink);
			   ActionHandler.wait(15);
			  }
			  ActionHandler.wait(3);
			  if (StringUtils.isNotBlank(model) && !model.equals("NA")) {
			   ActionHandler.click(driver.findElement(By.xpath(homePageContainer.modelBlock(model))));
			   ActionHandler.wait(15);
			  }
			  // Engine


			  LOGGER.info("Selecting  : " + engBlock);
			  extentLogger.log(Status.INFO, "Selecting engine : " + engBlock); 
			  ActionHandler.wait(3);
			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.engBlock(engBlock))));
			  ActionHandler.wait(10);
			  List<WebElement> engBlocks = homePageContainer.allEngBlocks;
			  for (WebElement engine : engBlocks) {
			   if (engBlock.equalsIgnoreCase(engine.getText())) {
			    engine.click();
			    ActionHandler.wait(10);
			   }
			  }
			  // Specification Pack

			  ActionHandler.wait(5);
			/*  if (!VerifyHandler.verifyElementPresent(homePageContainer.bodyStylesLink)) {
			 *   specificationPackValidation(portals, engBlock);

			  } */


			  System.out.println("before...........");


			  LOGGER.info("Selecting specification Pack : " + packName);


			  extentLogger.log(Status.INFO, "Selecting specification Pack : " + packName); 





			  String packValue = driver.findElement(By.xpath(homePageContainer.specificPackValue(packName))).getText();


			  System.out.println(packValue);


			  LOGGER.info("Pack Value:" + packValue);


			  System.out.println(packName);


			//  ActionHandler.click(driver.findElement(By.xpath("(//*[@class='toggle__icon'])[1]")));


			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.specificPackValue(packName))));


			  // NextStep


			  // nextStep="CONTINUE BUILD TO ORDER";


			//  extentLogger.log(Status.INFO, "Selecting Option " + nextStep);


			  ActionHandler.wait(10);


			  ActionHandler.click(driver.findElement(By.xpath(homePageContainer.nextStep(nextStep))));

			  ActionHandler.wait(10);

			  String headerSummaryVaue = ActionHandler.getElementText(homePageContainer.headerSummary);

	/*		  if (homePageContainer.vehicleType.getText().contains("ELECTRIC")) {


			   packValue = String.valueOf(Integer.parseInt(packValue.replaceAll("[^\\d.]", "").replaceAll(".00", "")) - 3500);

			   headerSummaryVaue = headerSummaryVaue.replaceAll("[^\\d.]", "").replaceAll(".00", "");

			  }
			  extenVerificationObj.extentVerification("Verifiying Header Summary Value", packValue, headerSummaryVaue,""); 


			   assertTrue(packValue.equals(headerSummaryVaue));

			  ActionHandler.wait(4); */


			 } catch (Exception e) {

			  extentLogger.error("Home Page : " + e);

			 }

			}
	public void getVISTA_url() {
		String VISTA_url = null;
		try {
			VISTA_url = Config.getPropertyValue("VISTA_url");
			assertTrue(StringUtils.isNotBlank(VISTA_url));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching VISTA_url.", e);
		driver.get(VISTA_url);
	    }
	}
	
	public void getMagento_url() {
		String Magento_url = null;
		try {
			Magento_url = Config.getPropertyValue("Magento_url");
			assertTrue(StringUtils.isNotBlank(Magento_url));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching Magento_url.", e);
		driver.get(Magento_url);
		ActionHandler.wait(10);
	    }
	}
	
	public void Magentocreds() {
			
		//Set username for Magento Admin portal
		String username = null;
		try {
			username = Config.getPropertyValue("Magento.username");
			assertTrue(StringUtils.isNotBlank(username));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching username.", e);
		}
		ActionHandler.click(Magento_AdminContainer.username);
		ActionHandler.setText(Magento_AdminContainer.username, username);
		ActionHandler.wait(2);
		
		//Set password for Magento Admin portal
		String password = null;
		try {
			password = Config.getPropertyValue("Magento.Password");
			assertTrue(StringUtils.isNotBlank(password));
		} catch (IOException e) {
			LOGGER.error("IOException while fetching username.", e);
		}
		ActionHandler.click(Magento_AdminContainer.password);
		ActionHandler.setText(Magento_AdminContainer.password, password);
		ActionHandler.wait(2);
		LOGGER.info("Entered Credentials");
		
		//Click on sign in
		ActionHandler.click(Magento_AdminContainer.SigninButton);
		ActionHandler.wait(10);
	}
	
	public void SalesDx_Model_Select(String model) {
//Landrover
		//LTO
				//If Range Rover
				if (("20MY RANGE ROVER EVOQUE").equals(model)) {
				ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l551/k20/l551_k20_gl_vehicle.jpg?v=487qt']")));
				}
				
				//If NEW DISCOVERY SPORT
				if (("NEW DISCOVERY SPORT").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l550/k20/l550_k20_gl_vehicle.jpg?v=487pj']")));
					}
				
				//If RANGE ROVER VELAR
				if (("RANGE ROVER VELAR").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l560/k18/l560_k18_gl_vehicle.jpg?v=487jw']")));
					}
				
				//If NEW LAND ROVER DEFENDER
				if (("NEW LAND ROVER DEFENDER").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l663/k20/l663_k20_gl_vehicle.jpg?v=487py']")));
					}
				
				//If LAND ROVER DISCOVERY
				if (("LAND ROVER DISCOVERY").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l462/k19/l462_k19_gl_vehicle.jpg?v=487ky']")));
					}
					
				//RANGE ROVER SPORT
				if (("NEW RANGE ROVER SPORT").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l494/k19/l494_k19_gl_vehicle.jpg?v=487qs']")));
					}
				
				//If RANGE ROVER
				if (("RANGE ROVER").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l405/k19/l405_k19_gl_vehicle.jpg?v=487lk']")));
					}
				
				
		//BTO
				//If NEW RANGE ROVER EVOQUE
				if (("NEW RANGE ROVER EVOQUE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l551/k20/l551_k20_gl_vehicle.jpg?v=47rad']")));
					}
				
				//If 20MY DISCOVERY SPORT
				if (("20MY DISCOVERY SPORT").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l550/k20/l550_k20_gl_vehicle.jpg?v=487r1']")));
					}
				
				//If 20MY LAND ROVER DEFENDER
				if (("20MY LAND ROVER DEFENDER").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l663/k20/l663_k20_gl_vehicle.jpg?v=487r1']")));
					}
				
				//If 20MY RANGE ROVER VELAR
				if (("20MY RANGE ROVER VELAR").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l560/k18/l560_k18_gl_vehicle.jpg?v=487qq']")));
					}
		
				//If RANGE ROVER SPORT
				if (("RANGE ROVER SPORT").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.landrover.com/lr2/l494/k19/l494_k19_gl_vehicle.jpg?v=482vx']")));
					}
				

		
//jaguar
				//BTO
				//If JAGUAR E-PACE
				if (("JAGUAR E-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/epace/k19/epace_k19_gl_vehicle.jpg?v=487pz']")));
					}
				
				//If NEW JAGUAR XE
				if (("NEW JAGUAR XE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/xe/k20/xe_k20_gl_vehicle.jpg?v=487l2']")));
					}
				
				//If JAGUAR XF
				if (("JAGUAR XF").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/xf/k19/xf_k19_gl_vehicle.jpg?v=487l0']")));
					}
				
				//If JAGUAR F-PACE
				if (("JAGUAR F-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/fpace/k19/fpace_k19_gl_vehicle.jpg?v=487q4']")));
					}
				
				//If NEW JAGUAR F-TYPE
				if (("NEW JAGUAR F-TYPE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/ftype/k21/ftype_k21_gl_vehicle.jpg?v=487l9']")));
					}
				
				//If ALL-ELECTRIC JAGUAR I-PACE
				if (("JAGUAR I-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/ipace/k19/ipace_k19_gl_vehicle.jpg?v=487p9']")));
					}
				
		//LTO
				//If JAGUAR E-PACE
				if (("E-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/epace/k19/epace_k19_gl_vehicle.jpg?v=484yn']")));
					}
				
				//If JAGUAR XF
				if (("XF").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/xf/k19/xf_k19_gl_vehicle.jpg?v=484yw']")));
					}
				
				//If NEW JAGUAR XE
				if (("XE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/xe/k20/xe_k20_gl_vehicle.jpg?v=484yy']")));
					}
				
				//If JAGUAR F-PACE
				if (("F-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/fpace/k19/fpace_k19_gl_vehicle.jpg?v=484ys']")));
					}
				
				//If NEW JAGUAR F-TYPE
				if (("F-TYPE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/ftype/k21/ftype_k21_gl_vehicle.jpg?v=484z3']")));
					}
				
				//If ALL-ELECTRIC JAGUAR I-PACE
				if (("I-PACE").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/ipace/k19/ipace_k19_gl_vehicle.jpg?v=484xy']")));
					}
				
				//If JAGUAR XJ
				if (("XJ").equals(model)) {
					ActionHandler.click(driver.findElement(By.xpath("//img[@src='https://assets.config.jaguar.com/jag2/cj/k19/cj_k19_gl_vehicle.jpg?v=484xu']")));
					}
				
				ActionHandler.wait(8);
				extentLogger.info("Select model" +model);
	}
}
