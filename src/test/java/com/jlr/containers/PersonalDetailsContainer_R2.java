package com.jlr.containers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PersonalDetailsContainer_R2 {

	@FindBy(how = How.ID, using = "acs-229")
	public WebElement pdEmailTxt;

	public String useButton(String use) {
		return "//*[text()='" + use + "']";
	}

	public String businessUseButton(String businessUse) {
		return "//*[text()='" + businessUse + "']";
	}

	@FindBy(how = How.ID, using = "acs-234")
	public WebElement personalUseButton;

	@FindBy(how = How.ID, using = "acs-235")
	public WebElement buisnessUseButton;

	@FindBy(how = How.ID, using = "acs-242")
	public WebElement buisnessUseCar_DriverButton;

	@FindBy(how = How.ID, using = "acs-243")
	public WebElement buisnessUseSmallBusButton;

	@FindBy(how = How.ID, using = "acs-244")
	public WebElement buisnessUseFleetButton;

	@FindBy(how = How.ID, using = "acs-249")
	public WebElement organisaNameTxt;

	@FindBy(how = How.XPATH, using ="//*[text()='Title: ']/following :: div[1]")
	public WebElement titleDropButton;

	@FindBy(how = How.XPATH, using = "//option[@value='Mr']")
	public WebElement titleDrop;

	@FindBy(how = How.ID, using = "acs-247")
	public WebElement firstNameTxt;

	@FindBy(how = How.ID, using = "acs-248")
	public WebElement lastNameTxt;

	@FindBy(how = How.ID, using = "acs-254")
	public WebElement mobileTxt;

	@FindBy(how = How.ID, using = "acs-270")
	public WebElement addrTxt;

	@FindBy(how = How.ID, using = "acs-364")
	public WebElement dateTimeCal;

	@FindBy(how = How.ID, using = "acs-381")
	public WebElement postTxt;

	@FindBy(how = How.XPATH, using ="//div[@data-sfc-name='LandRoverEMail']") //"//div[@class='av-checkbox av-optional id-eMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement emailChkBox;

	@FindBy(how = How.XPATH, using = "//div[@data-sfc-name='LandRoverTelephone']") //"//div[@class='av-checkbox av-optional id-telephone mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement telephoneChkBox;

	@FindBy(how = How.XPATH, using = "//div[@data-sfc-name='LandRoverLetter']") //"//div[@class='av-checkbox av-optional id-postDirectMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement postChkBox;

	@FindBy(how = How.XPATH, using = "//div[@data-sfc-name='LandRoverSMS']") //"//div[@class='av-checkbox av-optional id-smsTextMessage mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement smsChkBox;

	@FindBy(how = How.XPATH, using = "//*[text()='SUBMIT']")
	public WebElement submitBtn;

	@FindBy(how = How.CLASS_NAME, using = "thankyou-title")
	public List<WebElement> successMsg;
	
	@FindBy(how = How.XPATH, using = "//div[@class='thankyou-title']/following::div[@class='content']//descendant::div[1]")
	public List<WebElement> successMsg2;
	
	//div[@class='thankyou-title']/following::div[@class='content']//descendant::div[1]

	@FindBy(how = How.XPATH, using = "//H3[text()='Personal Details']")
	public WebElement personDetailHeading;

}
