package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ShoppingBasketContainer {

	@FindBy(how = How.XPATH, using = "//button[@title='CONTINUE']")
	public WebElement continueBtn;

	@FindBy(how = How.LINK_TEXT, using = "SELECT YOUR FINANCE OPTION")
	public WebElement financeOptLink;

	// Log In Or Register
	@FindBy(how = How.XPATH, using = "//label[text()='Log in']")
	public WebElement lonInRadioBtn;

	@FindBy(how = How.XPATH, using = "//label[text()='Register']")
	public WebElement registerRadioBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='gigya-input-text'][@placeholder='EMAIL ADDRESS *']")
	public WebElement emailTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@class='gigya-input-password'][@placeholder='PASSWORD *']")
	public WebElement passwordTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@value='LOGIN']")
	public WebElement loginBtn;

	// After Login

	public String deliveryMethod(String option) {
		return "//*[text()='" + option + "']"; // 1.Collect from a retailer
													// 2. Deliver to my home
	}

	@FindBy(how = How.ID, using = "button-reservation")
	public WebElement contReservationBtn;

	@FindBy(how = How.XPATH, using = "//strong[contains(text(),'I acknowledge the price update')]")
	public WebElement acknowLink;

	@FindBy(how = How.XPATH, using = "//a[@title='Continue to reservation']")
	public WebElement contOnlineRetailerBtn;

	public String retailerContactPref(String telEmail) {
		return "//*[text()='" + telEmail + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='field checkbox choice terms_and_conditions required']//label[@class='label']")
	public WebElement retailerTermsCond;
	
	@FindBy(how = How.ID, using = "search_retailer")
	public WebElement retailerSearchTxtBox;
	
	@FindBy(how = How.XPATH, using = "//*[@class='action primary']")
	public WebElement retailerSearch;
	
	@FindBy(how = How.XPATH, using = "//div[@class='search-retailers__autocomplete search-retailers-list']//following::li[@class='autocomplete-retailer list-g__item no-index']")
	public WebElement retailerSearchResult;
	
	public String retailerbox(String retailer)
	{
		return "//div[text()='"+retailer+"']";
	}
	
	@FindBy(how = How.XPATH, using = "//li[@class='retailers__item list-b__item'][1]")
	public WebElement firstRetailer;
	
	@FindBy(how = How.XPATH, using = "//*[@class='get-preferred-retailer']")
	public WebElement PreferedRetailer;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='list-b__item-title'])[1]")
	public WebElement PreferedRetailerName;

	@FindBy(how = How.LINK_TEXT, using = "FIND ANOTHER RETAILER")
	public WebElement findAnotherRetailerLink;
	
	@FindBy(how = How.XPATH, using = "//*[@class='gigya-screen address-search']")
	public WebElement Addresssearch;
	
	@FindBy(how = How.XPATH, using = "//div[@id='checkout-shipping-method-load']")
	public WebElement deliveryOptions;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Enter address manually')]")
	public WebElement EnterManually;
	
	@FindBy(how = How.XPATH, using = "//button[@id='btn-send-retailer']")
	public WebElement sendToRetailerBtn;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Your reference number is ']//following :: strong")
	public WebElement refNumTxt;
	
	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'DETAILS SENT TO RETAILER')]")////h2[contains(text(),'YOUR CONFIGURATION IS ON ITS WAY')]")
	public WebElement retilerSuccessMsg;

	@FindBy(how = How.XPATH, using = "//a[@title='VIEW SUMMARY']")
	public WebElement viewSummeryBtn;
	
	@FindBy(how = How.LINK_TEXT, using = "SEND TO RETAILER")
	public WebElement sendToRetailerLink;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Home phone number *']")
	public WebElement homePhoneNumTxtBox;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Home phone number *']//following::input[@value='Submit'][1]")
	public WebElement phoneNumSubmitBtn;
	
	@FindBy(how = How.XPATH, using = "//a[text()='Edit']")
	public WebElement phoneEditLink;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Save Shopping Basket']")
	public WebElement saveBasketLink;
	
	public String saveBasketMsg(String msg)
	{
		return "//div[text()='"+msg+"']";
	}
	
	@FindBy(how = How.XPATH, using = "//input[contains(@placeholder,'Email Address')]")
	public WebElement regEmailTxtBox;
	
	@FindBy(how = How.XPATH, using = "(//*[contains(@name,'profile.firstName')])[2]")
	public WebElement regFNameTxtBox;
	
	@FindBy(how = How.XPATH, using = "(//*[contains(@name,'profile.lastName')])[2]")
	public WebElement regLNameTxtBox;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@placeholder,'Password')]")
	public WebElement RegPwdTxtBox;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'I have read and understood the ')]")
	public WebElement regAcceptLink;
	
	@FindBy(how = How.XPATH, using = "//div[text()='ONLINE RETAILER PRICE']")
	public WebElement retailerPriceTxt;
	
	@FindBy(how = How.XPATH, using = "//label[@for='price_update']")
	public WebElement retailerPriceAck;
	
	@FindBy(how = How.XPATH, using = "//*[@class='action primary large']")
	public WebElement retailerPriceContinueBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@value='CONTINUE']")
	public WebElement regContinueBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@title='CONTINUE']")
	public WebElement onlineContinueBtn;
	
	@FindBy(how = How.XPATH, using = "//a[text()='BACK TO LOGIN']")
	public WebElement regBackToLoginLink;
	
	//Continue Reservation
	@FindBy(how = How.ID, using = "button-reservation")
	public WebElement continueResrvBtn;
	
	@FindBy(how = How.XPATH, using = "//span[@id='editAddressPaymentStep']")
	public WebElement editAddressLink;
	
	@FindBy(how = How.XPATH, using = "//a[@id='home-delivery-edit-address']")
	public WebElement editHomeDelivAddrLink;
	
	@FindBy(how = How.XPATH, using = "//input[@class='gigya-screen address-search']")
	public WebElement Addnewaddress;
	
	@FindBy(how = How.XPATH, using = "//form[@id='gigya-profile-form-address']//input[@value='Submit']")
	public WebElement submitAddressBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@id='block-sagepay']")
	public WebElement sagepay;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left'])[1]")
	public WebElement FP;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left'])[2]")
	public WebElement OTR_1;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left item-background'])[2]")
	public WebElement OTR_2;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left'])[5]")
	public WebElement Total_Credit_1;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left item-background'])[5]")
	public WebElement Total_Credit_2;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left'])[8]")
	public WebElement firstPayment_1;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left item-background'])[8]")
	public WebElement firstPayment_2;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left'])[10]")
	public WebElement totalPayment_1;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='align-left item-background'])[10]")
	public WebElement totalPayment_2;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='link-a'])[3]")
	public WebElement EditFinancefromBasket;
	
	@FindBy(how = How.LINK_TEXT, using = "BACK")
	public WebElement EditFinancefromBasket_BACK;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='section-a__edit'])[3]")
	public WebElement Edit_FromBasket;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='list-a__item-value '])[1]")
	public WebElement choosenFP;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='profile.phones.number' and @class='gigya-input-text'])[5]")
	public WebElement homephone;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='profile.phones.number' and @class='gigya-input-text'])[6]")
	public WebElement homephone2;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='gigya-input-submit'])[8]")
	public WebElement submitphone;
	
	@FindBy(how = How.XPATH, using = "//a[@id='home-delivery-edit-phone']")
	public WebElement edit_phone;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Accept All Cookies']")
	public WebElement Acceptcookies;
}
