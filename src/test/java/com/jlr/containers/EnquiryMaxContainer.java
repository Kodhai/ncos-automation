package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class EnquiryMaxContainer {

	@FindBy(how = How.ID, using = "UserName")
	public WebElement userNameSVCRM;

	@FindBy(how = How.ID, using = "Password")
	public WebElement passwordNameSVCRM;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement submitButton;

	@FindBy(how = How.XPATH, using = "//label[@for='Leads'] [text()='Central Leads']")
	public WebElement centralLeadLink;

	public String getInfo(String info) {

		return "//td[@sorttext='" + info + "'][1]//preceding-sibling::td";
	}

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Title')]//following::input[1]")
	public WebElement popUpTitle;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Email')]//following::input[1]")
	public WebElement popUpEmail;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Forename')]//following::input[1]")
	public WebElement popUpForename;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Surname')]//following::input[1]")
	public WebElement popUpSurname;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Mobile')]//following::input[1]")
	public WebElement popUpMobile;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Address')]//following::input[1]")
	public WebElement popUpAddress1;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Address')]//following::input[2]")
	public WebElement popUpAddress2;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Address')]//following::input[3]")
	public WebElement popUpAddress3;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Postcode')]//following::input[1]")
	public WebElement popUpPostcode;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vehicle of Enquiry/Proposed Vehicle')]//following-sibling :: div[contains(text(),'Make')]//following::input[1]")
	public WebElement popUpMake;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vehicle of Enquiry/Proposed Vehicle')]//following-sibling :: div[contains(text(),'Model')]//following::input[1]")
	public WebElement popUpModel;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vehicle of Enquiry/Proposed Vehicle')]//following-sibling :: div[contains(text(),'Derivative')]//following::input[1]")
	public WebElement popUpDerivative;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vehicle of Enquiry/Proposed Vehicle')]//following-sibling :: div[contains(text(),'Price')]//following::input[1]")
	public WebElement popUpPrice;

	@FindBy(how = How.ID, using = "j_username;")
	public WebElement avokaID;

	@FindBy(how = How.ID, using = "j_password")
	public WebElement avokaPass;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	public WebElement avokaSubButton;

	@FindBy(how = How.XPATH, using = "//*[text()='Latest Transactions']//following::table[1]//descendant::tr[2]/td[1]/a")
	public WebElement avokaTrans;

	@FindBy(how = How.ID, using = "Form_XML_Data")
	public WebElement avokaXMLMenu;

	@FindBy(how = How.XPATH, using = "//span[text()='<AccountEmailAddress>']//parent::pre")
	public WebElement avokaXMLEle;

	@FindBy(how = How.XPATH, using = "//a[text()='Logout']")
	public WebElement avokaLogOut;

	@FindBy(how = How.ID, using = "HeaderUserMenu")
	public WebElement logOutMenu;

	@FindBy(how = How.XPATH, using = "//li[text()='Logout']")
	public WebElement logOut;

}
