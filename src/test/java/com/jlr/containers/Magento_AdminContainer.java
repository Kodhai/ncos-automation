package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Magento_AdminContainer {

	@FindBy(how = How.XPATH, using = "//*[@name='login[username]']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//*[@name='login[password]']")
	public WebElement password;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Sign in']")
	public WebElement SigninButton;
	
	@FindBy(how = How.XPATH, using = "(//button[@class='action-close'])[1]")
	public WebElement Closepopup;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Sales'])[1]")
	public WebElement SalesMenu;
	
	@FindBy(how = How.XPATH, using = "(//span[text()='Orders'])[1]")
	public WebElement Sales_Orders;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='admin__control-text data-grid-search-control'])[1]")
	public WebElement search_ID;
	
	@FindBy(how = How.LINK_TEXT, using = "View")
	public WebElement View;
	
	@FindBy(how = How.XPATH, using = "//span[@id='order_status']")
	public WebElement VistaStatus;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='title'])[2]")
	public WebElement Order_ID;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='data-grid-cell-content'])[3]")
	public WebElement CustomerName;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='data-grid-cell-content'])[5]")
	public WebElement PaymentStatus;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='data-grid-cell-content'])[7]")
	public WebElement RetailerName;
}
