package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class InteriorContainer {

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='INTERIOR']")
	public WebElement interiorTab;

	@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[text()='HEADLINING']")
	public WebElement headlining;

	public String selectGivenCategory(String category) {
		//return "//*[contains(@class,'tray-block__line')][text()='" + category + "']/parent::div/parent::div//div[contains(@class,'toggle__icon')]";
		return "//*[contains(@class,'tray-block__line')][text()='" + category + "']";
	}

}
