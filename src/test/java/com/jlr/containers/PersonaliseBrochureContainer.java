package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PersonaliseBrochureContainer {

	@FindBy(how = How.ID, using = "accountEmailAddress")
	public WebElement pdEmailTxt;

	public String contactedByRetailer(String use) {
		return "//div//label//span[text()='"+use+"']";
	}

	@FindBy(how = How.ID, using = "accountFirstName")
	public WebElement firstName;

	@FindBy(how = How.ID, using = "accountLastName")
	public WebElement lastName;

	@FindBy(how = How.ID, using = "retailerPostcode")
	public WebElement retailerPostCode;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-eMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement emailChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-telephone mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement telephoneChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-postDirectMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement postChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-smsTextMessage mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement smsChkBox;

	@FindBy(how = How.XPATH, using = "//*[text()='Submit']")
	public WebElement submitBtn;
		
	@FindBy(how = How.XPATH, using = "//SPAN[@class='wdg-button-label'][text()='Download Your Brochure']")
	public WebElement downloadBrochure;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ng-binding')]")
	public WebElement thankYouMsg;
	
	


}
