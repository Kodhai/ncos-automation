package com.jlr.containers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SummaryContainer {

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='SUMMARY']")
	public WebElement summaryTab;

	// It will contain p/m value in case of finance quote
	@FindBy(how = How.XPATH, using = "(//TD[@class='summary-block__table-heading-value'])[2]")
	public WebElement grandTotal;

	@FindBy(how = How.XPATH, using = "//*[text()='Office of Low Emission Vehicles (OLEV) Grant']//following::td[1]")
	public List<WebElement> olevGrantValue;

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'cta-primary')][text()='ORDER NOW']")
	public WebElement orderNow;

	@FindBy(how = How.XPATH, using = "//*[text()='NEXT STEPS']//following::button[1]")
	public WebElement sendToRetailer;

	@FindBy(how = How.XPATH, using = "//span[text()='SHARE YOUR BUILD']")
	public WebElement shareYourBuild;

	@FindBy(how = How.XPATH, using = "//LABEL[@for='at-expanded-menu-service-filter']")
	public WebElement findService;

	@FindBy(how = How.XPATH, using = "//span[@class='at-icon-name']")
	public WebElement appSelection;
	
	@FindBy(how = How.XPATH, using = "//SPAN[text()='[TEST] PERSONALISED BROCHURE']")
	public WebElement personalisedBrochure;


	

}
