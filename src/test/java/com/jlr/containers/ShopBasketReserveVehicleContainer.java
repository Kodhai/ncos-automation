package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ShopBasketReserveVehicleContainer {

	// Reserve vehicle page (card details)

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Name on card']")
	public WebElement cardNameTxtBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Card number']")
	public WebElement cardNumberTxtBox;

	@FindBy(how = How.XPATH, using = "(//*[@class='select2-selection__rendered'])[5]")
	public WebElement expMonthDrpDown;

	@FindBy(how = How.XPATH, using = "(//*[@class='select2-selection__rendered'])[6]")
	public WebElement expYearDrpDown;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Security code (CVV)']")
	public WebElement cvvTxtBox;

	@FindBy(how = How.XPATH, using = "//span[text()='I agree to the']")
	public WebElement termConditionBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Pay securely']")
	public WebElement paySecureBtn;
	
	@FindBy(how = How.ID, using = "sagepaysuitepi-3Dsecure-iframe")
	public WebElement frame;
	
	//Password
	@FindBy(how = How.XPATH, using = "//input[@id='field_password']")
	public WebElement framePassword;
	
	@FindBy(how = How.ID, using = "submit-button")
	public WebElement frameSubmitBtn;
	
	@FindBy(how = How.XPATH, using = "(//dd[@class='list-i__item-value'])[1]")
	public WebElement reservationNum;
	
	@FindBy(how = How.XPATH, using = "(//dd[@class='list-i__item-value'])[2]")
	public WebElement reservationAmt;
	
	@FindBy(how = How.XPATH, using = "//div[@class='checkout-success']/h2")
	public WebElement paymentSuccessMsg;
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "APPLY FOR FINANCE")
	public WebElement financeAppBtn;
	
	@FindBy(how = How.XPATH, using = "//ul[@class='tabs-a__items']/li[1]")
	public WebElement onlineTab;
	
	@FindBy(how = How.XPATH, using = "//ul[@class='tabs-a__items']/li[2]")
	public WebElement retailerTab;
	
	@FindBy(how = How.XPATH, using = "//button[@title='APPLY VIA A RETAILER']")
	public WebElement viaRetailerBtn;
	
	@FindBy(how = How.XPATH, using = "//div[text()='Thank you.']//following::div[2]")
	public WebElement viaRetailerFinalTxt;
	
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "APPLY ONLINE")
	public WebElement viaOnlineBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@class='panel-a__h2']")
	public WebElement orderProcessMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Street *']")
	public WebElement addrStreet1;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Street 2']")
	public WebElement addrStreet2;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter your city/town *']")
	public WebElement addrCity; 
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Postcode *']")
	public WebElement addrCountry;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Postcode *']")
	public WebElement addrPostCode;
	
	@FindBy(how = How.XPATH, using = "(//*[contains(text(),'Postcode:')]//following::input[@value='Submit'])[3]")
	public WebElement addrSubmit;
	
	@FindBy(how = How.XPATH, using = "//span[text()='On The Road Price']//following::span[1]")
	public WebElement otrTxt;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Monthly Payments')]//following::span[1]")
	public WebElement monthlyCostTxt;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Part Exchange Equity')]//following::span[1]")
	public WebElement partExCostTxt;
	
	
	@FindBy(how = How.XPATH, using = "//span[text()='Finance Option']//following::span[1]")
	public WebElement financeOptionTxt;
	
	@FindBy(how = How.XPATH, using = "//div[@class='minibasket__item-link minibasket__item-link--show'][text()='FINANCE DETAILS']")
	public WebElement financeDetailExpBtn;
	
	
	
	
	
}
