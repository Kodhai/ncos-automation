package com.jlr.containers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageContainer {

	@FindBy(how = How.CLASS_NAME, using = "info-bar-title__nameplate")
	public WebElement pageHeader;

	public String modelStandardFeatures(String feature) {
		return "//*[text()='" + feature + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[@class='main-nav__link'])")
	public WebElement modelText;

	@FindBy(how = How.XPATH, using = "//div[@class='main-nav__link-container']//descendant::a[@class='main-nav__link'][text()='MODELS']")
	public WebElement modelButton;

	@FindBy(how = How.XPATH, using = "//div[@class='main-nav__link-container']//descendant::a[@class='main-nav__link'][text()='ENGINES']")
	public WebElement enginesButton;

	@FindBy(how = How.XPATH, using = "//div[@class='main-nav__link-container']//descendant::a[@class='main-nav__link'][text()='SPECIFICATION PACKS']")
	public WebElement specfPackButton;

	@FindBy(how = How.XPATH, using = "//div[@class='main-nav__link-container']//descendant::a[@class='main-nav__link'][text()='NEXT STEPS']")
	public WebElement nextStepButton;

	@FindBy(how = How.XPATH, using = "//h2[@class='grid-tray-header__title'] [text()='VELAR']")
	public WebElement modelVelar;

	@FindBy(how = How.XPATH, using = "//h2[@class='grid-tray-header__title'] [text()='R-DYNAMIC']")
	public WebElement modelRdynamic;

	@FindBy(how = How.XPATH, using = "//span[@class='info-bar-price__total']")
	public WebElement onTheRoadPrice;

	public String modelBlock(String modelName) {
		return "//span[@class='grid-tray-header__title'] [text()='" + modelName + "']";
	}

	public String engBlock(String engBlock) {
	//	return "//*[@class='engines-block']/descendant::*[text()='" + engBlock + "']";
			return "//*[text()='" + engBlock + "']";
		//*[text()='EV400']
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='engines-block']/descendant::span[@class='grid-tray-header__title']")
	public List<WebElement> allEngBlocks;

	@FindBy(how = How.XPATH, using = " //img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='WHEELS']") //"//img[@alt='WHEELS']")
	public List<WebElement> specfPackWheels;

	@FindBy(how = How.XPATH, using = " //img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='EXTERIOR LIGHTING']")//"//img[@alt='HEADLIGHTS']")
	public List<WebElement> specfPackHEADLIGHTS;

	@FindBy(how = How.XPATH, using = "//img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='CONVENIENCE']")//"//img[@alt='POWERED CONVENIENCE']")
	public List<WebElement> specfPackPOWERED;

	@FindBy(how = How.XPATH, using =" //img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='SEATING']") //"//img[@alt='SEATING']")
	public List<WebElement> specfPackSEATING;

	@FindBy(how = How.XPATH, using = " //img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='DRIVER ASSISTANCE']")//"//img[@alt='DRIVING AIDS']")
	public List<WebElement> specfPackAIDS;

	@FindBy(how = How.XPATH, using = "//img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='INFOTAINMENT']") //"//img[@alt='INFOTAINMENT']")
	public List<WebElement> specfPackINFOTAINMENT;

	@FindBy(how = How.XPATH, using = "//*[text()='BASE']/parent::tr//preceding-sibling::tr//descendant::td[@class='summary-block__table-heading-value']")
	public WebElement basePrice;

	public String specifiPack(String packName) {
		// *ST return
		return "//td[contains(@class,'grid__table-header--clickable')]/descendant::*[text()='" + packName + "']";
// (//*[text()='I-PACE S'])[1]
	//	return "//*[text()='" + packName + "']/following::span[@class='priceString']";
	}

	public String specificPackValue(String packName) {
		/*
		 * return
		 * "//td[@class='grid__table-cell grid__table-header grid__table-header--clickable']/descendant::*[text()='"
		 * + packName + "']/following :: span[@class='priceString'][1]";
		 */
		/*
		 * *ST return
		 * "//td[contains(@class,'grid__table-header--clickable')]/descendant::*[text()='"
		 * + packName + "']/following::span[@class='priceString'][1]";
		 */
		return "(//*[text()='" + packName + "'])[1]";
	}

	public String nextStep(String nextStep) {
		return "//*[text()='" + nextStep + "']";
		//*[text()='CONTINUE BUILD WITH FINANCE OPTION AND / OR PART EXCHANGE']
	}

	@FindBy(how = How.XPATH, using = "//span[@class='info-bar-price__total']")
	public WebElement headerSummary;
	
	@FindBy(how = How.XPATH, using = "//span[@class='info-bar-title__nameplate']")
	public WebElement vehicleType;

	@FindBy(how = How.CLASS_NAME, using = "info-bar__item__button__label__total")
	public WebElement perMonthInfoBarValue;

	// Jaguar E Pace
	@FindBy(how = How.XPATH, using ="//img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='EXTERIOR LIGHTING']")
	public List<WebElement> specfPackExteriorLighting;

	@FindBy(how = How.XPATH, using = "//img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='CONVENIENCE']")
	public List<WebElement> specfPackConvenience;

	@FindBy(how = How.XPATH, using = "//img//preceding::h4[@class='grid__section-heading grid__section-heading--secondary'][text()='DRIVER ASSISTANCE']")
	public List<WebElement> specfPackDriver;
	
	@FindBy(how = How.XPATH, using = "//a[@class='main-nav__link']")
	public WebElement bodyStylesLink;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cta__label-inner']")
	public WebElement XJpopup;

}
