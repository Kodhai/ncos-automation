package com.jlr.containers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PersonalDetailsContainer {

	@FindBy(how = How.ID, using = "personalDetailsEmailAddress")
	public WebElement pdEmailTxt;

	public String useButton(String use) {
		return "//*[text()='" + use + "']";
	}

	@FindBy(how = How.ID, using = "personalDetailsVehicleUseTypePersonal")
	public WebElement personalUseButton;

	@FindBy(how = How.ID, using = "personalDetailsVehicleUseTypeBusiness")
	public WebElement buisnessUseButton;

	@FindBy(how = How.ID, using = "personalDetailsBusinessUserTypeCar_Driver")
	public WebElement buisnessUseCar_DriverButton;

	@FindBy(how = How.ID, using = "personalDetailsBusinessUserTypeSmall_Business")
	public WebElement buisnessUseSmallBusButton;

	@FindBy(how = How.ID, using = "personalDetailsBusinessUserTypeFleet")
	public WebElement buisnessUseFleetButton;

	@FindBy(how = How.ID, using = "personalDetailsOrganisationName")
	public WebElement organisaNameTxt;

	@FindBy(how = How.XPATH, using = "//*[text()='Title']/following :: div[1]")
	public WebElement titleDropButton;

	@FindBy(how = How.XPATH, using = "//option[@label='Mr']")
	public WebElement titleDrop;

	@FindBy(how = How.ID, using = "personalDetailsFirstName")
	public WebElement firstNameTxt;

	@FindBy(how = How.ID, using = "personalDetailsLastName")
	public WebElement lastNameTxt;

	@FindBy(how = How.ID, using = "personalDetailsMobileNumber")
	public WebElement mobileTxt;

	@FindBy(how = How.ID, using = "pcaAddressLookupSearch")
	public WebElement addrTxt;

	@FindBy(how = How.ID, using = "inMarketDateAndTime")
	public WebElement dateTimeCal;

	@FindBy(how = How.ID, using = "retailerPostcode")
	public WebElement postTxt;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-eMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement emailChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-telephone mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement telephoneChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-postDirectMail mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement postChkBox;

	@FindBy(how = How.XPATH, using = "//div[@class='av-checkbox av-optional id-smsTextMessage mg-checkbox av-form-field col-xs-12 col-sm-12 col-md-3 col-lg-3 av-item-container']")
	public WebElement smsChkBox;

	@FindBy(how = How.XPATH, using = "//*[text()='Submit']")
	public WebElement submitBtn;

	@FindBy(how = How.CLASS_NAME, using = "ng-binding")
	public List<WebElement> successMsg;

	@FindBy(how = How.XPATH, using = "//H3[text()='Personal Details']")
	public WebElement personDetailHeading;

}
