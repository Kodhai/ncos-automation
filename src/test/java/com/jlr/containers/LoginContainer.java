package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginContainer {

	@FindBy(how = How.NAME, using = "q")
	public WebElement inputTextBox;

	@FindBy(how = How.ID, using = "username")
	public WebElement username;

	@FindBy(how = How.ID, using = "password")
	public WebElement password;

	@FindBy(how = How.NAME, using = "login")
	public WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'info-bar-close')]/div")
	public WebElement logoutButton;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'cta-primary')]")
	public WebElement goodByeButton;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'cta cta-secondary')]")
	public WebElement stayButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cta__label']")
	public WebElement Disclaimerpopup;
	
	@FindBy(how = How.XPATH, using = "//*[@class='modal-layout__inner-content-inner-close close-modal']")
	public WebElement Closepopup;	
	
	@FindBy(how = How.LINK_TEXT, using = "OK")
	public WebElement OK;	
	

}
