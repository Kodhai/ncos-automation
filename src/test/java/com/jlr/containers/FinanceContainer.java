package com.jlr.containers;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FinanceContainer {

	public String financeProduct(String value) {
		// return "//LABEL[contains(@id,'productSelector')][text()='" + value + "']";
		return "//*[text()='" + value + "']";
	}

	public String summaryFinanceProductValue(String value) {
		return "//*[text()='" + value + "']";
	}
	
	public String summaryFinanceProductOption(String value) {
		if("PCP".equals(value)) {
			return "//*[text()='PERSONAL CONTRACT PURCHASE (PCP)']";
		}
		if("HP".equals(value)) {
		return "//*[text()='HIRE PURCHASE']";
		}
		return "//*[text()='CASH PURCHASE']";
	}
	
	@FindBy(how = How.ID, using = "productSelector2")
	public WebElement hirePurchaseButton;

	@FindBy(how = How.XPATH, using = "//*[text()='CASH PURCHASE']")
	public WebElement selfFinanceCashPurchase;

	@FindBy(how = How.XPATH, using = "//BUTTON[@title='I have a vehicle to part exchange'][text()='YES']")
	public WebElement PartExchangeYes;

	@FindBy(how = How.XPATH, using = "//BUTTON[@title='I do not have a vehicle to part exchange'][text()='NO']")
	public WebElement PartExchangeNo;

	@FindBy(how = How.ID, using = "regInput")
	public WebElement registrationNumber;

	// Use for look up your vehicle and GET YOUR VALUATION buttons
	@FindBy(how = How.XPATH, using = "//button[text()='GET YOUR VALUATION']")
	public WebElement getyourvaluation;
	
	@FindBy(how = How.XPATH, using = "//button[text()='LOOK UP YOUR VEHICLE']")
	public WebElement submitButton;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'px-specs-list__spec')]")
	public List<WebElement> exchangeVehicleDetails;

	@FindBy(how = How.XPATH, using = "//button[text()='CONTINUE WITH VALUATION']")
	public WebElement continueVehicle;

	@FindBy(how = How.ID, using = "Mileage")
	public WebElement mileageTextBox;

	@FindBy(how = How.ID, using = "Good")
	public WebElement goodCondition;

	@FindBy(how = How.ID, using = "Excellent")
	public WebElement Excellent;

	@FindBy(how = How.ID, using = "Fair")
	public WebElement fairCondition;

	@FindBy(how = How.ID, using = "Yes")
	public WebElement settlementFigureYes;

	@FindBy(how = How.ID, using = "No")
	public WebElement settlementFigureNo;

	@FindBy(how = How.ID, using = "OutstandingFinanceAmount")
	public WebElement outstandingFinanceAmount;

	@FindBy(how = How.NAME, using = "AcceptTermsAndConditions")
	public WebElement acceptTermsAndConditions;

	@FindBy(how = How.XPATH, using = "//button[contains(@class,'jsc-cta')][text()='ACCEPT VALUES & CALCULATE']")
	public WebElement acceptValuesCalculate;

	@FindBy(how = How.XPATH, using = "//h3[@class='px-equity__finance-value']")
	public WebElement financeAgreementFigure;

	@FindBy(how = How.XPATH, using = "//h3[@class='px-equity__value-value']")
	public WebElement estimatedValue;

	@FindBy(how = How.XPATH, using = "//*[@class='px-equity__total-value']")
	public WebElement exchangeEquityTotalValue;

	// Getattribute of Value
	@FindBy(how = How.ID, using = "DepositManual")
	public WebElement totalDepositDefault;

	@FindBy(how = How.XPATH, using = "//*[text()='FINANCE OPTIONS']")
	public WebElement financeQuoteButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class = 'info-bar__item__button__label__total']")
	public WebElement financeQuoteButton1;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'slider__current')]")
	public List<WebElement> defaultValueList;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'minmax--maxvalue')]")
	public List<WebElement> maximumRangeList;

	@FindBy(how = How.XPATH, using = "//*[contains(@class,'minmax--minvalue')]")
	public List<WebElement> minimumRangeList;

	@FindBy(how = How.XPATH, using = "//button[text()='GET YOUR QUOTE']")
	public WebElement getYourQuote;

	@FindBy(how = How.XPATH, using = "//*[text()='ACCEPT VALUES & CALCULATE']") // ACCEPT
	public WebElement acceptAndContinue;    																				// AND
																						// CONTINUE
	

	@FindBy(how = How.XPATH, using = "//span[text()='Total Deposit']//following-sibling::span") // ACCEPT
																								// AND
																								// CONTINUE
	public WebElement finalDeposite;

	@FindBy(how = How.XPATH, using = "//BUTTON[text()='CONTINUE BUILD']")
	public WebElement continueToBuild;

	@FindBy(how = How.XPATH, using = "//BUTTON[text()='USE TOWARDS MY DEPOSIT']")
	public WebElement useTowardDepositButton;

	@FindBy(how = How.XPATH, using = "//BUTTON[text()='DEDUCT FROM ON THE ROAD PRICE']")
	public WebElement DEDUCTFROMONTHEROADPRICE;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Mileage per annum')]/following-sibling::span")
	public WebElement mileagePerAnnum;
	
	@FindBy(how = How.XPATH, using = "//*[@class = 'nf-slider__fieldlabel ncos-b-fieldlabel-AnnualMileage']")
	public WebElement AnnualMileage;
	
	@FindBy(how = How.XPATH, using = "//*[contains(@class,'px-unavailable')]")
	public WebElement partExchangeUnavailable;

	public String getQuote(String value) {
		return "//*[contains(text(),'" + value + "')]/following-sibling::span";
	}

	public String getQuoteR2(String value) {
		return "//span[text()='" + value + "']//following-sibling::span";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='OLEV Grant']/following-sibling::span")
	public List<WebElement> discountPrice;

	@FindBy(how = How.XPATH, using = "//div[@class='px-equity__no-valuation-title']/h3")
	public WebElement negativeEquityMsg1;

	@FindBy(how = How.XPATH, using = "//div[@class='px-equity__no-valuation-text']")
	public WebElement negativeEquityMsg2;

	@FindBy(how = How.XPATH, using = "//h3[@class='nf-controls__infobox__heading']")
	public WebElement surplusMsg1;

	@FindBy(how = How.XPATH, using = "//p[@class='nf-controls__infobox__text']")
	public WebElement surplusMsg2;

	@FindBy(how = How.XPATH, using = "//span[@class='nf-deposit__residual nf-deposit__residual--nonempty']")
	public WebElement cashBack;

	@FindBy(how = How.XPATH, using = "//div[@class='nf-slider__chevrons__icons']")
	public List<WebElement> sliders;

	@FindBy(how = How.XPATH, using = "//span[@class='nf-slider__fieldlabel ncos-b-fieldlabel-Term']//following-sibling::span[3]/span")
	public WebElement selectedTerm;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'miles per annum')]")
	public WebElement selectedAnnualMileage;

	// Total Deposit Slider left
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Total deposit (excluding part exchange)']/*[local-name() = 'svg'][1]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement totalDepositSliderLeft;

	//Total Deposit Slider right
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Total deposit (excluding part exchange)']/*[local-name() = 'svg'][2]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement totalDepositSliderRight;

	// Term Slider left
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Duration of Agreement']/*[local-name() = 'svg'][1]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement termSliderLeft;

	// Term Slider right
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Duration of Agreement']/*[local-name() = 'svg'][2]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement termSliderRight;

	// Annual Mileage Slider left
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Mileage Per Annum']/*[local-name() = 'svg'][1]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement annualMileageSliderLeft;

	// Annual Mileage Slider right
	@FindBy(how = How.XPATH, using = "//div[@aria-label='Mileage Per Annum']/*[local-name() = 'svg'][2]/*[local-name() = 'g']/*[local-name() = 'polygon']")
	public WebElement annualMileageSliderRight;
	
//	@FindBy(how = How.XPATH, using = "//*[text()='ORDER THIS VEHICLE']")
//	public WebElement orderVehicleLink;
	
	@FindBy(how = How.XPATH, using = "//*[text()='[STAGING] ORDER THIS VEHICLE']")
	public WebElement orderVehicleLink;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Order This Vehicle']")
	public WebElement orderVehicleLinksalesDX;
	
//	@FindBy(how = How.XPATH, using = "(//*[@class='cta cta-primary cta--location-summary-action cta--size-large cta--text-align-left cta--width-full cta--has-label cta-primary--style-default'])")
//	public WebElement orderVehicleLink;
	
	@FindBy(how = How.XPATH, using = "//span[text()='On The Road Price']//following::span[1]")
	public WebElement otrTxt;
	
	@FindBy(how = How.XPATH, using = "//div[text()='PER MONTH']//preceding::div[1]")
	public WebElement monthlyCostTxt;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Part Exchange Equity']//following::span[1]")
	public WebElement partExchangeCostTxt;
	
	@FindBy(how = How.XPATH, using = "//div[@class='nf-sections ncos-b-quote']//h2")
	public WebElement financeOptionTxt;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link-counter'])[6]")
	public WebElement Summary;
	
	@FindBy(how = How.XPATH, using = "//div[@class='info-bar__item__button__label__total']")
	public WebElement FinanceCalculator;

	@FindBy(how = How.XPATH, using = "(//button[@class='jsc-cta jsc-cta--text'])[1]")
	public WebElement EditFP;
	
	@FindBy(how = How.XPATH, using = "//button[@class='product-button'][contains(text(),'PERSONAL CONTRACT PURCHASE (PCP)')]")
	public WebElement PCP;
	
	@FindBy(how = How.XPATH, using = "//button[@class='product-button'][contains(text(),'HIRE PURCHASE')]")
	public WebElement HP;
	
	@FindBy(how = How.XPATH, using = "//button[@class='product-button'][contains(text(),'CASH PURCHASE')]")
	public WebElement Cash;
	
	@FindBy(how = How.XPATH, using = "//span[text()='OK']")
	public WebElement Disclaimer;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='info-bar__item__button__label'])[2]")
	public WebElement FC_SalesDX;
	
	@FindBy(how = How.XPATH, using = "//div[@class='widgetWithPriceSummary']")
	public WebElement FC_SalesDX_PM;
	
	@FindBy(how = How.XPATH, using = "(//*[text()='View summary'])[1]")
	public WebElement View_Summary;
	
	@FindBy(how = How.XPATH, using = "(//*[text()='View summary'])[2]")
	public WebElement View_Summary1;
}
