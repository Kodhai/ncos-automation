package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class VISTAContainer {

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='notificationCountClick(notification.notificationType)'])[1]")
	public WebElement OrderedOnline;

	@FindBy(how = How.XPATH, using = "(//span[@class='ng-binding ng-scope'])[1]")
	public WebElement ValidateConfig;
}
