package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class JaguarGearContainer {

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='JAGUAR GEAR']")
	public WebElement jaguarGearTab;

	@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[text()='CARRYING & TOWING']")
	public WebElement carryingTowing;

	@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[text()='WHEELS & WHEEL ACCESSORIES']")
	public WebElement wheelsAcc;

	public String selectGivenCategory(String category) {
		return "//*[contains(@class,'tray-block__line')][text()='" + category
				+ "']/parent::div/parent::div//div[contains(@class,'toggle__icon')]";
	}

	// E Pace
	@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[text()='TOURING ACCESSORIES']")
	public WebElement TouringAcc;

}
