package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AccessoriesContainer {

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='ACCESSORIES']")
	public WebElement accessoriesTab;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='JAGUAR GEAR']")
	public WebElement jaguarGearTab;

	@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[text()='TOURING ACCESSORIES']")
	public WebElement touringAccessories;

	public String selectGivenCategory(String category) {
		return "//*[contains(@class,'tray-block__line')][text()='" + category
				+ "']/parent::div/parent::div//div[contains(@class,'toggle__icon')]";
	}

}
