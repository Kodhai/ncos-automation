package com.jlr.containers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ExteriorContainer {

	// $x("(//span[@class='main-nav__link-counter'])[6]")
	// *[contains(@class,
	// 'main-nav__link-container')]//a[contains(@class,'main-nav__link') and
	// contains(text(),'EXTERIOR')]

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='EXTERIOR']")
	public WebElement exteriorTab;

	@FindBy(how = How.XPATH, using = "(//a[@class='sub-nav__link'])[1]")
	public WebElement colours;

	@FindBy(how = How.XPATH, using = "(//a[@class='sub-nav__link'])[2]")
	public WebElement visualPacks;

	public String selectGivenCategory(String category) {
	//	return "//*[contains(@class,'tray-block__line')][contains(text(),'" + category
	//			+ "')]/parent::div/parent::div//div[contains(@class,'toggle--select')]";
		return "//*[contains(@class,'tray-block__line')][contains(text(),'" + category + "')]";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'monthly-price--block')]")
	public List<WebElement> monthlyPriceBlock;

}
