package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ShopBasketFinanceServiceContainer {

	@FindBy(how = How.XPATH, using = "//*[@class= 'btn btn-primary']")
	public WebElement onlineConfirmQuoteBtn;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'I confirm I have read the privacy')]")
	public WebElement financeAckLink;
	
	@FindBy(how = How.XPATH, using = "//button[text()=' Agree and Continue ']")
	public WebElement AgreeContinBtn;
	
	//Application form
	@FindBy(how = How.XPATH, using = "//select[@name='Title']")
	public WebElement appTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Forename']")
	public WebElement appForeName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='MiddleName']")
	public WebElement appMiddleName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Surname']")
	public WebElement appSurname;
	
	@FindBy(how = How.XPATH, using = "//input[@name='HomeTelephone']")
	public WebElement appHomeTel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='MobileTelephone']")
	public WebElement appMobTel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Email']")
	public WebElement appEmail;
	
	@FindBy(how = How.XPATH, using = "//select[@name='DateOfBirth']")
	public WebElement appBirthDay;
	
	@FindBy(how = How.XPATH, using = "//div[@class='row']//div[2]//select[1]")
	public WebElement appBirthMonth;
	
	@FindBy(how = How.XPATH, using = "//div[@class='row']//div[3]//select[1]")
	public WebElement appBirthYear;
	
	//retailer on payment detail page
	@FindBy(how = How.XPATH, using = "//*[text()='Delivery method']//following::span[@class='list-a__item-title']")
	public WebElement retailerNameFinancePage;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Gender']")
	public WebElement appGender;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Nationality']")
	public WebElement appCountry;
	
	@FindBy(how = How.XPATH, using = "//select[@name='MaritalStatus']")
	public WebElement appMaritalStatus;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Dependants']")
	public WebElement appDependant;
	
	@FindBy(how = How.XPATH, using = "//select[@name='VehicleUsage']")
	public WebElement appVehicleUsage;
	
	//Address History
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Address History')]")  //
	public WebElement appAddHistoryBtn;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='Postcode'])[1]")
	public WebElement appAddHistoryPostCode;
	
	@FindBy(how = How.XPATH, using = "//input[@name='HouseName']")
	public WebElement appAddHisHouseName;
	
	
	@FindBy(how = How.XPATH, using = "(//input[@name='AddressLine1'])[1]")
	public WebElement appAddHisStreet;
	
	
	@FindBy(how = How.XPATH, using = "(//input[@name='AddressLine1'])[1]")
	public WebElement appAddHisDistr;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='TownCity'])[1]")
	public WebElement appAddHisTown;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='County'])[1]")
	public WebElement appAddHisCountry;
	
	@FindBy(how = How.XPATH, using = "//input[@name='YearsAtAddress']")
	public WebElement appAddHisYrsAdd;
	
	@FindBy(how = How.XPATH, using = "//input[@name='MonthsAtAddress']")
	public WebElement appAddHisMonthsAdd;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Accommodation']")
	public WebElement appAddHisAcd;
	
	//employment history
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Employment History')]")
	public WebElement empHistoryBtn;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='Postcode'])[2]")
	public WebElement empHisPostCode;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='BuildingName'])[1]")
	public WebElement empHisBuildName;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='AddressLine1'])[2]")
	public WebElement empHisStreet;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='AddressLine2'])[2]")
	public WebElement empHisDistrict;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='TownCity'])[2]")
	public WebElement empHisCity;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='County'])[2]")
	public WebElement empHisContry;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='Employer'])[1]")
	public WebElement empHisEmpr;
	
	@FindBy(how = How.XPATH, using = "(//input[@name='PhoneNumber'])[1]")
	public WebElement empHisPhone;

	@FindBy(how = How.XPATH, using = "(//input[@name='Occupation'])[1]")
	public WebElement empHisOccupation;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Basis']")
	public WebElement empHisBasis;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Category']")
	public WebElement empHisCat;
	
	@FindBy(how = How.XPATH, using = "//input[@name='YearsAtEmployment']")
	public WebElement empHisYrAtEmp;
	
	@FindBy(how = How.XPATH, using = "//input[@name='MonthsAtEmployment']")
	public WebElement empHisMonthemp;
	
	
	//Affordability
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Affordability')]")
	public WebElement AffortdBtn;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Replacement Loan']")
	public WebElement afforLoan;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Gross Annual Income']")
	public WebElement afforGrossIncome;
	
	
	@FindBy(how = How.XPATH, using = "//input[@name='Monthly Mortgage/Rent']")
	public WebElement afforRent;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Other Monthly Expenditure']")
	public WebElement afforExpend;
	
	@FindBy(how = How.XPATH, using = "//select[@name='Future Obligations']")
	public WebElement afforFeatureObli;
	
	//Bank Details
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Bank Details')]")
	public WebElement bankDetailBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='AccountName']")
	public WebElement bankAccountName;
	
	@FindBy(how = How.XPATH, using = "//select[@name='AccountType']")
	public WebElement bankAcType;
	
	@FindBy(how = How.XPATH, using = "//div[@class='row']//div[1]//input[1]")
	public WebElement BankSortCode1;

	@FindBy(how = How.XPATH, using = "//div[@class='row']//div[2]//input[1]")
	public WebElement BankSortCode2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='row']//div[3]//input[1]")
	public WebElement BankSortCode3;
	
	@FindBy(how = How.XPATH, using = "//input[@name='AccountNumber']")
	public WebElement bankAcNumber;
	
	@FindBy(how = How.XPATH, using = "//input[@name='BankName']")
	public WebElement bankAcName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='BranchName']")
	public WebElement bankAcBranch;
	
	@FindBy(how = How.XPATH, using = "//input[@name='BankAddress']")
	public WebElement bankAcAddrs;
	
	@FindBy(how = How.XPATH, using = "//input[@name='YearsAtBank']")
	public WebElement bankAcYrs;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary spec--apply-submit-button']")
	public WebElement appFormSubmitBtn;
	
	@FindBy(how = How.LINK_TEXT, using = "Reservation Fee")
	public WebElement ReservationFee;
	
	@FindBy(how = How.LINK_TEXT, using = "FINISH")
	public WebElement FINISH;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='list-i__item-value'])[1]")
	public WebElement ReservationID;
}
