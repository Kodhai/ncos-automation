package com.jlr.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OptionsContainer {

	@FindBy(how = How.XPATH, using = "(//*[@class='main-nav__link'])[text()='OPTIONS']")
	public WebElement optionsTab;

	//@FindBy(how = How.XPATH, using = "(//*[@class='sub-nav__link'])[contains(text(),'INFOTAINMENT')]")
	@FindBy(how = How.LINK_TEXT, using = "CONVENIENCE AND INFOTAINMENT")
	public WebElement convenienceInfotainment;
	
	//@FindBy(how = How.XPATH, using = "(//*[@aria-label])[text()='INFOTAINMENT']")
	@FindBy(how = How.XPATH, using = "//a[@aria-label='OPTIONS - INFOTAINMENT']")
	public WebElement infotainment;
	
	@FindBy(how = How.XPATH, using = "(//*[text()='CONVENIENCE AND INFOTAINMENT'])[1]")
	public WebElement convenience;

	public String selectGivenCategory(String category) {
		//return "//*[contains(@class,'tray-block__line')][text()='" + category + "']/parent::div/parent::div//div[contains(@class,'toggle__icon')]";
		return "//*[contains(@class,'tray-block__line')][text()='" + category + "']";
	}

}
