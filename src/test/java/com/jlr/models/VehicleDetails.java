package com.jlr.models;

public class VehicleDetails {

	String models;
	String engines;
	String specificationPack;
	String colours;
	String visualPack;
	String interior;
	String headlining;
	String options;
	String convenienceAndInfotainment;
	String Accessories;
	String touringaccessories;
	String paymentOption;
	String advRetail;
	String default_AR;
	String term;
	String defaultTerm;
	String annualMileage;
	String default_MIL;
	String deposit;
	String defaultDeposit;
	String baseTotal;
	String grandTotal;
	
	public String getModels() {
		return models;
	}
	public void setModels(String models) {
		this.models = models;
	}
	public String getEngines() {
		return engines;
	}
	public void setEngines(String engines) {
		this.engines = engines;
	}
	public String getSpecificationPack() {
		return specificationPack;
	}
	public void setSpecificationPack(String specificationPack) {
		this.specificationPack = specificationPack;
	}
	public String getColours() {
		return colours;
	}
	public void setColours(String colours) {
		this.colours = colours;
	}
	public String getVisualPack() {
		return visualPack;
	}
	public void setVisualPack(String visualPack) {
		this.visualPack = visualPack;
	}
	public String getInterior() {
		return interior;
	}
	public void setInterior(String interior) {
		this.interior = interior;
	}
	public String getHeadlining() {
		return headlining;
	}
	public void setHeadlining(String headlining) {
		this.headlining = headlining;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getConvenienceAndInfotainment() {
		return convenienceAndInfotainment;
	}
	public void setConvenienceAndInfotainment(String convenienceAndInfotainment) {
		this.convenienceAndInfotainment = convenienceAndInfotainment;
	}
	public String getAccessories() {
		return Accessories;
	}
	public void setAccessories(String accessories) {
		Accessories = accessories;
	}
	public String getTouringaccessories() {
		return touringaccessories;
	}
	public void setTouringaccessories(String touringaccessories) {
		this.touringaccessories = touringaccessories;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public String getAdvRetail() {
		return advRetail;
	}
	public void setAdvRetail(String advRetail) {
		this.advRetail = advRetail;
	}
	public String getDefault_AR() {
		return default_AR;
	}
	public void setDefault_AR(String default_AR) {
		this.default_AR = default_AR;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getDefaultTerm() {
		return defaultTerm;
	}
	public void setDefaultTerm(String defaultTerm) {
		this.defaultTerm = defaultTerm;
	}
	public String getAnnualMileage() {
		return annualMileage;
	}
	public void setAnnualMileage(String annualMileage) {
		this.annualMileage = annualMileage;
	}
	public String getDefault_MIL() {
		return default_MIL;
	}
	public void setDefault_MIL(String default_MIL) {
		this.default_MIL = default_MIL;
	}
	public String getDeposit() {
		return deposit;
	}
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
	public String getDefaultDeposit() {
		return defaultDeposit;
	}
	public void setDefaultDeposit(String defaultDeposit) {
		this.defaultDeposit = defaultDeposit;
	}
	public String getBaseTotal() {
		return baseTotal;
	}
	public void setBaseTotal(String baseTotal) {
		this.baseTotal = baseTotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	

	
	
	
}
