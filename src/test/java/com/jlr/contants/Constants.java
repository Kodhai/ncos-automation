package com.jlr.contants;

public class Constants {

	// Land Rover URL
	public static String DEFAULTURLNCOS = "https://rules.staging.configureconnect.com/lr2/r/nextsteps/_/en_gb-ncos-b-qa/l560_ncos/3kz1y/a-2td4_a-l560_sp0";// "https://rules.staging.configureconnect.com/lr2/r/model/_/en_gb-ncos-qa/l560_ncos/3kiu3/__";
	public static String DEFAULTURLCP = "gb.my.landrover.preview.qa.jlr-dev.com";

	// public static String LANDROVERHEADING="RANGE ROVER VELAR";
	public static String MODELHEADING = "MODELS";
	public static String MODELHEADING_BODYSTYLES = "BODYSTYLES";
	public static String LOGIN = "LOGIN";
	public static String REMINDERS = "REMINDERS";
	public static String HELLO = "HELLO";
	public static String LANDROVER = "LandRover";
	public static String JAGUARIPACE = "JaguarIPace";
	public static String JAGUAREPACE = "JaguarePace";
	public static String ENQUIRYMAX = "EnquiryMax";

	public static String THANKYOUMSG = "THANK YOU";
	public static String LANDROVERSUCCESSMSG = "A Land Rover representative will be in contact with you shortly.";
	public static String JAGUARSUCCESSMSG = "A Jaguar representative will be in contact with you shortly";

	public static String LANDROVERBROCHURESUCCESSMSG = "Thank you for generating your Personalised Configuration Brochure. If you have requested that your details be passed on to a Land Rover retailer they will be in contact with you soon.";
	public static String JAGUARBROCHURESUCCESSMSG = "Thank you for generating your Personalised Configuration Brochure. If you have requested that your details be passed on to a Jaguar retailer they will be in contact with you soon.";

	public static String REGISTRATIONNUMBER1 = "BJ67UMG"; // Change Last
															// Character From E
															// to Z
	public static String REGISTRATIONNUMBER2 = "BJ67UMY";

	// Vehicle Details
	public static String VEHICLENAME = "RENAULT CLIO 1.6L Automatic";
	public static String DOORS = "Doors: 5";
	public static String ENGINE = "Engine: 1.6L";
	public static String FUELTYPE = "Fuel Type: Petrol";
	public static String TRANSMISSION = "Transmission: Automatic";
	public static String REGDATE = "Registration Date: 20/10/17";

	// Vehicle Details for Registration number BJ67UMA
	public static String VEHICLENAME_BJ67UMA = "VOLKSWAGEN GOLF 1.6L Manual";
	public static String DOORS_BJ67UMA = "Doors: 5";
	public static String ENGINE_BJ67UMA = "Engine: 1.6L";
	public static String FUELTYPE_BJ67UMA = "Fuel Type: Diesel";
	public static String TRANSMISSION_BJ67UMA = "Transmission: Manual";
	public static String REGDATE_BJ67UMA = "Registration Date: 28/09/17";

	// Vehicle Details for Registration number BJ67UMO
	public static String VEHICLENAME_BJ67UMO = "VOLVO V40 2.0L Manual";
	public static String DOORS_BJ67UMO = "Doors: 5";
	public static String ENGINE_BJ67UMO = "Engine: 2.0L";
	public static String FUELTYPE_BJ67UMO = "Fuel Type: Diesel";
	public static String TRANSMISSION_BJ67UMO = "Transmission: Manual";
	public static String REGDATE_BJ67UMO = "Registration Date: 12/10/17";

	public static String MILEAGE = "3000";
	public static String NEGATIVEMILEAGE = "124578";
	public static String SETTLEMENTFIGURE = "200";

	public static String VEHICLENAMER1 = "RENAULT CLIO 1.6L Automatic";
	public static String DOORSR1 = "Doors: 5";
	public static String ENGINER1 = "Engine: 1.6L";
	public static String FUELTYPER1 = "Fuel Type: Petrol";
	public static String TRANSMISSIONR1 = "Transmission: Automatic";
	public static String REGDATER1 = "Registration Date: 20/10/17";

	public static final String PCP = "PERSONAL CONTRACT PURCHASE";
	public static final String HP = "HIRE PURCHASE";
	public static final String PCH = "PERSONAL CONTRACT HIRE (NON-MAINTAINED)";

	// Messages
	public static final String SURPLUSEQUITYMSG = "YOUR PART EXCHANGE EQUITY IS MORE THAN THE MAXIMUM DEPOSIT FOR YOUR NEW JAGUAR.\nThe remainder of your valuation will be offered to you as cashback.";
	public static final String PARTEXCHANGEUNAVAILABLE = "PART EXCHANGE IS NOT AVAILABLE FOR PCH\nYour preferred retailer can advise on the option to Part Exchange your current vehicle separately.";
	public static final String NEGATIVEEQUITYMSG2 = "Unfortunately we are unable to obtain this vehicles part exchange value, please contact the retailer to obtain a quote for this vehicle.";
	public static final String NEGATIVEEQUITYMSG1 = "UNFORTUNATELY, BASED ON THE INFORMATION PROVIDED IT APPEARS THAT YOUR VEHICLE DOES NOT HAVE ENOUGH EQUITY TO QUALIFY FOR A PART EXCHANGE";
	public static final String SURPLUSMSG2 = "The remainder of your valuation will be offered to you as cashback.";
	public static final String SURPLUSMSG1 = "YOUR PART EXCHANGE EQUITY IS MORE THAN THE MAXIMUM DEPOSIT FOR YOUR NEW JAGUAR.";

	// Customer Portal
	public static final String FINANCE = "Finance contract end date";
	public static final String INSURANCE = "Insurance end date";
	public static final String MANUFACTURER = "Manufacturer";
	public static final String MOT = "MOT due date";
	public static final String ROADTAX = "Road Tax due date";
	public static final String ROADSIDE = "Roadside Assistance service end date";
	public static final String SERVICEDUE = "Service due date";
	public static final String SERVICEPLAN = "Service Plan end date";
	public static final String HANDBOOKHEADER = "HANDBOOKS AND GUIDES";
	public static final String CHANGESSAVED = "Changes Saved";
	public static final String NEWVEHICLEADDEDSUCCESSMSG = "New Vehicle Added To Your Dashboard";
	public static final String ADDINGVEHICLEMSG = "Adding your vehicle details";
	public static final String RETAILERSAVED = "Retailer Saved";
	public static final String RED = "rgba(210, 0, 10, 1)";
	public static final String AMBER = "rgba(255, 127, 0, 1)";
	public static final String WHITE = "rgba(228, 232, 232, 1)";

	public static final String HELPSUPPORTTITLE = "HELP AND SUPPORT";
	public static final String GENERAL = "GENERAL";
	public static final String LOGINACCESS = "LOGIN AND ACCESS";
	public static final String DASHBOARD = "DASHBOARD";
	public static final String MYPROFILE = "MY PROFILE";
	public static final String VEHICLEMGMT = "VEHICLE MANAGEMENT";
	public static final String ADDVEHICLE = "ADDING A VEHICLE";
	public static final String HANDBOOKSGUIDES = "HANDBOOKS AND GUIDES";

	public static final String BSSERVICEPAGEMSG = "LET US KNOW WHICH SERVICES YOUR CAR NEEDS";
	public static final String BSCALENDARPAGEMSG = "PLEASE SELECT A DAY AND APPOINTMENT TIME";
	public static final String BSCONFIRMMSG = "THANK YOU FOR ENTERING YOUR DETAILS. YOUR SERVICE IS CONFIRMED FOR";

	public static final String SERVICERETAILERMAPPAGETITLEMSG = "Bing Maps - Directions, trip Planning, traffic Cameras & more";

	public static final String DEFAULTCOLOURJAGUAR = "Fuji White";
	public static final String DEFAULTHEADLININGJAGUAR = "Light Oyster Morzine Headlining";

	public static final String TOTALDEPOSIT = "Total deposit";
	public static final String TERM = "Term";
	public static final String ANNUALMILEAGE = "Annual Mileage";	
	
	//Delivery method login
	public static final String DELIVERYMETHODID = "testfact40@mailinator.com";
	public static final String DELIVERYMETHODPASS = "Ncos@2020";	
	
	public static final String HOMEPHONENUMBER = "0987654325";	
	public static final String SHOPPINGBSKETMSG = "Your basket has been saved and a link sent to your email address at";	
	
	public static final String CARDNAME = "ABCD";
	public static final String CARDNUMBER = "4929000000006";
	public static final String EXPDATE = "2020";
	public static final String EXPmonth  = "04 - April";
	public static final String CVVNUM = "1234";
	public static final String PASSWORD = "password";
	public static final String PAYMENTSUCCESSMSG ="YOUR PAYMENT HAS BEEN RECEIVED";
	public static final String ORDERRECEIVEDMSG ="YOUR ORDER HAS BEEN RECEIVED AND IS BEING PROCESSED";
	
	
}
