package com.jlr.data;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import com.jlr.autotest.utils.Config;
import com.jlr.base.InputExcelDataReader;

public class DataReader {
	InputExcelDataReader inputDataReader = new InputExcelDataReader();

	
	/* @DataProvider(name = "getdata")//, parallel = true) public Object[][]
     getdata(Method testName) throws Exception {
	 
	 return inputDataReader.readData(testName.getName(), "TestData.xlsx");
	 
	 }
	*/

	@DataProvider(name = "getdata") // , parallel = true)
	public Object[][] getdata(ITestContext context) throws Exception {

		System.out.println("Test Name " + context.getCurrentXmlTest().getName());
		return inputDataReader.readData(context.getCurrentXmlTest().getName(),
				Config.getPropertyValue("testData.excel"));
	}

	@DataProvider(name = "getEnquiryMaxData") // , parallel = true)
	public Object[][] getEnquiryMaxData(ITestContext context) throws Exception {

		System.out.println("Test Name " + context.getCurrentXmlTest().getName());
		return inputDataReader.readEnquiryMaxData(context.getCurrentXmlTest().getName(),
				Config.getPropertyValue("testData.enquiryMax.excel"));
	} 
	
	@DataProvider(name = "getdataR2") // , parallel = true)
	public Object[][] getdataR2(ITestContext context) throws Exception {

		System.out.println("Test Name " + context.getCurrentXmlTest().getName());
		return inputDataReader.readData(context.getCurrentXmlTest().getName(),
				Config.getPropertyValue("testDataR2.excel"));
	}
}
