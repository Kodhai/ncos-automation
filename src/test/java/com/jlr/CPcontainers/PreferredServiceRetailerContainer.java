package com.jlr.CPcontainers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PreferredServiceRetailerContainer {

	@FindBy(how = How.XPATH, using = "//*[text()='MY SERVICE RETAILER']")
	public WebElement serviceRetailerLink;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'CDX__DashboardWidgetCard__header undefined CDX__DashboardWidgetCard__header undefined--expanded')]//descendant::*[text()='MY SERVICE RETAILER']")
	public WebElement serviceRetailerextendLink;

	@FindBy(how = How.XPATH, using = "//div[@id='service-retailer-widget']//descendant::button[text()='CHANGE RETAILER']")
	public WebElement changeRetailerButton;

	@FindBy(how = How.XPATH, using = "//h3[text()='MY SERVICE RETAILER']/parent::div/following-sibling::div//input[contains(@class,'CDX__PreferredRetailer__search-form__input')]")
	public WebElement changeRetailerTextBox;

	@FindBy(how = How.XPATH, using = "//h3[text()='MY SERVICE RETAILER']/parent::div/following-sibling::div//div[@class='CDX__PreferredRetailer__search-form__button']")
	public WebElement changeRetailerSearchBtn;

	@FindBy(how = How.CLASS_NAME, using = "CDX__PreferredRetailer__search-item__retailer__name")
	public List<WebElement> nearestRetailerList;

	@FindBy(how = How.XPATH, using = "//*[text()='MY SERVICE RETAILER']//following::div[@class='CDX__PreferredRetailer__retailer-header__retailer-name']")
	public WebElement retailerName;

	@FindBy(how = How.XPATH, using = "//img[@class='CDX__PreferredRetailer__retailer-card__map-image']")
	public WebElement retailerServiceMap;

	@FindBy(how = How.XPATH, using = "(//A[@role='link'])[2]")
	public WebElement retailerServiceDirectionButton;

}
