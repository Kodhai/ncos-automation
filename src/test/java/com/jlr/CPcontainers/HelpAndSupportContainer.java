package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HelpAndSupportContainer {

	@FindBy(how = How.LINK_TEXT, using = "HELP AND SUPPORT")
	public WebElement helpSupportLink;

	@FindBy(how = How.XPATH, using = "//SPAN[@class='CDX__Navigation__navigation__title'][text()='HELP AND SUPPORT']")
	public WebElement helpAndSupportTitle;

	public String headerText(String header) {
		return "//H3[@class='CDX__Card__large__header__title'][text()='" + header + "']";
	}

	public String questionSearch(String question) {
		return "//H3[@class='CDX__Card__small__header__title'][text()='" + question + "']";
	}

	public String answer(String question) {
		return "//H3[text()='" + question
				+ "']//parent::div/parent::div//div[@class='CDX__ContentAccordion__card__inner--content']";
	}

}
