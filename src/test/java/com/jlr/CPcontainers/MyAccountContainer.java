package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MyAccountContainer {

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__DashboardWidgetCard__header CDX__DashboardWidgetCard__header--link']//descendant::*[text()='MY ACCOUNT']")
	public WebElement myAccountLink;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__Profile__header__title' and text()='PERSONAL INFORMATION']//following :: div[contains(@class,'CDX__Icon__cdx-icons--add')][1]")
	public WebElement personalInfoExpandLink;

	@FindBy(how = How.ID, using = "gigya-textbox-firstName")
	public WebElement personalInfoFName;

	@FindBy(how = How.ID, using = "gigya-textbox-lastName")
	public WebElement personalInfoLName;

	@FindBy(how = How.ID, using = "gigya-textbox-city")
	public WebElement personalInfoCity;

	@FindBy(how = How.ID, using = "gigya-dropdown-county")
	public WebElement personalInfoCountry;

	@FindBy(how = How.ID, using = "gigya-textbox-mobile-number")
	public WebElement personalInfoMobile;

	@FindBy(how = How.ID, using = "gigya-textbox-buildingNumber")
	public WebElement personalInfoBuildNo;

	@FindBy(how = How.ID, using = "gigya-textbox-buildingName")
	public WebElement personalInfoBuildName;

	@FindBy(how = How.ID, using = "gigya-textbox-postcode")
	public WebElement personalInfoPostCode;

	@FindBy(how = How.XPATH, using = "//*[text()='PERSONAL INFORMATION']//following::input[@class='gigya-input-submit'][1]")
	public WebElement personalInfoSaveButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Changes Saved']")
	public WebElement personalInfoSavedMsg;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__Profile__header__title' and text()='SECURITY']//following :: div[contains(@class,'CDX__Icon__cdx-icons--add')][1]")
	public WebElement securityExpandLink;

	@FindBy(how = How.ID, using = "gigya-password-42874458973501070")
	public WebElement securityPassword;

	@FindBy(how = How.ID, using = "gigya-password-newPassword")
	public WebElement securityOldPassword;

	@FindBy(how = How.XPATH, using = "//*[text()='SECURITY']//following::input[@class='gigya-input-submit'][1]")
	public WebElement securitySaveButton;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__Profile__header__title' and text()='GENERAL PREFERENCES']//following :: div[contains(@class,'CDX__Icon__cdx-icons--add')][1]")
	public WebElement preferenceExpandLink;

	@FindBy(how = How.ID, using = "gigya-dropdown-7503012648544021")
	public WebElement preferenceDistUnit;

	@FindBy(how = How.ID, using = "gigya-dropdown-17745126918946876")
	public WebElement preferenceDateFormat;

	@FindBy(how = How.XPATH, using = "//*[text()='GENERAL PREFERENCES']//following::input[@class='gigya-input-submit'][1]")
	public WebElement preferenceButton;
}
