package com.jlr.CPcontainers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageContainer {
	@FindBy(how = How.XPATH, using = "//div[@class='CDX__MainNavigation__button']")
	public WebElement moreOption;

	@FindBy(how = How.XPATH, using = "//*[text()='LOG OUT']")
	public WebElement logOut;
	
	@FindBy(how = How.LINK_TEXT, using = "MY ACCOUNT")
	public WebElement myAccount;
	
	@FindBy(how = How.CLASS_NAME, using = "CDX__PersonalisationBar__welcome-message__greeting")
	public WebElement welcomeMessage;

	@FindBy(how = How.CLASS_NAME, using = "CDX__DashboardWidgetCard__header__title")
	public WebElement reminders;

	@FindBy(how = How.XPATH, using = "//span[text()='ADD A VEHICLE']")
	public WebElement addVehicle;

	@FindBy(how = How.CLASS_NAME, using = "CDX__VehicleDropdown__expand-icon")
	public WebElement expandIcon;

	@FindBy(how = How.XPATH, using = "//A[@class='CDX__Cta__secondary']")
	public WebElement editVehicleInfo;

	@FindBy(how = How.XPATH, using = "(//span[@class='CDX__HeroVehicleSummary__nameplate-title__item-text'])[1]")
	public WebElement registrationNo;

	@FindBy(how = How.XPATH, using = "(//span[@class='CDX__HeroVehicleSummary__nameplate-title__item-text'])[2]")
	public WebElement vinNumber;

	@FindBy(how = How.XPATH, using = "(//span[@class='CDX__HeroVehicleSummary__nameplate-title__item-text'])[3]")
	public WebElement engineNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__Notifications__notification__title']")
	public WebElement notificationMsg;

	@FindBy(how = How.CLASS_NAME, using = "CDX__HeroVehicleSummary__nameplate-title__title")
	public WebElement vehicleNameHeader;

	@FindBy(how = How.CLASS_NAME, using = "CDX__HeroVehicleSummary__nameplate-title__nickname")
	public WebElement nickName;

	@FindBy(how = How.CLASS_NAME, using = "CDX__RemindersWidget__show-more__cta")
	public WebElement showMoreButton;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__RemindersWidget__reminder__status']")
	public WebElement daysRemaining;

	@FindBy(how = How.XPATH, using = "//a[text()='EDIT REMINDERS']")
	public WebElement editReminders;

	@FindBy(how = How.XPATH, using = "//h3[text()='HANDBOOKS AND GUIDES']")
	public WebElement handbookManualHeader;

	@FindBy(how = How.LINK_TEXT, using = "EXPLORE HANDBOOKS AND GUIDES")
	public WebElement exploreHandbookManualLink;

	@FindBy(how = How.XPATH, using = "//h3[text()='MY SALES RETAILER']")
	public WebElement mySalesRetailer;

	@FindBy(how = How.XPATH, using = "(//button[text()='CHANGE RETAILER'])[1]")
	public WebElement changeSalesRetailer;

	@FindBy(how = How.XPATH, using = "//h3[text()='MY SALES RETAILER']/parent::div/following-sibling::div//input[contains(@class,'CDX__PreferredRetailer__search-form__input')]")
	public WebElement enterSalesRetailer;

	@FindBy(how = How.XPATH, using = "//h3[text()='MY SALES RETAILER']/parent::div/following-sibling::div//div[@class='CDX__PreferredRetailer__search-form__button']")
	public WebElement searchButton;

	@FindBy(how = How.CLASS_NAME, using = "CDX__PreferredRetailer__search-item")
	public List<WebElement> retailerSearchList;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__PreferredRetailer__search-item__distance']")
	public List<WebElement> salesRetailerDistance;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__PreferredRetailer__search-item__retailer__name']")
	public List<WebElement> salesRetailerName;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__PreferredRetailer__search-item__retailer__address']")
	public List<WebElement> salesRetailerAddress;

	@FindBy(how = How.XPATH, using = "//*[@class='CDX__PreferredRetailer__search-item__retailer__address']")
	public List<WebElement> selectRetailer;

	@FindBy(how = How.XPATH, using = "//*[text()='MY SALES RETAILER']//parent::div//following-sibling::div//div[@class='CDX__PreferredRetailer__retailer-header__retailer-name']")
	public WebElement retailerName;

	@FindBy(how = How.XPATH, using = "//*[text()='MY SALES RETAILER']//parent::div//following-sibling::div//div[@class='CDX__PreferredRetailer__retailer-content__detail']")
	public List<WebElement> retailerContentDetail;

	@FindBy(how = How.XPATH, using = "(//A[@role='link'])[1]")
	public WebElement retailerGetDirections;

	public String selectRetailer(String retailerName) {
		return "(//*[@class='CDX__PreferredRetailer__search-item__retailer__name' and contains(text(),'" + retailerName
				+ "')])[1]";
	}

	public String reminderSelection(String reminderName) {
		return "//span[@class='CDX__RemindersWidget__reminder__info__title' and contains(text(),'" + reminderName
				+ "')]";
	}

	public String reminderDate(String reminderName) {
		return "//span[contains(text(),'" + reminderName
				+ "')]/following-sibling::span[@class='CDX__RemindersWidget__reminder__info__subtitle']";
	}

	public String vehicleSelection(String vehicleName) {
		return "(//SPAN[@class='CDX__VehicleDropdown__vehicle-name__description'][text()='" + vehicleName + "'])[1]";
	}

	public String reminderDayColor(String reminderName) {
		return "//span[contains(text(),'" + reminderName + "')]/parent::div//preceding::div[1]";
	}
}
