package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HandbookManualContainer {

	@FindBy(how = How.XPATH, using = "//H2[text()='SELECT YOUR VEHICLE AND MODEL YEAR']")
	public WebElement selectVehicleText;

	public String countryLangSelection(String param) {
		return "//BUTTON[@type='button'][text()='" + param + "']";
	}

}
