package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddVehicleContainer {

	@FindBy(how = How.ID, using = "inputField")
	public WebElement vinNumber;

	@FindBy(how = How.XPATH, using = "//button[text()='OWNER']")
	public WebElement owner;

	@FindBy(how = How.XPATH, using = "//button[text()='DRIVER']")
	public WebElement driver;

	@FindBy(how = How.XPATH, using = "//button[text()='FIND MY VEHICLE']")
	public WebElement findMyVehicle;

	@FindBy(how = How.XPATH, using = "(//INPUT[@type='null'])[2]")
	public WebElement engineNumber;

	@FindBy(how = How.XPATH, using = "//button[text()='ADD TO MY GARAGE']")
	public WebElement addToMyGarage;

	@FindBy(how = How.CLASS_NAME, using = "CDX__Navigation__navigation__title")
	public WebElement addVehiceTitle;

	public String ownerDriverSelection(String ownerDriver) {
		return "//button[text()='" + ownerDriver + "']";
	}

}
