package com.jlr.CPcontainers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BookAServiceContainer {

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__DashboardWidgetCard__header CDX__DashboardWidgetCard__header--link']//descendant::*[text()='BOOK A SERVICE']")
	public WebElement bookAServiceLink;

	@FindBy(how = How.XPATH, using = "//span[@class='CDX__HeroVehicleSummary__nameplate-title__title']")
	public WebElement homePageVehicleName;

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__HeroVehicleSummary__nameplate-title__model']")
	public WebElement homePageVehicleModel;

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__HeroVehicleSummary__nameplate-title__derivative']")
	public WebElement homePageVehicleEngine;

	@FindBy(how = How.XPATH, using = "//div[@class='CDX__HeroVehicleSummary__nameplate-title__transmission']")
	public WebElement homePageVehicleTrans;

	@FindBy(how = How.XPATH, using = "//span[text()='Model']//following-sibling::span")
	public WebElement bookSeviceVehicleName;

	@FindBy(how = How.XPATH, using = "//span[text()='Engine Size']//following-sibling::span")
	public WebElement bookServiceEngineSize;

	@FindBy(how = How.XPATH, using = "//span[text()='Transmission']//following-sibling::span")
	public WebElement bookServiceTrans;

	@FindBy(how = How.XPATH, using = "//span[text()='Model Year']//following-sibling::span")
	public WebElement bookServiceModelYear;

	@FindBy(how = How.XPATH, using = "//a[@id='select-retailer-7506']//descendant :: span[@class='s-label ng-binding']")
	public List<WebElement> bSNearestRetailerName;

	@FindBy(how = How.XPATH, using = "//div[@class='jlrsb c-retailers']//descendant::div[@class='s-item ng-scope'][1]//descendant::button[@title='Select Retailer']")
	public WebElement bSNearestRetailer;

	@FindBy(how = How.XPATH, using = "//*[text()='FIND A RETAILER CLOSE TO YOU']")
	public WebElement bSrRetailerSection;

	@FindBy(how = How.XPATH, using = "//div[@id='serviceStep']//descendant::span")
	public WebElement bSServiceTabMsg;

	@FindBy(how = How.XPATH, using = "//div[text()='I have a service plan ']//parent :: div[1]//following-sibling::div[contains(@class,'e-checkbox')]//descendant::input")
	public WebElement bSServicePlanChk;

	@FindBy(how = How.XPATH, using = "//div[@id='servicesCategory3']//descendant :: div[@class='s-label ng-binding ng-scope']")
	public List<WebElement> bSDefaultServicesList;

	@FindBy(how = How.XPATH, using = "//div[@id='servicesCategory3']//descendant::input")
	public List<WebElement> bSServiceDefaultPlanChk;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'c-button')]//descendant::button[@title='Continue']")
	public WebElement bSServiceContinueBtn;

	@FindBy(how = How.XPATH, using = "//div[@id='calendarStep']//descendant::span")
	public WebElement bSCalTabMsg;

	public String calMonth(String month) {
		return "//div[contains(@class,'s-month')]//descendant::span[contains(@class,'s-Do ng-binding')][text()='"
				+ month + "']/parent::span[1]";
	}

	public String calDay(String Day) {
		return "//div[contains(@class,'s-date')][@id='" + Day + "']";
	}

	public String timeSlot(String timeSlot) {
		return "//span[contains(@class,'s-hour')][text()='" + timeSlot + "']//preceding-sibling :: input[1]";
	}

	@FindBy(how = How.XPATH, using = "//div[@class='js-calendar ng-scope']/following::button")
	public WebElement bSCalContnBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='I have read, understood and accept the Privacy Policy - Please read']/preceding::input[1]")
	public WebElement bSPDPolicyChk;

	@FindBy(how = How.XPATH, using = "//span[text()='I accept the terms and conditions - Please read']/preceding::input[1]")
	public WebElement bSPDTermChk;

	@FindBy(how = How.XPATH, using = "//span[text()='I have read, understood and accept the Privacy Policy - Please read']//following::span[text()='Continue']")
	public WebElement bSPDContinueBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-select-match-text pull-left']")
	public WebElement bSPDTitleBtn;

	public String title(String title) {
		return "//span[@class='ng-binding ng-scope'][text()='" + title + "']";
	}

	@FindBy(how = How.ID, using = "first-name")
	public WebElement bSPDFirstName;

	@FindBy(how = How.ID, using = "last-name")
	public WebElement bSPDLastName;

	@FindBy(how = How.ID, using = "email")
	public WebElement bSPDEmail;

	@FindBy(how = How.ID, using = "housenumber")
	public WebElement bSPDHouseNumber;

	@FindBy(how = How.ID, using = "street")
	public WebElement bSPDStreet;

	@FindBy(how = How.ID, using = "town")
	public WebElement bSPDCity;

	@FindBy(how = How.ID, using = "postcode")
	public WebElement bSPDPostcode;

	@FindBy(how = How.ID, using = "telephone")
	public WebElement bSPDTelephone;

	@FindBy(how = How.XPATH, using = "//span[@class='c-location']//descendant::span[@class='s-name ng-binding']")
	public WebElement bSPDSummaryRetailerName;

	@FindBy(how = How.XPATH, using = "//span[@class='c-location']//descendant::span[@class='s-address ng-binding']")
	public WebElement bSPDSummaryRetailerAddr;

	@FindBy(how = How.XPATH, using = "//span[@class='s-date ng-binding']")
	public WebElement bSPDSummaryDateTime;

	@FindBy(how = How.XPATH, using = "//span[@class='s-name ng-binding ng-scope']")
	public WebElement bSPDSummaryName;

	@FindBy(how = How.XPATH, using = "//span[@class='s-phone ng-binding']")
	public WebElement bSPDSummaryMobile;

	@FindBy(how = How.XPATH, using = "//span[@class='s-address ng-binding ng-scope']")
	public WebElement bSPDSummaryAddr;

	@FindBy(how = How.XPATH, using = "//span[@class='s-email ng-binding']")
	public WebElement bSPDSummaryEmail;

	@FindBy(how = How.XPATH, using = "//p[@class='CDX__ServiceBooking__service-booking__summary']")
	public WebElement bSSummaryConfirmationLabel;

	@FindBy(how = How.ID, using = "confirmLnk")
	public WebElement bSSummaryContinueBtn;

	@FindBy(how = How.XPATH, using = "//span[@class='s-confirmed ng-binding']")
	public WebElement bSPDConfirmedMsg;

	@FindBy(how = How.ID, using = "amendBtn")
	public WebElement bSPDAmendBtn;
	
	@FindBy(how = How.XPATH, using ="//SPAN[text()='Car']")
	public WebElement bookServiceHeader;
	

}
