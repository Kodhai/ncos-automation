package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ManageVehiclePageContainer {

	@FindBy(how = How.CLASS_NAME, using = "CDX__Navigation__navigation__title")
	public WebElement manageMyVehicleHeader;

	@FindBy(how = How.ID, using = "registrationInputField")
	public WebElement regID;

	@FindBy(how = How.ID, using = "nicknameInputField")
	public WebElement nickName;

	@FindBy(how = How.CLASS_NAME, using = "CDX__Modal__modal__content")
	public WebElement popupContent;

	@FindBy(how = How.CLASS_NAME, using = "CDX__Modal__modal__header__title")
	public WebElement popupHeader;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'CDX__Modal__close')]")
	public WebElement popupClose;

	@FindBy(how = How.XPATH, using = "//textarea[contains(@class,'CDX__VehicleManagement__notes__input')]")
	public WebElement notes;

	@FindBy(how = How.XPATH, using = "//button[contains(@class,'VehicleManagement__save-button')]")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'CDX__Navigation__navigation__back')])[1]")
	public WebElement backButton;

	@FindBy(how = How.CLASS_NAME, using = "notificationMessages")
	public WebElement changesSaved;

	@FindBy(how = How.CLASS_NAME, using = "CDX__ToggleSwitch__switch CDX__Reminders__reminder__switch")
	public WebElement toggleSwitch;

	public String vehicleSelection(String vehicleName) {
		return "(//SPAN[@class='CDX__VehicleDropdown__vehicle-name__description'][text()='" + vehicleName + "'])[1]";
	}

	public String enterDay(String dateField) {
		return "//span[text()='" + dateField
				+ "']/parent::div/parent::div//div[@class='CDX__Reminders__reminder__date']/input[1]";
	}

	public String enterMonth(String dateField) {
		return "//span[text()='" + dateField
				+ "']/parent::div/parent::div//div[@class='CDX__Reminders__reminder__date']/input[2]";
	}

	public String enterYear(String dateField) {
		return "//span[text()='" + dateField
				+ "']/parent::div/parent::div//div[@class='CDX__Reminders__reminder__date']/input[3]";
	}

	public String info(String dateField) {
		return "//span[contains(text(),'" + dateField + "')]/following-sibling::div";
	}

	public String dateToggle(String dateField) {
		return "//span[text()='" + dateField
				+ "']/parent::div/parent::div//div[@class='CDX__ToggleSwitch__switch__slider']";
	}

	public String enabledDisabledToggle(String dateField) {
		return "//span[text()='" + dateField
				+ "']/parent::div/parent::div//div[contains(@class,'CDX__Reminders__reminder__switch')]";
	}

}
