package com.jlr.CPcontainers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginContainer {

	@FindBy(how = How.ID, using = "idToken1")
	public WebElement forgeRockUsername;

	@FindBy(how = How.ID, using = "idToken2")
	public WebElement forgeRockPassword;

	@FindBy(how = How.ID, using = "loginButton_0")
	public WebElement forgeRockLoginButton;

	@FindBy(how = How.XPATH, using = "(//INPUT[@name='username'])[4]")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//INPUT[@id='gigya-password-59166145808769920']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "(//INPUT[@class='gigya-input-submit'])[13]")
	public WebElement loginButton;	

	@FindBy(how = How.XPATH, using = "(//A[@data-switch-screen='gigya-register-screen'][text()='REGISTER'][text()='REGISTER'])[2]")
	public WebElement registerLink;
	
	@FindBy(how = How.XPATH, using = "//*[@id='register-site-login']/div[1]/div[2]/input")
	public WebElement emailId;
	
	@FindBy(how = How.ID, using = "gigya-textbox-140142641957359520")
	public WebElement firstName;
	
	@FindBy(how = How.ID, using = "gigya-textbox-143155388238866030")
	public WebElement lastName;
	
	@FindBy(how = How.ID, using = "gigya-password-106827196223889950")
	public WebElement passwordRegister;
	
	@FindBy(how = How.XPATH, using = "//INPUT[@id='gigya-checkbox-terms']")
	public WebElement termsConditions;
	
	@FindBy(how = How.XPATH, using = "(//INPUT[@class='gigya-input-submit'])[13]")
	public WebElement registerNowBackToLogin;
	
/*	@FindBy(how = How.XPATH, using = "")
	public WebElement backToLogin;*/
	
	
	
	
	
	

}
